% This script runs the KG and KG2 algorithms on the ATO (assemble to order)
% problem.

% When you run this script, make sure that the directories containing the AKG,
% AKG2, AssembleOrder, and associated functions called by these functions, are
% included in the path.  For example, if you are in the directory containing
% this script, you can first run the following command
% addpath(genpath('.'))

% Set up some information about the problem that we will solve (ATO).
q = 4;% this is a parameter of the Rosenbrock function determining its dimension.
dim=2*q;
x0 = 20*ones(1,dim); % sample at the point [20,..,20] initially.
probparam = struct('dim',dim,'runlength',70,'other',q);
valuerange = -10:10;
% Our problem function handle returns the negative function value of the
% Rosenbrock function. Maximizing this is the same as minimizing the Rosenbrock
% function.
problem = @(x,runlength,seed,other) -1*Rosenbrock(x,runlength,seed,other);
budget = 1:200;

% Accelerated KG2.
other2 = struct('n1',10*dim,'n2',2*dim,'rescale',2,...
    'NumRandomStartS',4,'NumRandomStartP',2,'NumRandomScreenS',1000,'NumRandomScreenP',500,...
    'UpdateTimes',[30,60,100,150,200:100:budget(end)]);
solverparam2 = struct('numinitsols',1, 'numfinalsols',length(budget), 'other',other2);

A2 = AKG2(x0,budget,problem,valuerange,probparam,solverparam2,randi(999999999),randi(999999999));

% Accelerated KG.  To run Accelerated KG, set NumRandomStartP and NumRandomScreenP to 0, and call AKG2.
other = struct('n1',10*dim,'n2',2*dim,'rescale',2,...
    'NumRandomStartS',4,'NumRandomStartP',0,'NumRandomScreenS',1000,'NumRandomScreenP',0,...
    'UpdateTimes',[30,60,100,150,200:100:budget(end)]);
solverparam = struct('numinitsols',0, 'numfinalsols',length(budget), 'other',other);

A = AKG2(x0,budget,problem,valuerange,probparam,solverparam,randi(999999999),randi(999999999));
