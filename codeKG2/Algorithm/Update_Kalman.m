%function [Mu,Sigma,S]=Update_Kalman(mu0,beta,alpha,n,L,CofX,Y)
function [Mu,Sigma]=Update_Kalman(mu0,Sigma0,n,invS,Yt)
%Sigma0=GPCov(beta,alpha,CofX);
%Yt=Y-mu0;
%S=Sigma0+L;
K=Sigma0*invS;
Mu=mu0+K*Yt;
Sigma=(eye(n)-K)*Sigma0;