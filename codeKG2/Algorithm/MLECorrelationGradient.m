function [pRg,pRalpha,pRcorr] = MLECorrelationGradient(xd,R,L,g,noisecorr,scale)
[n,d] = size(xd);
pRcorr = (1-g)*L;
pRg = R-eye(n)-noisecorr*L;
pRalpha = zeros(n,n,d);
for i=1:d
    temp = (bsxfun(@minus,xd(:,i),(xd(:,i))')).^2;
    pRalpha(:,:,i)=2*g*R.*temp/(scale(i)^3);
end