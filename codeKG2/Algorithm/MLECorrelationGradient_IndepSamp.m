function [pRg,pRalpha] = MLECorrelationGradient_IndepSamp(xd,R,g,scale)
[n,d] = size(xd);
pRg = R-eye(n);
pRalpha = zeros(n,n,d);
for i=1:d
    temp = (bsxfun(@minus,xd(:,i),(xd(:,i))')).^2;
    pRalpha(:,:,i)=2*g*R.*temp/(scale(i)^3);
end