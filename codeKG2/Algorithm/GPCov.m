% s=GPCov(beta,alpha,CofP1,CofP2)
% Computes the covariance matrix of a Gaussian process prior between every pair
% of alternatives, where the first alternative in the pair is from CofP1 and
% the second is from CofP2. 
%
% The covariance between two alternatives x1 and x2, both vectors, is:
% beta*exp(-sum(((x1-x2)./alpha).^2));
%
% beta is a scalar.
% alpha is either a scalar, or a vector of length dim, where dim is the problem dimension.
% CofP1 is a matrix with A rows and dim columns.  CofP1(i,:) is the ith alternative.
% CofP2 is a matrix with B rows and dim columns.  CofP2(i,:) is the ith alternative.
%
function s=GPCov(beta,alpha,CofP1,CofP2)
a=size(CofP1,1);b=size(CofP2,1);
s=zeros(a,b);
for i=1:a
    for j=1:b
        s(i,j)=beta*exp(-sum(((CofP1(i,:)-CofP2(j,:))./alpha).^2));
    end
end

