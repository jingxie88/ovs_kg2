% CofX is a vector containing the point at which to test the value of information.
% Its length is dim.
% n is the length of CofAltsSampled.

% We return the -log(VOI), which allows maximizing the VOI by minimizing the -log(VOI).
% We also return the gradient of the -log(VOI).

function [V,g]=VOI_TestSingular_Conly(Cofx,n,Mu,Sigma,mu0,Sigma0,cov0hat,scalehat,noisevarhat,invS,CofAltsSampled,Yt)
        Dim=length(Cofx);        
        V=-inf;

        if (size(Sigma)~=[n,n])
	  error(sprintf('Size of Sigma0 must be nxn.  n=%d, size(Sigma0)=%s', n, mat2str(size(Sigma))));
	end


	% AlreadySampled is the set of indices of those previously sampled
	% alternatives that are the same as Cofx.
        AlreadySampled=find(sum(abs(CofAltsSampled-ones(n,1)*Cofx),2)==0); 
        TMP=1:n;
        TMP(AlreadySampled)=[];
        if (~isempty(TMP)) % If we ever sampled something other than CofX. (usually true)
	    % m is a vector that is 1 for all alternatives that are the best
	    % among all already sampled alternatives except CofX. 
            m= Mu(TMP)==max(Mu(TMP));
            mi=ChooseRandomElement(TMP(m));
            if (~isempty(AlreadySampled)) % If the proposed alternative is one we've sampled.
               A=[AlreadySampled(1),mi];
               istar=AlreadySampled(1);
               [V,st]=VOI(istar,A,1,Mu,Sigma,eye(n)*noisevarhat);
               if nargout>1
                   if isnan(st)
                       g=zeros(Dim,1);
                   else
                       g = -SingularGradient_VOI(1,Dim,n,CofAltsSampled,istar,Sigma0,Sigma,invS,Yt,noisevarhat,...
					Cofx,scalehat.^(-2),1,A,Mu,st,1,V);
                   end
               end
            else         
               %Cofx=Coordinates(x,Dim);
               [MuP,SigmaP,Sigmat0]=Update_KalmanPlus_Conly_Abr(Mu,Sigma,mu0,Sigma0,cov0hat,scalehat,n,invS,CofAltsSampled,Yt,Cofx);
               %m=MuP(end);
               A=[n+1,mi];
               [V,st]=VOI(n+1,A,1,MuP,SigmaP,eye(n+1)*noisevarhat);
               if nargout>1
                   if isnan(st)
                       g=zeros(Dim,1);
                   else
                       g = -SingularGradient_VOI(1,Dim,n,CofAltsSampled,n+1,Sigmat0,SigmaP,invS,Yt,noisevarhat,...
						 Cofx,scalehat.^(-2),1,A,MuP,st,1,V);
                   end
               end
            end
        else % we have only sampled CofX.  
            % The VOI is 0, so log(V) is -inf as set above, and g=0.
            g=zeros(Dim,1);
        end
        V=-V; % we return -log(VOI), since we optimize using 'fmincon'.
%disp(V)
%disp(g)
end
