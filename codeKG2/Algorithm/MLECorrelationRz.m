function R = MLECorrelationRz(xd,scale)
n = size(xd,1); % Number of measurements	
R = zeros(n);
for i=2:n
    R(i,1:i-1)=GPCov(1,scale,xd(i,:),xd(1:i-1,:));
    R(1:i-1,i)=R(i,1:i-1)';
end