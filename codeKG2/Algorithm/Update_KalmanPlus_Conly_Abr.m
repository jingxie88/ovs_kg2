function [Mut,Sigmat,Sigmat0,AK]=Update_KalmanPlus_Conly_Abr(Mu,Sigma,mu0,Sigma0,beta,alpha,n,invS,CofX,Yt,Cofx)
sop=size(Cofx,1);
% Sigmat0=beta*eye(n+sop);
% Sigmat0(1:end-sop,1:end-sop)=Sigma0;
% if (sop==2)
% Sigmat0(n+1,n+2)=GPCov(beta,alpha,Cofx);
% Sigmat0(n+2,n+1)=Sigmat0(n+1,n+2);
% end
% for j=1:sop
%     AlreadySampled=find(X==x(j));    
%     if (~isempty(AlreadySampled))
%         Sigmat0(1:n,n+j)=Sigma0(1:n,AlreadySampled(1));
%         Sigmat0(n+j,1:n)=Sigmat0(1:n,n+j)';        
%     else for i=1:n
%         Sigmat0(i,n+j)=GPCov(beta,alpha,[CofX(i,:);Cofx(j,:)]);
%         end        
%         Sigmat0(n+j,1:n)=Sigmat0(1:n,n+j)';
%     end
% end
AlreadySampled=zeros(1,sop);
for j=1:sop
    temp=find(sum(abs(CofX-ones(n,1)*Cofx(j,:)),2)==0);
    if (~isempty(temp))
    AlreadySampled(j)=temp(1);
    end
end
if (sop==2)
    temp=GPCov(beta,alpha,Cofx(1,:),Cofx(2,:));
    RD=[beta,temp;temp,beta];
    if (AlreadySampled(1)~=0)
        if (AlreadySampled(2)~=0)
            Sigmat0=[[Sigma0,Sigma0(:,[AlreadySampled(1),AlreadySampled(2)])];[Sigma0([AlreadySampled(1),AlreadySampled(2)],:),RD]];
            Sigmat=[[Sigma,Sigma(:,[AlreadySampled(1),AlreadySampled(2)])];[Sigma([AlreadySampled(1),AlreadySampled(2)],:),[Sigma(AlreadySampled(1),AlreadySampled(1)),Sigma(AlreadySampled(1),AlreadySampled(2));Sigma(AlreadySampled(2),AlreadySampled(1)),Sigma(AlreadySampled(2),AlreadySampled(2))]]];  
            Mut = [Mu;Mu(AlreadySampled(1));Mu(AlreadySampled(2))];
        else
            RU=GPCov(beta,alpha,CofX,Cofx(2,:));
            TEMP=[Sigma0(:,AlreadySampled(1)),RU];
            Sigmat0=[[Sigma0,TEMP];[TEMP',RD]];
            AK = RU'*invS;
            Mut = [Mu;Mu(AlreadySampled(1));mu0+AK*Yt];
            tmp = Sigmat0(end,:)-AK*Sigmat0(1:end-2,:);
            TMP = [Sigma(AlreadySampled(1),:),Sigma(AlreadySampled(1),AlreadySampled(1)),tmp(end-1);tmp];
            Sigmat = [Sigma,(TMP(:,1:end-2))';TMP]; 
        end
    else if (AlreadySampled(2)~=0)
            RU=GPCov(beta,alpha,CofX,Cofx(1,:));
            TEMP=[RU,Sigma0(:,AlreadySampled(2))];
            Sigmat0=[[Sigma0,TEMP];[TEMP',RD]];
            AK = RU'*invS;
            Mut = [Mu;mu0+AK*Yt;Mu(AlreadySampled(2))];
            tmp = Sigmat0(end-1,:)-AK*Sigmat0(1:end-2,:);
            TMP = [tmp;Sigma(AlreadySampled(2),:),tmp(end),Sigma(AlreadySampled(2),AlreadySampled(2))];
            Sigmat = [Sigma,(TMP(:,1:end-2))';TMP];             
        else
            RU=GPCov(beta,alpha,CofX,Cofx);
            Sigmat0=[[Sigma0,RU];[RU',RD]];
            AK = RU'*invS;
            Mut = [Mu;mu0+AK*Yt];
            TMP = Sigmat0(end-1:end,:)-AK*Sigmat0(1:end-2,:);
            Sigmat = [Sigma,(TMP(:,1:end-2))';TMP];         
        end
    end
else if (AlreadySampled~=0)     
        Sigmat0=[[Sigma0,Sigma0(:,AlreadySampled)];[Sigma0(AlreadySampled,:),beta]];
        Sigmat=[Sigma,Sigma(:,AlreadySampled);Sigma(AlreadySampled,:),Sigma(AlreadySampled,AlreadySampled)];
        Mut = [Mu;Mu(AlreadySampled)];
    else
        RU=GPCov(beta,alpha,CofX,Cofx);
        Sigmat0=[[Sigma0,RU];[RU',beta]];
        AK = RU'*invS;%disp(AK)
        Mut = [Mu;mu0+AK*Yt];
        TMP = Sigmat0(end,:)-AK*Sigmat0(1:end-1,:);
        Sigmat = [Sigma,(TMP(:,1:end-1))';TMP];                 
    end
end

%Kt=Sigmat0(:,1:n)*invS;
%Mu=mu0+Kt*Yt;
%Sigma=Sigmat0-Kt*Sigmat0(1:n,:);
% AK = Sigmat0(end-sop+1:end,1:n)*invS;
% Mut = [Mu;mu0+AK*Yt];
% tmp = Sigmat0(end-sop+1:end,:)-AK*Sigmat0(1:n,:);
% Sigmat = [Sigma,(tmp(:,1:n))';tmp]; 
