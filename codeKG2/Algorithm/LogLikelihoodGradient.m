function pp = LogLikelihoodGradient(R,pR,onesR,ybR,y,sigma2)
[~,~,d] = size(pR);
pp = zeros(d,1);
for i=1:d
temp = onesR*pR(:,:,i);
pb = 1/((sum(onesR))^2)*(temp*(onesR)'*onesR*y-sum(onesR)*temp/R*y);
% if isnan(pb)
%     error('pb!')
% end
ps = 2*pb*sum(ybR)+ybR*pR(:,:,i)*ybR';
pp(i) = -ps/sigma2+trace(pR(:,:,i)/R);
end