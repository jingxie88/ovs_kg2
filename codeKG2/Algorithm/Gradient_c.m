function g = Gradient_c(a,b,ga,gb)
% a,b are length m column vectors. ga,gb are Dim-by-m matrices.
g = -diff(ga,1,2)*diag(1./diff(b))+diff(gb,1,2)*diag(diff(a)./(diff(b).^2));
% g is a Dim-by-(m-1) matrix.