function [V,g]=VOI_TestPair_Conly_GPSampCorr(CofX,Dim,NumSampled,Mu,Sigma,mu0,Sigma0,cov0hat,scalehat,CumSampCov,invS,CofAltsSampled,Yt,SampVar,SampCorrScale)
        Cofx=[CofX(1:Dim);CofX(Dim+1:end)];
if (sum(Cofx(1,:)==Cofx(2,:))==Dim)
    if nargout>1
    [V,g]=VOI_TestSingular_Conly(Cofx(1,:),NumSampled,Mu,Sigma,mu0,Sigma0,cov0hat,scalehat,SampVar,invS,CofAltsSampled,Yt);
    g=[g;g];    
    else
    V=VOI_TestSingular_Conly(Cofx(1,:),NumSampled,Mu,Sigma,mu0,Sigma0,cov0hat,scalehat,SampVar,invS,CofAltsSampled,Yt);
    end
else
        V=-inf;
        if nargout>1
            g = rand(2*Dim,1);
        end
    AlreadySampled1=find(sum(abs(CofAltsSampled-ones(NumSampled,1)*Cofx(1,:)),2)==0);
    AlreadySampled2=find(sum(abs(CofAltsSampled-ones(NumSampled,1)*Cofx(2,:)),2)==0);
    TMP=1:NumSampled;
    TMP(union(AlreadySampled1,AlreadySampled2))=[];
    if (~isempty(TMP))
            m= Mu(TMP)==max(Mu(TMP)); 
            mi=ChooseRandomElement(TMP(m));            
    if (~isempty(AlreadySampled1))
        if (~isempty(AlreadySampled2))
           A=[AlreadySampled1(1),AlreadySampled2(1),mi];
           istar=[AlreadySampled1(1),AlreadySampled2(1)];
           [V,st,~,~,c,I]=VOI(istar,A,1,Mu,Sigma,CumSampCov);
           V=V-log(2);      
             if nargout>1
                if isnan(st)
                    g=zeros(2*Dim,1);
                else
			    g = -PairGradient_VOI_GPSampCorr(I,Dim,NumSampled,CofAltsSampled,istar,Sigma0,Sigma,invS,Yt,SampVar,...
					 SampCorrScale,Cofx,scalehat.^(-2),1,A,Mu,st,c,V);
                end
             end
        else
        [MuP,SigmaP,Sigmat0]=Update_KalmanPlus_Conly_Abr(Mu,Sigma,mu0,Sigma0,cov0hat,scalehat,NumSampled,invS,CofAltsSampled,Yt,Cofx(2,:));
        A=[AlreadySampled1(1),NumSampled+1,mi];
        RU=GPCov(SampVar,SampCorrScale,CofAltsSampled,Cofx(2,:));
        CumSampCov1=[[CumSampCov,RU];[RU',SampVar]];
		istar=[AlreadySampled1(1),NumSampled+1];
        [V,st,~,~,c,I]=VOI(istar,A,1,MuP,SigmaP,CumSampCov1);
        V=V-log(2);   
        
        if nargout>1
            if isnan(st)
                g=zeros(2*Dim,1);
            else
                g = -PairGradient_VOI_GPSampCorr(I,Dim,NumSampled,CofAltsSampled,istar,Sigmat0,SigmaP,invS,Yt,SampVar,...
									  SampCorrScale,Cofx,scalehat.^(-2),1,A,MuP,st,c,V);
            end
        end
        end
           
    else if (~isempty(AlreadySampled2))
        [MuP,SigmaP,Sigmat0]=Update_KalmanPlus_Conly_Abr(Mu,Sigma,mu0,Sigma0,cov0hat,scalehat,NumSampled,invS,CofAltsSampled,Yt,Cofx(1,:));
        A=[NumSampled+1,AlreadySampled2(1),mi];    
        RU=GPCov(SampVar,SampCorrScale,CofAltsSampled,Cofx(1,:));
        CumSampCov1=[[CumSampCov,RU];[RU',SampVar]];
        istar=[NumSampled+1,AlreadySampled2(1)];
        [V,st,~,~,c,I]=VOI(istar,A,1,MuP,SigmaP,CumSampCov1);
        V=V-log(2);    
            if nargout>1
                if isnan(st)
                    g=zeros(2*Dim,1);
                else
                    g = -PairGradient_VOI_GPSampCorr(I,Dim,NumSampled,CofAltsSampled,istar,Sigmat0,SigmaP,invS,Yt,SampVar,...
								SampCorrScale,Cofx,scalehat.^(-2),1,A,MuP,st,c,V);
                end
            end
        else
                [MuP,SigmaP,Sigmat0]=Update_KalmanPlus_Conly_Abr(Mu,Sigma,mu0,Sigma0,cov0hat,scalehat,NumSampled,invS,CofAltsSampled,Yt,Cofx);
                A=[NumSampled+1,NumSampled+2,mi];
                RU=GPCov(SampVar,SampCorrScale,CofAltsSampled,Cofx);
                RL=GPCov(SampVar,SampCorrScale,Cofx(1,:),Cofx(2,:));
                CumSampCov1=[[CumSampCov,RU];[RU',[SampVar,RL;RL,SampVar]]];
			    istar=[NumSampled+1,NumSampled+2];
                [V,st,~,~,c,I]=VOI(istar,A,1,MuP,SigmaP,CumSampCov1);
                V=V-log(2);   
                if nargout>1
                    if isnan(st)
                        g=zeros(2*Dim,1);
                    else
                        g = -PairGradient_VOI_GPSampCorr(I,Dim,NumSampled,CofAltsSampled,istar,Sigmat0,SigmaP,invS,Yt,SampVar,...
							 SampCorrScale,Cofx,scalehat.^(-2),1,A,MuP,st,c,V);
                    end		
                end
        end
    end
    end
    V=-V;
end
end