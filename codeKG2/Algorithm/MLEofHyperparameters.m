% MLEofHyperparameters
% Estimates hyperparameters of a GP prior using maximum likelihood estimation. 
% Assumes that samples have taken with common random numbers are correlated with a
% single fixed correlation parameter.  Also assumes that the variance of the
% sampling noise is constant across the domain.  Uses a GP prior with a
% constant mean function.
%
% Inputs
% xd:	a set of points that have been previously evaluated
% y:	values observed at each of these points
% L:	PF: what is this?
% g0, noisecorr0, scale0 are initial parameter estimates of g=cov0/(noisevar +
% cov0), noisecorr, and scale respectively.  See the outputs below for a
% description of cov0, noisevar, noisecorr, and scale.
% rescale:	optional argument, with a default value of 1.  PF: what is this?  
%
% Outputs: 
% cov0hat:	estimate of the marginal variance of the value of the objective
% 		function at a single point, under the prior.
% noisevarhat:	estimate of the variance of the noise.
% scalehat:	estimate of the scale parameter in the Gaussian process prior.
% noisecorrhat:	estimate of the correlation of the noise.
% betahat:	estimate of the mean of the GP prior.
function [cov0hat,noisevarhat,scalehat,noisecorrhat,betahat]...
         = MLEofHyperparameters(xd,y,L,g0,noisecorr0,scale0,rescale)
     
if nargin<8
    rescale=1;
end	
     
n = length(y); % Number of measurements
mxd = mean(xd);   sxd = std(xd);
my = mean(y);   sy = std(y);
xd = (xd - repmat(mxd,n,1)) ./ repmat(sxd,n,1);
y = (y - my)/sy;
scale0 = scale0./sxd;

% scale = scale0; g = g0; noisecorr = noisecorr0;
% for n=1:10
%     scale = fminsearchbnd3(@(X)HuangLogLikelihood(y,MLECorrelationRz(xd,y,X),L,g,noisecorr),scale,ones(1,d)*1e-3,ones(1,d)*1e3,optimset('Algorithm','active-set','MaxFunEvals',10));
% R = MLECorrelationRz(xd,y,scale);
% T = fminsearchbnd3(@(X)HuangLogLikelihood(y,R,L,X(1),X(2)),[g,noisecorr],[1e-3,1e-3],[1,1],optimset('Algorithm','active-set','MaxFunEvals',100));
% g = T(1); noisecorr = T(2);
% %disp(n)
% end
myfun = @(X)HuangLogLikelihood('true',xd,y,MLECorrelationRz(xd,X(3:end)),L,X(1),X(2),X(3:end));
options = optimset('GradObj','on','TolFun',1e-8);%,'Algorithm','active-set');%);%,'MaxFunEvals',1000);
HAT = fmincon(myfun,[g0,noisecorr0,scale0],[],[],[],[],[.01,.01,scale0*.1],[.95,.95,scale0*10],[],options);
ghat = HAT(1);
noisecorrhat = HAT(2);
scalehat = HAT(3:end)/rescale;

[~,~,betahat,sigma2hat] = HuangLogLikelihood('false',xd,y,MLECorrelationRz(xd,scalehat),L,ghat,noisecorrhat,scalehat);
cov0hat = ghat*sigma2hat;
noisevarhat = sigma2hat - cov0hat;
    
betahat = betahat*sy + my;
cov0hat = cov0hat * sy^2;
noisevarhat = noisevarhat * sy^2;
scalehat = scalehat .* sxd;
