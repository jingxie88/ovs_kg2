function f=fm(z)
%fm(.)=f(-.) in the paper.
if (z>10)
    f=normpdf(z)./(z.^2+1);
else
f=normpdf(z)-z.*normcdf(-z);
end