function g = SingularGradient_mu(I,Dim,istar,invS,Yt,J)
% I shoud be row vector.
i = find(I==istar);
g = sparse(Dim,length(I)); 
if ~isempty(i)
g(:,i)=repmat(J*(invS*Yt),1,length(i));
end
