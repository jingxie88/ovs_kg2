function g = PairGradient_mu(I,Dim,istar,invS,Yt,J)
% I shoud be row vector.
i1 = find(I==istar(1));
i2 = find(I==istar(2));
g = sparse(2*Dim,length(I)); 
if ~isempty(i1)
g(1:Dim,i1)=repmat(J(1:Dim,:)*(invS*Yt),1,length(i1));
end
if ~isempty(i2)
g(Dim+1:end,i2)=repmat(J(Dim+1:end,:)*(invS*Yt),1,length(i2));
end
