function J = JforGradient(Cofx,Dim,NumSampled,CofAltsSampled,PriorCovVec,alpha)
% alpha = scale.^(-2); PriorCovVec = Sigma0(istar,1:NumSampled).
l = size(Cofx,1); % l==1 if singular, l==2 if pair. l==size(PriorCovVec,1).
J = zeros(l*Dim,NumSampled);
for i = 1:l
J((i-1)*Dim+1:i*Dim,:) = 2*repmat(PriorCovVec(i,:),Dim,1).*repmat(alpha(:),1,NumSampled).*(CofAltsSampled'...
-repmat(Cofx(i,:)',1,NumSampled));
end

