function st=SigmaTilde(X,m,Sigma,Lambda)
%When vector X has only 1 or 2 rows, we can only input the index (or
%indices) of the non-zero element(s).
if (length(X)==1)
    st=Sigma(:,X)/sqrt(Lambda(X,X)/m+Sigma(X,X));
else if(length(X)==2)
        x1=X(1);x2=X(2);
        temp=Lambda(x1,:)-Lambda(x2,:);
tempp=Sigma(x1,:)-Sigma(x2,:);
den=(temp(x1)-temp(x2))/m+(tempp(x1)-tempp(x2));
if (den<=0)
    st=NaN;
else
st=(Sigma(:,x1)-Sigma(:,x2))/sqrt(den);
end
    else
        st=Sigma*X'/sqrt(X*Lambda*X'/m+X*Sigma*X');
    end
end



