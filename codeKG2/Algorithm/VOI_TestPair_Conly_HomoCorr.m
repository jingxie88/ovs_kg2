function [V,g]=VOI_TestPair_Conly_HomoCorr(CofX,Dim,NumSampled,Mu,Sigma,mu0,Sigma0,cov0hat,scalehat,noisevarhat,noisecorrhat,invS,CofAltsSampled,Yt)
        Cofx=[CofX(1:Dim);CofX(Dim+1:end)];
if (sum(Cofx(1,:)==Cofx(2,:))==Dim)
    if nargout>1
    [V,g]=VOI_TestSingular_Conly(Cofx(1,:),NumSampled,Mu,Sigma,mu0,Sigma0,cov0hat,scalehat,noisevarhat,invS,CofAltsSampled,Yt);
    g=[g;g];
    else
    V=VOI_TestSingular_Conly(Cofx(1,:),NumSampled,Mu,Sigma,mu0,Sigma0,cov0hat,scalehat,noisevarhat,invS,CofAltsSampled,Yt);
    end
else
        V=-inf;
        if nargout>1
            g = rand(2*Dim,1);
        end
    AlreadySampled1=find(sum(abs(CofAltsSampled-ones(NumSampled,1)*Cofx(1,:)),2)==0);
    AlreadySampled2=find(sum(abs(CofAltsSampled-ones(NumSampled,1)*Cofx(2,:)),2)==0);
    TMP=1:NumSampled;
    TMP(union(AlreadySampled1,AlreadySampled2))=[];
    if (~isempty(TMP))
            m= Mu(TMP)==max(Mu(TMP)); 
            mi=ChooseRandomElement(TMP(m));            
    if (~isempty(AlreadySampled1))
        if (~isempty(AlreadySampled2))
           A=[AlreadySampled1(1),AlreadySampled2(1),mi];
           EstSampCov=noisevarhat*(noisecorrhat*ones(NumSampled)+(1-noisecorrhat)*eye(NumSampled));
           istar=[AlreadySampled1(1),AlreadySampled2(1)];
           [V,st,~,~,c,I]=VOI(istar,A,1,Mu,Sigma,EstSampCov);
           V=V-log(2);
           if nargout>1
                if isnan(st)
                    g=zeros(2*Dim,1);
                else
                    g = -PairGradient_VOI(I,Dim,NumSampled,CofAltsSampled,istar,Sigma0,Sigma,invS,Yt,noisevarhat,...
								 noisecorrhat,Cofx,scalehat.^(-2),1,A,Mu,st,c,V);
                end
           end
        else
        [MuP,SigmaP,Sigmat0]=Update_KalmanPlus_Conly_Abr(Mu,Sigma,mu0,Sigma0,cov0hat,scalehat,NumSampled,invS,CofAltsSampled,Yt,Cofx(2,:));
        A=[AlreadySampled1(1),NumSampled+1,mi];
        %RU=noisevarhat*CorrPredictor(CofAltsSampled,Cofx(2,:),dmodel);
        EstSampCov1=noisevarhat*(noisecorrhat*ones(NumSampled+1)+(1-noisecorrhat)*eye(NumSampled+1));
        istar=[AlreadySampled1(1),NumSampled+1]; 
        [V,st,~,~,c,I]=VOI(istar,A,1,MuP,SigmaP,EstSampCov1);
        V=V-log(2); 
        if nargout>1
            if isnan(st)
                g=zeros(2*Dim,1);
            else
                g = -PairGradient_VOI(I,Dim,NumSampled,CofAltsSampled,istar,Sigmat0,SigmaP,invS,Yt,noisevarhat,...
					 noisecorrhat,Cofx,scalehat.^(-2),1,A,MuP,st,c,V);
            end
        end
        end
    else if (~isempty(AlreadySampled2))
        [MuP,SigmaP,Sigmat0]=Update_KalmanPlus_Conly_Abr(Mu,Sigma,mu0,Sigma0,cov0hat,scalehat,NumSampled,invS,CofAltsSampled,Yt,Cofx(1,:));
        A=[NumSampled+1,AlreadySampled2(1),mi];    
        %RU=noisevarhat*CorrPredictor(CofAltsSampled,Cofx(1,:),dmodel);
        %EstSampCov1=[[EstSampCov,RU];[RU',noisevarhat]];
        EstSampCov1=noisevarhat*(noisecorrhat*ones(NumSampled+1)+(1-noisecorrhat)*eye(NumSampled+1));
		istar=[NumSampled+1,AlreadySampled2(1)];
        [V,st,~,~,c,I]=VOI(istar,A,1,MuP,SigmaP,EstSampCov1);
        V=V-log(2);        
        if nargout>1
            if isnan(st)
                g=zeros(2*Dim,1);
            else
                g = -PairGradient_VOI(I,Dim,NumSampled,CofAltsSampled,istar,Sigmat0,SigmaP,invS,Yt,noisevarhat,...
									   noisecorrhat,Cofx,scalehat.^(-2),1,A,MuP,st,c,V);
            end
        end
        else
                [MuP,SigmaP,Sigmat0]=Update_KalmanPlus_Conly_Abr(Mu,Sigma,mu0,Sigma0,cov0hat,scalehat,NumSampled,invS,CofAltsSampled,Yt,Cofx);
                A=[NumSampled+1,NumSampled+2,mi];
                %RU=noisevarhat*CorrPredictor(CofAltsSampled,Cofx,dmodel);
                %RL=CorrPredictor(Cofx(1,:),Cofx(2,:),dmodel);
                EstSampCov1=noisevarhat*(noisecorrhat*ones(NumSampled+2)+(1-noisecorrhat)*eye(NumSampled+2));
                %EstSampCov1=[[EstSampCov,RU];[RU',noisevarhat*[1,RL;RL,1]]];
                istar=[NumSampled+1,NumSampled+2];
                [V,st,~,~,c,I]=VOI(istar,A,1,MuP,SigmaP,EstSampCov1);
                V=V-log(2);  
            
                if nargout>1
                    if isnan(st)
                    g=zeros(2*Dim,1);
                    else
                    g = -PairGradient_VOI(I,Dim,NumSampled,CofAltsSampled,istar,Sigmat0,SigmaP,invS,Yt,noisevarhat,...
					 noisecorrhat,Cofx,scalehat.^(-2),1,A,MuP,st,c,V);
                    end
                end
        end
    end
    end
    V=-V;
end
end