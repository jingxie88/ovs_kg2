function [p,G,beta,sigma2] = HuangLogLikelihood(GradientNeeded,xd,y,R0,L,g,noisecorr,scale)
[n,d] = size(xd);
%R = MLECorrelationRz(xd,y,scale);
R = g*R0+eye(n)+(1-g)*noisecorr*L;
%invR = inv(R);
CF = chol(R,'lower');
onesR = (CF'\(CF\ones(n,1)))';
% onesR = ones(1,n)/R;
beta = onesR*y/sum(onesR);
% ybR = (y-beta)'/R;
ybR = (CF'\(CF\(y-beta)))';
sigma2 = ybR*(y-beta)/n;
% p = log(det(R))+n*log(sigma2);
p = 2*sum(log(diag(CF)))+n*log(sigma2);

if GradientNeeded
[pRg,pRa,pRr] = MLECorrelationGradient(xd,R0,L,g,noisecorr,scale);
G = zeros(2+d,1);
G(1) = LogLikelihoodGradient(R,pRg,onesR,ybR,y,sigma2);
G(2) = LogLikelihoodGradient(R,pRr,onesR,ybR,y,sigma2);
G(3:end) = LogLikelihoodGradient(R,pRa,onesR,ybR,y,sigma2);
else
    G=0;
end