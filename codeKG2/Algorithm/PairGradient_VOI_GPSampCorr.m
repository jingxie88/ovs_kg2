function g = PairGradient_VOI_GPSampCorr(I,Dim,NumSampled,CofAltsSampled,istar,Sigma0,Sigma,invS,Yt,SampVar,SampCorrScale,Cofx,alpha,beta,A,mu,st,c,V)
J = JforGradient(Cofx,Dim,NumSampled,CofAltsSampled,Sigma(istar,1:NumSampled),alpha);
AI = A(I);
ga = PairGradient_mu(AI,Dim,istar,invS,Yt,J);
gb = PairGradient_st_GPSampCorr(AI,Dim,NumSampled,istar,Sigma0,Sigma,invS,J,SampVar,SampCorrScale,Cofx,alpha,beta);
a = mu(AI); b = st(AI);
a = a(:); b = b(:); c = c(2:end-1); c=c(:);
% length(c) should be length(a) or length(b)-1
if isempty(c)
    g = zeros(2*Dim,1);
else
    if V < -1e2
        g = randn(2*Dim,1);
    else
        g1 = diff(gb,1,2)*fm(abs(c));
        g2 = Gradient_c(a,b,ga,gb)*(sign(c).*normcdf(-abs(c)).*diff(b));
        g = (g1-g2)/exp(V);
        t = max(abs(g));
        if t>1e10
            g = g/t;
        end
    end
end