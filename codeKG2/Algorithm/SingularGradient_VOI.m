function g = SingularGradient_VOI(I,Dim,NumSampled,CofAltsSampled,istar,Sigma0,Sigma,invS,Yt,SampVar,Cofx,alpha,beta,A,mu,st,c,V)

J = JforGradient(Cofx,Dim,NumSampled,CofAltsSampled,Sigma0(istar,1:NumSampled),alpha);

if length(A)>2
AI = A(I);
ga = SingularGradient_mu(AI,Dim,istar,invS,Yt,J);
gb = SingularGradient_st(AI,Dim,NumSampled,istar,Sigma0,Sigma,invS,J,SampVar,beta);
a = mu(AI); b = st(AI);
a = a(:); b = b(:); c = c(2:end-1); c = c(:); 
% length(c) should be length(a) or length(b)-1
if isempty(c)
    g = zeros(Dim,1);
else
    g = (diff(gb,1,2)*fm(abs(c))-Gradient_c(a,b,ga,gb)*(sign(c).*normcdf(-abs(c)).*diff(b)))/exp(V);
end

else % length(A)==2
s = abs(st(A(1))-st(A(2)));
if s < 1e-8
    g = zeros(Dim,1);
else
    if V < -1e2
        g = randn(Dim,1);
    else
        d = abs(mu(A(1))-mu(A(2)));
        gs = -sign(st(A(1))-st(A(2)))*diff(SingularGradient_st(A,Dim,NumSampled,istar,Sigma0,Sigma,invS,J,SampVar,beta),1,2);
        gd = -sign(mu(A(1))-mu(A(2)))*diff(SingularGradient_mu(A,Dim,istar,invS,Yt,J),1,2);
        g = (fm(d/s)*gs-normcdf(-d/s)*(gd*s-d*gs)/s)/exp(V);
    end
end
end