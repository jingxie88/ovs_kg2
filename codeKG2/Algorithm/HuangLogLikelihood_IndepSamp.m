function [p,G,beta,sigma2] = HuangLogLikelihood_IndepSamp(GradientNeeded,xd,y,R0,g,scale)
[n,d] = size(xd);
%R = MLECorrelationRz(xd,y,scale);
R = g*R0+eye(n);
%invR = inv(R);
CF = chol(R,'lower');
onesR = (CF'\(CF\ones(n,1)))';
beta = onesR*y/sum(onesR);
ybR = (CF'\(CF\(y-beta)))';
sigma2 = ybR*(y-beta)/n;
p = 2*sum(log(diag(CF)))+n*log(sigma2);
if GradientNeeded
[pRg,pRa] = MLECorrelationGradient_IndepSamp(xd,R0,g,scale);
G = zeros(1+d,1);
G(1) = LogLikelihoodGradient(R,pRg,onesR,ybR,y,sigma2);
G(2:end) = LogLikelihoodGradient(R,pRa,onesR,ybR,y,sigma2);
else
    G=0;
end