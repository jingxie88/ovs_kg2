function [V,st,a,b,c,I]=VOI(X,A,m,mu,Sigma,Lambda)
% Returns Log(VOI).
V=-inf;
if any(isnan(mu))||any(any(isnan(Sigma)))
   st=NaN;a=NaN;b=NaN;c=NaN;I=NaN;
else
if(length(A)>1)
    st=SigmaTilde(X,m,Sigma,Lambda);
    if isnan(st)
        V=-inf;st=NaN;a=NaN;b=NaN;c=NaN;I=NaN;
    else
    if (length(A)==2)    
        s=abs(st(A(1))-st(A(2)));
%   V=s*fm(abs(mu(A(1))-mu(A(2)))/s);
        V=log(s)+LogEI(-abs(mu(A(1))-mu(A(2)))/s);
    else 
%       V=h(mu(A),st(A));
        [V,a,b,c,I]=LogEmaxAffine(mu(A),st(A));        
    end
    end
end
end