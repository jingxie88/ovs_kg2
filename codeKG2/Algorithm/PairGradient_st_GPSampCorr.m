function g = PairGradient_st_GPSampCorr(I,Dim,NumSampled,istar,Sigma0,Sigma,invS,J,SampVar,SampCorrScale,Cofx,alpha,beta)

temp = 2*Sigma0(istar(1),istar(2))*(diff(Cofx))'.*(alpha(:));
temp1 = J*(invS*Sigma0(1:NumSampled,istar(1)));
temp2 = J*(invS*Sigma0(1:NumSampled,istar(2)));

corr = GPCov(1,SampCorrScale,Cofx(1,:),Cofx(2,:));
densqure = 2*SampVar*(1-corr)/beta+Sigma(istar(1),istar(1))+Sigma(istar(2),istar(2))-2*Sigma(istar(1),istar(2));
den = sqrt(densqure);

temp3 = 2*SampVar*corr/beta*(SampCorrScale(:))'.^(-2).*(diff(Cofx))';
g21 = (-temp3-temp-temp1(1:Dim)+temp2(1:Dim))/den;
g22 = (temp3+temp-temp2(1+Dim:end)+temp1(1+Dim:end))/den;

l = length(I);
g = zeros(2*Dim,l);
for j = 1:l
i = I(j);
if i~=istar(1)
    if i>NumSampled
        g11 = temp-J(1:Dim,:)*(invS*Sigma0(1:NumSampled,i));
    else
        g11 = J(1:Dim,i)-J(1:Dim,:)*(invS*Sigma0(1:NumSampled,i));
    end
else
g11 = -temp-2*temp1(1:Dim)+temp2(1:Dim);
end
if i~=istar(2)
    if i>NumSampled
        g12 = temp+J(Dim+1:end,:)*(invS*Sigma0(1:NumSampled,i));
    else
        g12 = -J(Dim+1:end,i)+J(Dim+1:end,:)*(invS*Sigma0(1:NumSampled,i));
    end
else
g12 = -temp+2*temp2(1+Dim:end)-temp1(1+Dim:end);
end
g(:,j) = [(den*g11-(Sigma(i,istar(1))-Sigma(i,istar(2)))*g21);(den*g12-(Sigma(i,istar(1))-Sigma(i,istar(2)))*g22)]/densqure;
end
