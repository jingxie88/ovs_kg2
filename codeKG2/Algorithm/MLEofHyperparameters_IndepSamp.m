function [cov0hat,noisevarhat,scalehat,betahat]...
         = MLEofHyperparameters_IndepSamp(xd,y,g0,scale0,rescale)
% g0, scale0 are initial parameter estimates.      

if nargin<6
    rescale=1;
end
     
n = length(y); % Number of measurements
mxd = mean(xd);   sxd = std(xd);
my = mean(y);   sy = std(y);
xd = (xd - repmat(mxd,n,1)) ./ repmat(sxd,n,1);
y = (y - my)/sy;
scale0 = scale0./sxd;

% scale = scale0; g = g0; noisecorr = noisecorr0;
% for n=1:10
%     scale = fminsearchbnd3(@(X)HuangLogLikelihood(y,MLECorrelationRz(xd,y,X),L,g,noisecorr),scale,ones(1,d)*1e-3,ones(1,d)*1e3,optimset('Algorithm','active-set','MaxFunEvals',10));
% R = MLECorrelationRz(xd,y,scale);
% T = fminsearchbnd3(@(X)HuangLogLikelihood(y,R,L,X(1),X(2)),[g,noisecorr],[1e-3,1e-3],[1,1],optimset('Algorithm','active-set','MaxFunEvals',100));
% g = T(1); noisecorr = T(2);
% %disp(n)
% end
myfun = @(X)HuangLogLikelihood_IndepSamp('true',xd,y,MLECorrelationRz(xd,X(2:end)),X(1),X(2:end));
options = optimset('GradObj','on','TolFun',1e-8,'MaxFunEvals',1000);%,'Algorithm','active-set');
HAT = fmincon(myfun,[g0,scale0],[],[],[],[],[1e-2,scale0*.1],[1-1e-2,scale0*10],[],options);
ghat = HAT(1);
scalehat = HAT(2:end)/rescale;

[~,~,betahat,sigma2hat] = HuangLogLikelihood_IndepSamp('false',xd,y,MLECorrelationRz(xd,scalehat),ghat,scalehat);
cov0hat = ghat*sigma2hat;
noisevarhat = sigma2hat - cov0hat;
    
betahat = betahat*sy + my;
cov0hat = cov0hat * sy^2;
noisevarhat = noisevarhat * sy^2;
scalehat = scalehat .* sxd;