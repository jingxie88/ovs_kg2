% This test code runs the AKG2 algorithm on the RosenbrockXieChickFrazier
% function.  It is focused on testing the implementation of the
% "UseFirstStageSamples" flag.

% Runs AKG2 under the Rosenbrock with UseFirstStageSamples on.
function test_AKG2a()
  % test1(1);
  % test1(0);
  test2();
end

% Runs AKG2 with and without UseFirstStageSamples turned on, and just sees
% whether or not it runs without crashing.
function test1(UseFirstStageSamples)
  addpath(genpath('.'));
  q = 6;% this is a parameter of the Rosenbrock function determining its dimension.
  dim=q;
  x0 = []; % no initial samples.

  % Independent random number stream used by RosenbrockXieChickFrazier.
  stream = RandStream.create('mrg32k3a', 'NumStreams',2,'StreamIndices',2); 

  probother = struct('q',q,'stream',stream);
  probparam = struct('dim',dim,'runlength',1,'other',probother);
  valuerange = -.8:.3:1.9;

  % Our problem function handle returns the negative function value of the
  % RosenbrockXieFrazierChick function. Maximizing this is the same as
  % minimizing RosenbrockXieFrazierChick.
  problem = @(x,runlength,seed,other) -1*RosenbrockXieFrazierChick(x,runlength,seed,other);

  budget = 1:200;
  other = struct('UseFirstStageSamples',UseFirstStageSamples);
  solverparam = struct('numinitsols',0,'numfinalsols',length(budget),'other',other);

  % Run the algorithm.
  seed = 1;
  solutions = AKG2(x0,budget,problem,valuerange,probparam,solverparam,seed,seed);
end

% Run a treatment with UseFirstStageSamples turned off, and compares with a
% test file created before this flag was implemented, and it was hard-coded as
% off. 
function test2()

  addpath(genpath('.'));
  q = 6;% this is a parameter of the Rosenbrock function determining its dimension.
  dim=q;
  x0 = []; % no initial samples.

  % load in the solution file for the old run of this.
  old = load('test_AKG2.mat');

  stream = RandStream.create('mrg32k3a', 'NumStreams',2,'StreamIndices',2); 
  probother = struct('q',q,'stream',stream);
  probparam = struct('dim',dim,'runlength',1,'other',probother);
  valuerange = -.8:.3:1.9;
  problem = @(x,runlength,seed,other) -1*RosenbrockXieFrazierChick(x,runlength,seed,other);
  budget = 1:140;
  % This is treatment 'e' from experiment_20140801.
  other = struct('n1',12*dim,'n2',2*dim,'rescale',2,...
  'NumRandomStartS',4,'NumRandomStartP',2,'NumRandomScreenS',1000,'NumRandomScreenP',500,...
  'UpdateTimes',[30,60,100,150,200:100:budget(end)],'UseFirstStageSamples',0);
  solverparam = struct('numinitsols',0,'numfinalsols',length(budget),'other',other);

  % Run the algorithm.
  seed = 1;
  solutions = AKG2(x0,budget,problem,valuerange,probparam,solverparam,seed,seed);

  if (all(solutions == old.solutions(budget,:)))
     disp('test2 OK');
  else
     disp('Solutions from the current version of the code:');
     disp(solutions);
     disp('Solutions from the old version of the code:');
     disp(old.solutions);
     disp('Solutions differ at these time indices:')
     disp(find(sum(solutions')~=sum(old.solutions(budget,:)')));
     disp('test2 FAIL');
  end
end
