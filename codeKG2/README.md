"Algorithm" contains major functions called by KG/KG2 algorithms, including Bayesian updates, MLE, VOI & gradient calculation.

"Auxiliary" contains other supporting functions for the algorithms.

"100kGrid" contains code/data/figures for the 100-alternative grid problem in the paper.

"Rosenbrock" contains code/data/figures for the 10^6-alternative Rosenbrock problem in the paper.

"ATO" contains files for the Assemble to Order problem at SimOpt.org.

"ShowKG" contains figures from a random KG and a random KG2 sample path on the 100-alternative grid problem (used for animation in slides), and code/data for generating these figures.

"Miscellaneous" includes some code for future research.

"AKG.m": KG algorithm for solving problems like ATO.
"AKG2.m": KG2 algorithm for solving problems like ATO.

"ATOscript.m": script for implementing KG/KG2 algorithms on the ATO problem.
"RosenbrockScript.m": script for implementing KG/KG2 on the Rosenbrock problem from simopt, Rosenbrock_SimOpt.  This is different from scripts used to generate the figures in the version of the paper originally submitted, which used the files in Rosenbrock/.
