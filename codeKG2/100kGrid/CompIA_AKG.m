function [OC,F,SF] = CompIA_AKG(rep,NumStarts)
load('CompIA.mat')
beta=1;%N=100;
OC = zeros(1,N);
F = zeros(1,N);
SF = zeros(1,N);
mu0 = 0;
Dim = 1;
%for rep = 1:REP
theta=(Theta(rep,:))';
Domain = Domain(:);
options = optimset('GradObj','on','Algorithm','active-set','TolFun',1e-8);%,'Algorithm','active-set','MaxFunEvals',1000);

IND = randi(LD,[1 Dim]);
SecondStageAlts=Domain(IND);
SecondStageObs=normrnd(theta(IND),sqrt(SampVar));
sigma0=PriorCov;

for n = 1:N  
    L=eye(n)*SampVar;
    L=sparse(L);

    Yt=SecondStageObs-mu0;
    invS=inv(sigma0+L);

    [Mu,sigma]=Update_Kalman(mu0,sigma0,n,invS,Yt);

    x01=ChooseRandomElement(find(Mu==max(Mu)));
    CurrentBest=SecondStageAlts(x01,:);
    
    Bests= sum(abs(SecondStageAlts-repmat(CurrentBest,n,1)),2)==0;
    temp=1:n;temp(Bests)=[];
    
    OC(n)=max(theta)-theta(IND(x01));
    
    if (sum(isnan(Mu))>0)
        Cofx=Domain(randi(LD,[1 Dim]));
    else
        if ~isempty(temp)
            if NumStarts>2
                x02=temp(ChooseRandomElement(find(Mu(temp)==max(Mu(temp)))));
                SecondBest=SecondStageAlts(x02,:);
                if sum(sum(abs(SecondStageAlts(temp,:)-repmat(SecondBest,length(temp),1)),2)==0)<length(temp)
                    CofX0=[CurrentBest;SecondBest;(Domain(randi(LD,[NumStarts-2 Dim])))'];
                else
                    CofX0=[CurrentBest;Domain(randi(LD,[NumStarts-1 Dim]))];
                end
                
            else if NumStarts == 2
                    CofX0=[CurrentBest;Domain(randi(LD,[NumStarts-1 Dim]))];
                else
                    CofX0=Domain(randi(LD,[NumStarts Dim]));
                end
            end
        else
            CofX0=Domain(randi(LD,[NumStarts Dim]));
        end
        
        % pay attention to matrix size when NumStarts=1or2.
    
        CofX=zeros(NumStarts,Dim);
        Fval=zeros(NumStarts,1);
    
        for start=1:NumStarts
            CofX(start,:) = fmincon(@(cc)VOI_TestSingular_Conly(cc,n,Mu,sigma,mu0,sigma0,PriorCov,PriorScale,SampVar,invS,SecondStageAlts,Yt),CofX0(start,:),[],[],[],[],Domain(ones(1,Dim)),Domain(end)*ones(1,Dim),[],options);%,'Display','Iter'));
            if (sum(isnan(CofX(start,:)))>0)
                CofX(start,:)=CofX0(start,:);
            else
                CofXD=max(min(Domain(end),CofX(start,:)),Domain(1));
                [~,Index]=min(abs(bsxfun(@minus,CofXD,Domain)));
                CofXD=Domain(Index);
                CofX(start,:)=(CofXD(:))';
            end
            Fval(start) = VOI_TestSingular_Conly(CofX(start,:),n,Mu,sigma,mu0,sigma0,PriorCov,PriorScale,SampVar,invS,SecondStageAlts,Yt);
        end
        [F(n),IS]=min(Fval);F(n)=-F(n);
        Cofx=CofX(IS,:);
    end
        disp(n)
        ind = round((Cofx-Domain(1))/.1)+1;
        IND = [IND;ind];
        AlreadySampled=find(sum(abs(SecondStageAlts-repmat(Cofx,n,1)),2)==0);

        SF(n) = VOIStar_Singular(Cofx,n,Mu,sigma,mu0,sigma0,PriorCov,PriorScale,SampVar,invS,SecondStageAlts,Yt,1000,SearchLength);
        y=normrnd(theta(ind),sqrt(SampVar));
        
        if (~isempty(AlreadySampled))
            sigma0=[[sigma0,sigma0(:,AlreadySampled(1))];[sigma0(AlreadySampled(1),:),PriorCov]];
        else
            RU=GPCov(PriorCov,PriorScale,SecondStageAlts,Cofx);
            sigma0=[[sigma0,RU];[RU',PriorCov]];
        end
        
        SecondStageAlts=[SecondStageAlts;Cofx];
        SecondStageObs=[SecondStageObs;y];
end