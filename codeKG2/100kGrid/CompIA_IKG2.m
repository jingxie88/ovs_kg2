function [OC,F,SF] = CompIA_IKG2(rep)
load('CompIA.mat')
beta=1;
OC = zeros(1,N+2);
F = zeros(1,N+2);
SF = zeros(1,N+2);

%for rep = 1:REP
theta=(Theta(rep,:))';
Sigma=Sigma0;
mu=mu0;

n = 1;
for iter = 1:1e8
        
    Best=ChooseRandomElement(find(mu==max(mu)));
    OC(n)=max(theta)-theta(Best);
    
    gamma=KGFactor(k,mu,Sigma,Lambda,beta);
    F(n) = max(gamma(:));
    [g1,g2]=find(gamma==F(n));
        
    i=randi(length(g1));
    x = g1(i); xx = g2(i);
%     x=mod(i,k);
%     if (x==0)
%         x=k;
%     end
%     xx=ceil(i/k);
                

    if(xx==x)
        IX=[1:x-1,x+1:k];
        [~,ix]=max(mu(IX));
        A=[x,IX(ix)];

        SF(n) = VOIStar_v(x,A,mu,Sigma,Lambda,1000,SearchLength,F(n));    
        y=normrnd(theta(x),sqrt(Lambda(x,x)));
        [mu,Sigma]=Update(k,mu,Sigma,Lambda,x,beta,y);
                
        disp(n)
        n = n+1;
    else
        
        IX=1:k;IX([x,xx])=[];
        [~,ix]=max(mu(IX));
        A=[x,xx,IX(ix)];
        SF(n) = VOIStar_v([x,xx],A,mu,Sigma,Lambda,500,SearchLength,F(n)+log(2))-log(2);

        Y=(mvnrnd([theta(xx),theta(x)]',[Lambda(xx,xx),Lambda(xx,x);Lambda(x,xx),Lambda(x,x)]))';
        X=zeros(2,k);
        X(1,xx)=1;
        X(2,x)=1;
        [mu,Sigma]=Update(k,mu,Sigma,Lambda,X,beta,Y);
        disp([n n])
        OC(n+1) = OC(n);
        F(n+1) = F(n); SF(n+1) = SF(n);
        n = n+2;
    end
    if n >= N+2
        break;
    end
end