function t = CompIA_theta(x,rep)
load('CompIA.mat')
if isnan(x)
    t = max(Theta(rep,:));
else
    t=Theta(rep,round((x+5)*10)+1);
end
