Domain = -5:.1:4.9;
LD=length(Domain);
k=LD;
SampVar=50;
SampCorr = 0.5;
PriorCov=100;PriorScale=.5;
Lambda=SampVar*(SampCorr*ones(k,k)+(1-SampCorr)*eye(k));

Alts=Domain';
Sigma0=GPCov(PriorCov,PriorScale,Alts,Alts);
beta=1;
mu0=zeros(k,1);

%REP = 5000; N = 500;
REP = 50; N = 500;
SearchLength = 30;
%plot(1:100,mvnrnd(zeros(k,1),Sigma0),'color','r','LineWidth',2)
OC_I = zeros(REP,N+1);
F_I = zeros(REP,N+1);
SF_I = zeros(REP,N+1);
Theta = zeros(REP,k);

for rep = 1:REP
    Theta(rep,:)=mvnrnd(zeros(k,1),Sigma0);
    disp(rep)
end
HetVar = mvnrnd(zeros(k,1),GPCov(2000,PriorScale,Alts,Alts));
HetVar = HetVar-min(HetVar)+1;
plot(HetVar)

CheckParallel = ver('distcomp');        % Hope this works!
if ~isempty(CheckParallel) % if parallelism is allowed, run some reps parallel
    matlabpool open local 4
    parfor rep = 1:REP
        [OC,F,SF] = CompIA_IKG(rep);
        OC_I(rep,:) = OC;
        F_I(rep,:) = F;
        SF_I(rep,:) = SF;
        disp(rep)
    end
    matlabpool close
else
    for rep = 1:REP
        [OC,F,SF] = CompIA_IKG(rep);
        OC_I(rep,:) = OC;
        F_I(rep,:) = F;
        SF_I(rep,:) = SF;
        disp(rep)
    end    
end

OC_I2_GPSC = [];F_I2_GPSC = [];SF_I2_GPSC = [];
OC_A2_GPSC = [];F_A2_GPSC = [];
OC_AE_GPSC = [];F_AE_GPSC = [];par=[];
for rep=3:2000
    [t1,t2,t3]=CompIA_IKG2(rep);
OC_I2_GPSC=[OC_I2_GPSC;t1];
F_I2_GPSC=[F_I2_GPSC;t2];
SF_I2_GPSC=[SF_I2_GPSC;t3];
[t1,t2]=CompIA_AKG2M(rep,2,2);
OC_A2_GPSC=[OC_A2_GPSC;t1];
F_A2_GPSC=[F_A2_GPSC;t2];
[t1,t2,pc,ps,sv,sc]=ImyAKG2_GPSampCorr(500,@CompIA_theta,1,-5:.1:4.9,50,3.6,10,2,2,2,7,rep);
OC_AE_GPSC=[OC_AE_GPSC;t1];
F_AE_GPSC=[F_AE_GPSC;t2];
par=[par;pc,ps,sv,sc];
disp(rep)
save('CompIA_GPSampCorr')
end
plot(1:500,mean(OC_I2_GPSC(:,2:end-1)))
plot(2:500,mean(OC_A2_GPSC(:,2:500)))
plot(14:500,mean(OC_AE_GPSC(:,2:488)))

OC_A2 = zeros(REP,N+2);
F_A2 = zeros(REP,N+2);
SF_A2 = zeros(REP,N+2);
for rep_A2 = 98:1000%REP
    [OC,F,SF] = CompIA_AKG2(rep_A2,3);
    OC_A2(rep_A2,:) = OC;
    F_A2(rep_A2,:) = F;
    SF_A2(rep_A2,:) = SF;
    disp(rep_A2)
    save('CompIA_AKG2.mat','OC_A2','F_A2','SF_A2','rep_A2')
end

OC_A25 = zeros(REP,N+2);
F_A25 = zeros(REP,N+2);
SF_A25 = zeros(REP,N+2);
%for count = 0:39
%par
for rep_A25 = 73:2000%2+count*50:1+(count+1)*50%REP
    [OC,F,SF] = CompIA_AKG2M(rep_A25,2,2);
    OC_A25(rep_A25,:) = OC;
    F_A25(rep_A25,:) = F;
    SF_A25(rep_A25,:) = SF;
%end
save('CompIA_AKG25.mat','OC_A25','F_A25','SF_A25','rep_A25')
end

OC_AE = zeros(REP,N-11);
F_AE = zeros(REP,N-11);
SF_AE = zeros(REP,N-11);
PC=[];PS=[];SV=[];SC=[];
for rep_AE = 2:2000
    [OC,F,SF,pc,ps,sv,sc] = ImyAKG2(500,@CompIA_theta,1,-5:.1:4.9,50,.5,10,2,2,2,7,rep_AE);
    OC_AE(rep_AE,:) = OC;
    F_AE(rep_AE,:) = F;
    SF_AE(rep_AE,:) = SF;
    PC=[PC,pc];PS=[PS,ps];SV=[SV,sv];SC=[SC,sc];
    save('CompIA_AE.mat','OC_AE','F_AE','SF_AE','rep_AE')
end

OC_A = cell(1,3);
F_A = cell(1,3);
SF_A = cell(1,3);
OC_A2{3} = OC_A23;
F_A2{3} = F_A23;
SF_A2{3} = SF_A23;

for rep_A = 1:REP
    [OC,F,SF] = CompIA_AKG(rep_A,1);
    OC_A{1}(rep_A,:) = OC;
    F_A{1}(rep_A,:) = F;
    SF_A{1}(rep_A,:) = SF;
    
    [OC,F,SF] = CompIA_AKG(rep_A,2);
    OC_A{2}(rep_A,:) = OC;
    F_A{2}(rep_A,:) = F;
    SF_A{2}(rep_A,:) = SF;
    
    [OC,F,SF] = CompIA_AKG(rep_A,3);
    OC_A{3}(rep_A,:) = OC;
    F_A{3}(rep_A,:) = F;
    SF_A{3}(rep_A,:) = SF;
    
    disp(rep_A)
    save('CompIA_AKG.mat','OC_A','F_A','SF_A','rep_A')
end

OC_hessian = [];
OC2_hessian = [];

for rep = 1002:2000
    OC_hessian = [OC_hessian;CompIA_AKG_Hessian(rep,3)];
    OC2_hessian = [OC2_hessian;CompIA_AKG2M_Hessian(rep,2,2)];
    disp(ones(1,10)*rep)
    save('CompIA__Hessian.mat','OC_hessian','OC2_hessian','rep')
end


max([min(F_I(:,2:501),[],2);min(SF_I(:,2:501),[],2);...
    min(F_I2(1:2500,2:500),[],2);min(SF_I2(1:2500,2:500),[],2);...
    min(F_A2{1}(:,2:499),[],2);min(SF_A2{1}(:,2:499),[],2);...
    min(F_A2{2}(:,2:499),[],2);min(SF_A2{2}(:,2:499),[],2);...
    min(F_A2{3}(:,2:499),[],2);min(SF_A2{3}(:,2:499),[],2)])

min([max(F_I(:,2:501),[],2);max(SF_I(:,2:501),[],2);...
    max(F_I2(1:2500,2:500),[],2);max(SF_I2(1:2500,2:500),[],2);...
    max(F_A2{1}(:,2:499),[],2);max(SF_A2{1}(:,2:499),[],2);...
    max(F_A2{2}(:,2:499),[],2);max(SF_A2{2}(:,2:499),[],2);...
    max(F_A2{3}(1:rep_A23,2:499),[],2);max(SF_A2{3}(1:rep_A23,2:499),[],2)])


logC = 0:-.1:-8.3;

OCS_I = zeros(REP,length(logC));
OCS_IS = zeros(REP,length(logC));
T_I = zeros(REP,length(logC));
T_IS = zeros(REP,length(logC));

for i=1:length(logC)
    for j=1:5000
        temp = find(F_I(j,2:end)<logC(i),1);
        if ~isempty(temp)
            T_I(j,i)= temp;
        else
            T_I(j,i)= 500;
        end
        OCS_I(j,i)=OC_I(j,T_I(j,i)+1);
        temp = find(SF_I(j,2:end)<logC(i),1);
        if ~isempty(temp)
            T_IS(j,i) = temp;
        else
            T_IS(j,i) = 500;
        end
        OCS_IS(j,i)=OC_I(j,T_IS(j,i)+1);
    end
end

OCS_I2 = zeros(2500,length(logC));
OCS_I2S = zeros(2500,length(logC));
T_I2 = zeros(2500,length(logC));
T_I2S = zeros(2500,length(logC));

for i=1:length(logC)
    for j=1:2500
        temp = find(F_I2(j,2:end)<logC(i),1);
        if ~isempty(temp) && temp <= 500
            T_I2(j,i)= temp;
        else
            T_I2(j,i)= 500;
        end
        OCS_I2(j,i)=OC_I2(j,T_I2(j,i)+1);
        temp = find(SF_I2(j,2:end)<logC(i),1);
        if ~isempty(temp) && temp <= 500
            T_I2S(j,i) = temp;
        else
            T_I2S(j,i) = 500;
        end
        OCS_I2S(j,i)=OC_I2(j,T_I2S(j,i)+1);
    end
end

OCS_A2 = cell(1,3);
T_A2 = cell(1,3);
OCS_A2S = cell(1,3);
T_A2S = cell(1,3);

for i=1:length(logC)
    for j=1:size(OC_A2{1},1)
        temp = find(F_A2{1}(j,2:end)<logC(i),1)+1;
        if ~isempty(temp) && temp <= 500
            T_A2{1}(j,i)= temp;
        else
            T_A2{1}(j,i)= 500;
        end
        OCS_A2{1}(j,i)=OC_A2{1}(j,T_A2{1}(j,i));
        
        temp = find(SF_A2{1}(j,2:end)<logC(i),1)+1;
        if ~isempty(temp) && temp <= 500
            T_A2S{1}(j,i) = temp;
        else
            T_A2S{1}(j,i) = 500;
        end
        OCS_A2S{1}(j,i)=OC_A2{1}(j,T_A2S{1}(j,i));
    end
    for j=1:size(OC_A2{2},1)
        temp = find(F_A2{2}(j,2:end)<logC(i),1)+1;
        if ~isempty(temp) && temp <= 500
            T_A2{2}(j,i)= temp;
        else
            T_A2{2}(j,i)= 500;
        end
        OCS_A2{2}(j,i)=OC_A2{2}(j,T_A2{2}(j,i));
        
        temp = find(SF_A2{2}(j,2:end)<logC(i),1)+1;
        if ~isempty(temp) && temp <= 500
            T_A2S{2}(j,i) = temp;
        else
            T_A2S{2}(j,i) = 500;
        end
        OCS_A2S{2}(j,i)=OC_A2{2}(j,T_A2S{2}(j,i));
    end
    for j=1:size(OC_A2{3},1)
        temp = find(F_A2{3}(j,2:end)<logC(i),1)+1;
        if ~isempty(temp) && temp <= 500
            T_A2{3}(j,i)= temp;
        else
            T_A2{3}(j,i)= 500;
        end
        OCS_A2{3}(j,i)=OC_A2{3}(j,T_A2{3}(j,i));
        
        temp = find(SF_A2{3}(j,2:end)<logC(i),1)+1;
        if ~isempty(temp) && temp <= 500
            T_A2S{3}(j,i) = temp;
        else
            T_A2S{3}(j,i) = 500;
        end
        OCS_A2S{3}(j,i)=OC_A2{3}(j,T_A2S{3}(j,i));
    end
    
end
plot(14:500,mean(modified1{2}(:,2:end-1)),'r')
plot(14:500,mean(modified{2}(end-699:end,2:end-1)),'k')
plot(14:500,mean(modified{2}(:,2:end-1)),'r')

plot(1:500,mean(OC_I(:,2:end)),'k')
hold on
h2=plot(1:N,mean(OC_I2(1:2500,2:end-1)),'r')
h1=plot(1:N,mean(OC_I25(:,2:end-1)),'r')
legend([h2,h1],'\rho=0.5','\rho=0.25')

plot(2:500,mean(OC_A2{1}(:,2:500)))
plot(2:500,mean(OC_A2{2}(:,2:500)),'g')
plot(2:500,mean(OC_A2{3}(:,2:500)),'m')
plot(2:500,mean(OC_A2{4}(:,2:500)),'m')

plot(2:500,mean(OC2_hessian(:,2:500)),'m')
plot(2:500,mean(OC2_hessian3(:,2:500)),'m')
plot(2:500,mean(OC2_hessian5(:,2:500)),'m')
plot(2:500,mean(OC2_hessian10(:,2:500)),'m')

plot(1:500,mean(OC_A{1}))
plot(1:500,mean(OC_A{2}),'g')
plot(1:500,mean(OC_A{3}),'m')
plot(13:500,mean(OC_AA),'m')

plot(14:500,mean(OC_AE(1:2000,2:end-1)))
plot(2:500,mean(OC_A25(1:2000,2:500)))
plot(14:500,mean(OC_AE25(:,2:end-1)))

plot(mean(T_I),mean(OCS_I),'k')
plot(mean(T_IS),mean(OCS_IS),'k')
plot(mean(T_I2),mean(OCS_I2),'m')
plot(mean(T_I2S),mean(OCS_I2S),'m')
plot(mean(T_A2{1}),mean(OCS_A2{1}))
plot(mean(T_A2S{1}),mean(OCS_A2S{1}))
plot(mean(T_A2{2}),mean(OCS_A2{2}),'g')
plot(mean(T_A2S{2}),mean(OCS_A2S{2}),'g')
plot(mean(T_A2{3}),mean(OCS_A2{3}),'m')
plot(mean(T_A2S{3}),mean(OCS_A2S{3}),'m')

plot(1:length(logC),exp(logC).*mean(T_I)+mean(OCS_I),'k')
plot(1:length(logC),exp(logC).*mean(T_IS)+mean(OCS_IS))
plot(1:length(logC),exp(logC).*mean(T_I2)+mean(OCS_I2),'m')
plot(1:length(logC),exp(logC).*mean(T_I2S)+mean(OCS_I2S),'m')
plot(1:length(logC),exp(logC).*mean(T_A2{1})+mean(OCS_A2{1}))
plot(1:length(logC),exp(logC).*mean(T_A2S{1})+mean(OCS_A2S{1}))
plot(1:length(logC),exp(logC).*mean(T_A2{2})+mean(OCS_A2{2}),'g')
plot(1:length(logC),exp(logC).*mean(T_A2S{2})+mean(OCS_A2S{2}),'g')
plot(1:length(logC),exp(logC).*mean(T_A2{3})+mean(OCS_A2{3}),'m')
plot(1:length(logC),exp(logC).*mean(T_A2S{3})+mean(OCS_A2S{3}),'m')
plot(1:length(logC),min((exp(logC))'*(1:N)+repmat(mean(OC_I(:,2:end)),length(logC),1),[],2),'r')
plot(1:length(logC),min((exp(logC))'*(1:N)+repmat(mean(OC_I2(1:2500,2:end-1)),length(logC),1),[],2),'r')

plot(1:N,mean(F_I(:,2:end)))
hold on
plot(1:N,mean(F_I2(1:550,2:end-1)))
plot(1:N,mean(SF_I(:,2:end)))
hold on
plot(1:N,mean(SF_I2(1:550,2:end-1)))
subplot(2,2,1)
hist(par(:,1),20)
hold on
plot(100,0,'or','MarkerSize',5)
subplot(2,2,2)
hist(par(:,2),50)
%axis([0 1.5],'auto y')
hold on
plot(.5,0,'or','MarkerSize',5)
subplot(2,2,3)
hist(par(:,3),20)
hold on
plot(50,0,'or','MarkerSize',5)
subplot(2,2,4)
hist(par(:,4),20)
hold on
plot(.5,0,'or','MarkerSize',5)

plot(mean(OC_batch3))
hold on
plot(mean(OC_batch10),'k')
plot(mean(OC_esthv),'r')
plot(mean(OC_b3knhv),'m')
plot(mean(OC_b3knhv_k),'r')
plot(mean(OC_esthv_batch),'g')