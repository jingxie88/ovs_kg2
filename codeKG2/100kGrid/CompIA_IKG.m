function [OC,F,SF] = CompIA_IKG(rep)
load('CompIA.mat')
beta=1;
OC = zeros(1,N+1);
F = zeros(1,N+1);
SF = zeros(1,N+1);

%for rep = 1:REP
theta=(Theta(rep,:))';
Sigma=Sigma0;
mu=mu0;

for n = 1:N+1
        
    Best=ChooseRandomElement(find(mu==max(mu)));
    OC(n)=max(theta)-theta(Best);

    gamma=IKGFactor(k,mu,Sigma,Lambda,beta);
    F(n) = max(gamma);
    x=ChooseRandomElement(find(gamma==F(n)));
        
    IX=[1:x-1,x+1:k];
    [~,ix]=max(mu(IX));
    A=[x,IX(ix)];
    SF(n) = VOIStar_v(x,A,mu,Sigma,Lambda,1000,SearchLength,F(n));    
    
    y=normrnd(theta(x),sqrt(Lambda(x,x)));
    [mu,Sigma]=Update(k,mu,Sigma,Lambda,x,beta,y);
end