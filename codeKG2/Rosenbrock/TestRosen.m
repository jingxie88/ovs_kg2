plot(72:1000,mean(OC2(:,2:end-1)),'r')
hold on
plot(71:1000,mean(OC),'k')
plot(72:1000,mean(OCRS2(:,2:end-1)))
plot(71:1000,mean(OCRS),'g')

plot(72:1000,mean((F2(:,2:end-1)-SF2(:,2:end-1))./SF2(:,2:end-1)))

tmp=OC2(:,2:end-1);tmpp = mean(tmp);
for i=1:length(tmpp)
    t=tmp(:,i);
    tmpp(i)=mean(t(t<inf));
end
% tmpp=tmpp-[zeros(1,212),ones(1,length(tmpp)-212)*.8];
% tmpp=tmpp-[zeros(1,325),ones(1,length(tmpp)-325)*.3];
% tmpp=tmpp-[zeros(1,498),ones(1,length(tmpp)-498).*(1:(-2/(length(tmpp)-498-1)):-1)];
plot(72:1000,tmpp,'r')

tmp=OC;tmpp = mean(tmp);
for i=1:length(tmpp)
    t=tmp(:,i);
    tmpp(i)=mean(t(t<inf));
end
hold on
plot(71:1000,tmpp,'k')
tmp=OCRS2(:,2:end-1);tmpp = mean(tmp);
for i=1:length(tmpp)
    t=tmp(:,i);
    tmpp(i)=mean(t(t<inf));
end
plot(72:1000,tmpp)
tmp=OCRS;tmpp = mean(tmp);
for i=1:length(tmpp)
    t=tmp(:,i);
    tmpp(i)=mean(t(t<inf));
end
plot(71:1000,tmpp,'g')

for i=1:6
    subplot(6,1,i)
    plot(EB{20}(i,1:500),'r')
    hold on
    plot(X{20}(71:end,i))
    axis([0 500 -1 2])
end
subplot(7,1,1)
plot(SF{10}(1,:)-SF{10}(3,:),'g')
hold on
plot(SF{10}(2,:)-SF{10}(3,:))
plot(F(378,:)-SF{10}(3,:),'r')
plot(zeros(1,1000))

T=inf;N=1000;Dim=6;fun=@rosen;Domain=-.8:.3:1.9;
SampVar=100;n1=50;n2=10;NumStarts=3;
