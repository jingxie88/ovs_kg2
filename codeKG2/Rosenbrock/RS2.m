function [OC,PriorCov,PriorScale,SampVarHat,SampCorrHat] = RS2(T,N,fun,Dim,Domain,SampVar,SampCorr,n1,n2)%,NumUpdates)
tic
N1=n1+n2;

OC = inf*ones(1,N+1-N1);

LD=length(Domain);
%Domain = Domain(:);
scale0=abs(Domain(end)-Domain(1))*ones(1,Dim);

FirstStageAlts1=Domain(randi(LD,[n1 Dim]));
if n1 == 1
    FirstStageAlts1=(FirstStageAlts1(:))';
else if Dim == 1
        FirstStageAlts1=FirstStageAlts1(:);
    end
end
FirstStageCov1=SampVar*(eye(n1)*(1-SampCorr)+ones(n1)*SampCorr);
FirstStageObs1=(mvnrnd(-feval(fun,FirstStageAlts1,Dim),FirstStageCov1))';%'
[~,Best]=sort(FirstStageObs1,'descend');
FirstStageAlts2=FirstStageAlts1(Best(1:n2),:);
FirstStageObs2=(mvnrnd(-feval(fun,FirstStageAlts2,Dim),FirstStageCov1(Best(1:n2),Best(1:n2))))';%'

FirstStageAlts=[FirstStageAlts1;FirstStageAlts2];
FirstStageObs=[FirstStageObs1;FirstStageObs2(:)];

FL=zeros(N1,N1);
FL(1:n1,1:n1)=ones(n1)-eye(n1);
FL(n1+1:end,n1+1:end)=ones(n2)-eye(n2);

[PriorCov,SampVarHat,PriorScale,SampCorrHat,mu0]= MLEofHyperparameters(FirstStageAlts,FirstStageObs,FL,.5,.5,scale0,scale0);

SecondStageAlts=Domain(randi(LD,[2 Dim]));
if Dim == 1
    SecondStageAlts = SecondStageAlts(:);
end
SecondStageObs=(mvnrnd(-feval(fun,SecondStageAlts,Dim),SampVar*[1,SampCorr;SampCorr,1]))';
L=SampVarHat*[1,SampCorrHat;SampCorrHat,1];
LI=[1,2;2,1];
temp=GPCov(PriorCov,PriorScale,SecondStageAlts(1,:),SecondStageAlts(2,:));
sigma0=[PriorCov,temp;temp,PriorCov];

%Update = [30,60,round(exp(log(90):(log(N-N1-1)-log(90))/(NumUpdates-3):log(N-N1-1)))];
Update=[30,60,92,140,214,327,500];
UpInd = 1;
n = 2;

while toc < T  
    
    if UpInd<=length(Update)&&n>=Update(UpInd)
        AllAlts=[FirstStageAlts;SecondStageAlts];
        AllObs=[FirstStageObs;SecondStageObs];
        AL=full([FL,zeros(N1,n);zeros(n,N1),sparse(LI(1,:),LI(2,:),ones(1,size(LI,2)),n,n)]);
        [PriorCov,SampVarHat,PriorScale,SampCorrHat,mu0]= MLEofHyperparameters(AllAlts,AllObs,AL,.5,.5,scale0,scale0);
        UpInd=UpInd+1;
        sigma0=eye(n);
        for i=2:n
            sigma0(i,1:i-1)=GPCov(1,PriorScale,SecondStageAlts(i,:),SecondStageAlts(1:i-1,:));
            sigma0(1:i-1,i)=sigma0(i,1:i-1)';%'
        end
        sigma0=sigma0*PriorCov;
        L = SampVarHat*(eye(n)+AL(end-n+1:end,end-n+1:end)*SampCorrHat);
        clear AL        
    end

    Yt=SecondStageObs-mu0;
    invS=inv(sigma0+L);

    [Mu,~]=Update_Kalman(mu0,sigma0,n,invS,Yt);

    x01=ChooseRandomElement(find(Mu==max(Mu)));
    CurrentBest=SecondStageAlts(x01,:);
        
    OC(n)=feval(fun,CurrentBest,Dim);
        
    Cofx=Domain(randi(LD,[2 Dim]));
    if Dim == 1
        Cofx=Cofx(:);
    end
    
    if size(Cofx,1)==1 ||(sum(Cofx(1,:)==Cofx(2,:))==Dim)
        disp(n)
        Cofx=Cofx(1,:);
        AlreadySampled=find(sum(abs(SecondStageAlts-repmat(Cofx,n,1)),2)==0);

        %SF(n) = VOIStar_Singular(Cofx,n,Mu,sigma,mu0,sigma0,PriorCov,PriorScale,SampVarHat,invS,SecondStageAlts,Yt,1000,SearchLength);
        y=normrnd(-feval(fun,Cofx,Dim),sqrt(SampVar));
        n = n+1;
    
        if (n>N-N1)
            break;    
        else

        if (~isempty(AlreadySampled))
            sigma0=[[sigma0,sigma0(:,AlreadySampled(1))];[sigma0(AlreadySampled(1),:),PriorCov]];
        else
            RU=GPCov(PriorCov,PriorScale,SecondStageAlts,Cofx);
            sigma0=[[sigma0,RU];[RU',PriorCov]];
        end
        
        SecondStageAlts=[SecondStageAlts;Cofx];
        SecondStageObs=[SecondStageObs;y];

        L=[[L,zeros(n-1,1)];[zeros(1,n-1),SampVarHat]];
        L=sparse(L);
        end
    else
        disp([n,n])
        %SF(n) = VOIStar_Pair_HomoCorr(Cofx,n,Mu,sigma,mu0,sigma0,PriorCov,PriorScale,SampVarHat,SampCorrHat,invS,SecondStageAlts,Yt,500,SearchLength);           
        OC(n+1) = OC(n);
        n = n+2;

        if n>N-N1
            break;
        else
            
            Y=(mvnrnd(-feval(fun,Cofx,Dim),SampVar*[1,SampCorr;SampCorr,1]))';
            SecondStageObs=[SecondStageObs;Y];

            AlreadySampled=zeros(1,2);

            for j=1:2
                temp=find(sum(abs(SecondStageAlts-repmat(Cofx(j,:),n-2,1)),2)==0);
                if (~isempty(temp))
                    AlreadySampled(j)=temp(1);
                end
            end
            
            temp=GPCov(PriorCov,PriorScale,Cofx(1,:),Cofx(2,:));
            RD=[PriorCov,temp;temp,PriorCov];
            RD3=SampVarHat*[1,SampCorrHat;SampCorrHat,1];

            if (AlreadySampled(1)~=0)
                if (AlreadySampled(2)~=0)
                    sigma0=[[sigma0,sigma0(:,[AlreadySampled(1),AlreadySampled(2)])];[sigma0([AlreadySampled(1),AlreadySampled(2)],:),RD]];
                else
                    RU=GPCov(PriorCov,PriorScale,SecondStageAlts,Cofx(2,:));
                    TEMP=[sigma0(:,AlreadySampled(1)),RU];
                    sigma0=[[sigma0,TEMP];[TEMP',RD]];%'
                end
            else if (AlreadySampled(2)~=0)
                    RU=GPCov(PriorCov,PriorScale,SecondStageAlts,Cofx(1,:));
                    TEMP=[RU,sigma0(:,AlreadySampled(2))];
                    sigma0=[[sigma0,TEMP];[TEMP',RD]];
                else
                    RU=GPCov(PriorCov,PriorScale,SecondStageAlts,Cofx);
                    sigma0=[[sigma0,RU];[RU',RD]];
                end
            end
            
            SecondStageAlts=[SecondStageAlts;Cofx];
            L=[[L,zeros(n-2,2)];[zeros(2,n-2),RD3]];
            L=sparse(L);
            LI=[LI,[n-1,n;n,n-1]];
        end
    end
end