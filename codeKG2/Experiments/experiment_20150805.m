% This function is equivalent to experiment_20140801, except that it stores the
% debugging output of the AKG2 algorithm.
function experiment_20150805(seed,treatment,do_addpath,outfile)
  if (nargin < 3 || do_addpath)
    % directory in which the repository lives.
    dir = find_repository_dir();
    addpath(genpath(dir)); % Make sure all needed commands are in the path.

    % directory in which the data will be stored 
    data_dir = sprintf('%s/Experiments/data',dir);
    if (exist(data_dir,'dir')~=7)
      error(sprintf('Data directory %s does not exist.', data_dir));
    end
  else
    if (nargin < 4)
      error('If do_addpath is set, we require that outfile also be passed.');
    end
  end

  % file where we will save the output.
  if (nargin < 4)
    outfile = sprintf('%s/Experiments/data/20150805%s_%05d.mat',dir,treatment,seed); 
  end
  if (exist(outfile,'file')==2)
    error(sprintf('Data file %s already exists.', outfile));
  end

  q = 6;% this is a parameter of the Rosenbrock function determining its dimension.
  dim=q;
  x0 = []; % no initial samples.

  % Independent random number stream used by RosenbrockXieChickFrazier, passed
  % throuh probother.  The solver uses stream 1, both for its own randomness
  % and when calling the problem function handle.  We use stream 2 for this
  % independent random number stream.  I checked whether it is ok to do this,
  % i.e., whether you can call RandStream.create with 'NumStream' set to 1, and
  % then at another time call it with 'NumStream' set to 2 and get the second
  % stream, and have those two streams be indpendent.  I did this with the
  % following code:
  %   s2 = RandStream.create('mrg32k3a','NumStreams',2,'StreamIndices',2);
  %   r2 = rand(s2,100000,1);
  %   s1 = RandStream.create('mrg32k3a','NumStreams',1);
  %   r1 = rand(s1,100000,1);
  %   corrcoef([r1,r2])
  %   ans =
  %      1.0000    0.0016
  %      0.0016    1.0000
  stream = RandStream.create('mrg32k3a', 'NumStreams',2,'StreamIndices',2); 

  probother = struct('q',q,'stream',stream);
  probparam = struct('dim',dim,'runlength',1,'other',probother);
  valuerange = -.8:.3:1.9;
  % Our problem function handle returns the negative function value of the
  % RosenbrockXieFrazierChick function. Maximizing this is the same as
  % minimizing RosenbrockXieFrazierChick.
  problem = @(x,runlength,seed,other) -1*RosenbrockXieFrazierChick(x,runlength,seed,other);

  if treatment == 'a'
    %KG2, Jing's configuration, with screening and a different update schedule.
    % Jing told me in an email in Sep 2014 that she set n1 to 12.
    % She said that she tried rescale=1, rescale=2 and rescale=sqrt(2).
    budget = 1:1000;
    other = struct('n1',12*dim,'n2',2*dim,'rescale',2,...
    'NumRandomStartS',4,'NumRandomStartP',2,'NumRandomScreenS',1000,'NumRandomScreenP',500,...
    'UpdateTimes',[30,60,100,150,200:100:budget(end)],'UseFirstStageSamples',0);
    solverparam = struct('numinitsols',0,'numfinalsols',length(budget),'other',other);
  elseif treatment == 'b'
    %KG2 without screening, and with the default update schedule. 
    budget = 1:1000;
    other = struct('NumRandomStartS',4,'NumRandomStartP',2,'UseFirstStageSamples',0);
    solverparam = struct('numinitsols',0,'numfinalsols',length(budget),'other',other);
  elseif treatment == 'c'
    %KG2 with 1 random restart.
    budget = 1:1000;
    other = struct('NumRandomStartS',1,'NumRandomStartP',1,'UseFirstStageSamples',0);
    solverparam = struct('numinitsols',0,'numfinalsols',length(budget),'other',other);
  elseif treatment == 'd'
    % KG with 1 random restart.
    budget = 1:1000;
    other = struct('NumRandomStartS',1,'NumRandomStartP',0,'IncludeBestSecondBestPair',0,'UseFirstStageSamples',0);
    solverparam = struct('numinitsols',0,'numfinalsols',length(budget),'other',other);
  elseif treatment == 'e'
    % Jing's configuration of KG2 using a budget of 500, to make it faster than
    % 1000, but allow us to look for the "bump"
    budget = 1:500;
    other = struct('n1',12*dim,'n2',2*dim,'rescale',2,...
    'NumRandomStartS',4,'NumRandomStartP',2,'NumRandomScreenS',1000,'NumRandomScreenP',500,...
    'UpdateTimes',[30,60,100,150,200:100:budget(end)],'UseFirstStageSamples',0);
    solverparam = struct('numinitsols',0,'numfinalsols',length(budget),'other',other);
  elseif treatment == 'f' % KG2, one random start, budget of 500
    budget = 1:500;
    other = struct('NumRandomStartS',1,'NumRandomStartP',1,'UseFirstStageSamples',0);
    solverparam = struct('numinitsols',0,'numfinalsols',length(budget),'other',other);
  elseif treatment == 'g' % KG2, with one pair obtained by doing a local search from the best/second-best pair, and no random pairs.
    budget = 1:500;
    other = struct('NumRandomStartS',1,'NumRandomStartP',0,'UseFirstStageSamples',0);
    solverparam = struct('numinitsols',0,'numfinalsols',length(budget),'other',other);
  elseif treatment == 'h' % KG2, 10 random starts because the 0803 experiment suggested this would be good, budget of 500
    budget = 1:500;
    other = struct('NumRandomStartS',10,'NumRandomStartP',10,'UseFirstStageSamples',0);
    solverparam = struct('numinitsols',0,'numfinalsols',length(budget),'other',other);
  elseif treatment == 'i' % RSGP (random search with gaussian process).
    % RSGP is achieved by turning off local search, having no pairs (either
    % random or from include best/second best), turning off inclusion of the
    % best and second-best singleton, and having one random starting singleton.
    % Note that we still have a first stage of samples in this configuration.
    budget = 1:1000;
    other = struct('DoLocalSearch',0,'NumRandomStartS',1,'NumRandomStartP',0,...
      'IncludeBestSingleton',0,'IncludeSecondBestSingleton',0,'IncludeBestSecondBestPair',0,...
      'UseFirstStageSamples',0);
    solverparam = struct('numinitsols',0,'numfinalsols',length(budget),'other',other);
  elseif treatment == 'j'
    % KG2 without screening, and with the default update schedule. 
    % Like treatment b, but with only 200 samples, and UseFirstStageSamples turned on.
    budget = 1:200;
    other = struct('NumRandomStartS',4,'NumRandomStartP',2,'UseFirstStageSamples',1);
    solverparam = struct('numinitsols',0,'numfinalsols',length(budget),'other',other);
  elseif treatment == 'k'
    % identical to treatment b.  This treatment was added by mistake, but
    % leaving it here because we generated some seeds using it, and those seeds
    % might be useful.
    budget = 1:1000;
    other = struct('NumRandomStartS',4,'NumRandomStartP',2,'UseFirstStageSamples',0);
    solverparam = struct('numinitsols',0,'numfinalsols',length(budget),'other',other);
  elseif treatment == 'l'
    %KG2 without screening, and with the default update schedule. 
    % Like treatment b, but with UseFirstStageSamples turned on.
    % Like treatment j, but with 1000 samples.
    budget = 1:1000;
    other = struct('NumRandomStartS',4,'NumRandomStartP',2,'UseFirstStageSamples',1);
    solverparam = struct('numinitsols',0,'numfinalsols',length(budget),'other',other);
  elseif treatment == 'm'
    % Like treatment 'l', but using KG1 instead of KG2.
    budget = 1:1000;
    other = struct('NumRandomStartS',4,'NumRandomStartP',0,'IncludeBestSecondBestPair',0,'UseFirstStageSamples',1);
    solverparam = struct('numinitsols',0,'numfinalsols',length(budget),'other',other);
  elseif treatment == 'n'
    % RSGP with UseFirstStageSamples on.
    budget = 1:1000;
    other = struct('DoLocalSearch',0,'NumRandomStartS',1,'NumRandomStartP',0,...
      'IncludeBestSingleton',0,'IncludeSecondBestSingleton',0,'IncludeBestSecondBestPair',0,...
      'UseFirstStageSamples',1);
    solverparam = struct('numinitsols',0,'numfinalsols',length(budget),'other',other);
  else
    error(sprintf('Treatment %s is not valid', treatment));
  end


  % Using git, check the status of the repository, to see what version of the
  % code we are using, and whether there are any uncommitted changes.
  % We save this in the outfile.
  [exit_code, git_rev] = system('git rev-parse HEAD'); % Get the current revision.
  [exit_code, git_status] = system('git status'); % Get whether there are any uncommitted changes.

  % Actually run the algorithm.
  [solutions, times, debug] = AKG2(x0,budget,problem,valuerange,probparam,solverparam,seed,seed);

  save(outfile,'solutions','x0','budget','problem','valuerange','probparam','solverparam','seed','git_rev','git_status','treatment','debug');

end
