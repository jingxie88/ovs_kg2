% Produces a new version of figure 2 from the paper (OC under Rosenbrock vs. #
% number of samples for KG2, KG1, RSGP), but which uses first stage samples for
% all three policies. 
function fig2_UseFirstStageSamples(use_errorbars)
if (nargin<1)
    use_errorbars=0;
end

KG2_treatment='l';
KG1_treatment='m';
RSGP_treatment='n';

[KG2_mean, KG2_std, KG2_budget, KG2_values, KG2_oc] = experiment_20140801_finish_one_treatment(KG2_treatment);
[KG1_mean, KG1_std, KG1_budget, KG1_values, KG1_oc] = experiment_20140801_finish_one_treatment(KG1_treatment);
[RSGP_mean, RSGP_std, RSGP_budget, RSGP_values, RSGP_oc] = experiment_20140801_finish_one_treatment(RSGP_treatment);

linewidth=1.5;
if (use_errorbars)
  h=errorbar(RSGP_budget,RSGP_oc,2*RSGP_std,'k--','LineWidth',linewidth);
else
  h=plot(RSGP_budget,RSGP_oc,'k--','LineWidth',linewidth);
end
set(get(h,'Parent'), 'YScale', 'log') % make it a semilog plot.
set(gca,'Fontsize',18)
xlabel('Sample Size')
ylabel('Expected Opportunity Cost')
axis([0 KG2_budget(end) 1 1000])
hold on
if (use_errorbars)
  errorbar(KG1_budget,KG1_oc,2*KG1_std,'k-.','LineWidth',linewidth);
  errorbar(KG2_budget,KG2_oc,2*KG2_std,'k-','LineWidth',linewidth)
else
  plot(KG1_budget,KG1_oc,'k-.','LineWidth',linewidth);
  plot(KG2_budget,KG2_oc,'k-','LineWidth',linewidth);
end
hold off
legend('RSGP','KG_1','KG^2_1');
dir = find_repository_dir();
if use_errorbars
  print('-dpdf',sprintf('%s/Experiments/fig2_UseFirstStageSamples_Errorbars_bw.pdf',dir));
else
  print('-dpdf',sprintf('%s/Experiments/fig2_UseFirstStageSamples_NoErrorbars_bw.pdf',dir));
end
