#!/bin/sh

# Run experiment_20141021 for seeds 1 through 5 and both treatments.
# Does NOT use the NBS system.

# Get an optional argument, which is the maximum number of runs to do.
# Once we hit this maximum number of runs started, we stop.
if (( BASH_ARGC == 0 )); then
  export MAX_RUNS=10 # default value
else
  export MAX_RUNS=${1}
fi
echo "Maximum number of runs is $MAX_RUNS"

export RUNS=1 #Counts the number of runs started
for SEED in {1..5}
do
  for TREATMENT in b
  do
    export FILENAME=`printf "data/20141021%s_%05d.mat" $TREATMENT $SEED`
    if (( RUNS>MAX_RUNS )); then # Run no more than the maximum number of runs.
      break; 
    fi
    if ! [ -a $FILENAME ]; then # If the file already exists, don't need to run the experiment.
      echo "Starting treatment=$TREATMENT seed=$SEED filename=$FILENAME"
      ./experiment_20141021.nbs $SEED $TREATMENT
      RUNS=$(($RUNS+1))
    fi
  done
done
