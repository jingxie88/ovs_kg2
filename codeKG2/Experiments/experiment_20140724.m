% This code takes as argument a single seed value, runs the AKG2 algorithm on
% the Rosenbrock function from simopt, and saves the output to a file.  The
% name of the file is 20140724_seed.mat, where seed is the seed value passed to
% this function.  Given the seed, the output is deterministic.
% This Rosenbrock function is *NOT* the one that we use in our paper.

function experiment_20140724(seed)
  % directory in which the repository lives.
  dir = find_repository_dir();
  addpath(genpath(dir)); % Make sure all needed commands are in the path.
 
  % data in which the data will be stored 
  data_dir = sprintf('%s/Experiments/data',dir);
  if (exist(data_dir,'dir')~=7)
    error(sprintf('Data directory %s does not exist.', data_dir));
  end

  % file where we will save the output.
  outfile = sprintf('%s/Experiments/data/20140724_%05d.mat',dir,seed); 
  if (exist(outfile,'file')==2)
    error(sprintf('Data file %s already exists.', outfile));
  end

  q = 4;% this is a parameter of the Rosenbrock function determining its dimension.
  dim=2*q;
  x0 = 20*ones(1,dim); % sample at the point [20,..,20] initially.
  probparam = struct('dim',dim,'runlength',70,'other',q);
  valuerange = -10:10;
  % Our problem function handle returns the negative function value of the
  % Rosenbrock function. Maximizing this is the same as minimizing the Rosenbrock
  % function.
  problem = @(x,runlength,seed,other) -1*Rosenbrock(x,runlength,seed,other);
  budget = 1:200;

  other = struct('n1',10*dim,'n2',2*dim,'rescale',2,...
  'NumRandomStartS',4,'NumRandomStartP',2,'NumRandomScreenS',1000,'NumRandomScreenP',500,...
  'UpdateTimes',[30,60,100,150,200:100:budget(end)]);
  solverparam = struct('numinitsols',1,'numfinalsols',length(budget), 'other',other);

  % Using git, check the status of the repository, to see what version of the
  % code we are using, and whether there are any uncommitted changes.
  % We save this in the outfile.
  [exit_code, git_rev] = system('git rev-parse HEAD'); % Get the current revision.
  [exit_code, git_status] = system('git status'); % Get whether there are any uncommitted changes.

  % Actually run the algorithm.
  solutions = AKG2(x0,budget,problem,valuerange,probparam,solverparam,seed,seed);

  save(outfile,'solutions','x0','budget','problem','valuerange','probparam','solverparam','seed','git_rev','git_status');

end
