#!/bin/sh
# Start many worker threads, each of which will save output to a separate file
# in the directory data/.  The first argument is the seed to run, and the
# second is the treatment.
# These runs fill in seeds 1 through 100.

# Get an optional argument, which is the maximum number of runs to do.
# Once we hit this maximum number of runs started, we stop.
if (( BASH_ARGC == 0 )); then
  export MAX_RUNS=10 # default value
else
  export MAX_RUNS=${1}
fi
echo "Maximum number of runs is $MAX_RUNS"

export RUNS=1 #Counts the number of runs started
for TREATMENT in l
do
  for SEED in {1..200}
  do
    export FILENAME=`printf "data/20150805%s_%05d.mat" $TREATMENT $SEED`
    if (( RUNS>MAX_RUNS )); then # Run no more than the maximum number of runs.
      break; 
    fi
    if ! [ -a $FILENAME ]; then # If the file already exists, don't need to run the experiment.
      echo "Starting treatment=$TREATMENT seed=$SEED filename=$FILENAME"
      jsub "experiment_20150805.nbs $SEED $TREATMENT" -comment "seed=$SEED treatment=$TREATMENT" -mfail -email peter.i.frazier@gmail.com -queue huge -nproc 1 12
      RUNS=$(($RUNS+1))
    fi
  done
done
