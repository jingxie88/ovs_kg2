#!/bin/sh
# Start many worker threads, each of which will save output to a separate file
# in the directory data/

# The treatment to do is defined by levelA, levelB, and levelC.
# levelA is DoLocalSearch.  possible values 0 and 1, though 0 is not implemented yet
# levelB is IncludeBest*. possible values 0 and 1
# levelC is NumRandomStartS/P.  possible values 0,1,2,3,5,10,100.  
# Dropping 1000 as a possible value for levelC because it takes so long.
# We cannot set both levelB and levelC to 0, as this is not a well-defined
# algorithm, and our code exits with an error.


# Get an optional argument, which is the maximum number of runs to do.
# Once we hit this maximum number of runs started, we stop.
if (( BASH_ARGC == 0 )); then
  export MAX_RUNS=10 # default value
else
  export MAX_RUNS=${1}
fi
echo "Maximum number of runs is $MAX_RUNS"

export RUNS=0 #Counts the number of runs started
for levelA in {0,1}
do
  for levelB in {0,1}
  do
    for levelC in {0,1,2,3,5,10,100}
    do
      for SEED in {1..100}
      do
        if (( levelB + levelC == 0 )); then
          # We cannot run with both B and C set to 0, because the algorithm is not meaningful.
          # Skip this.
          continue
        fi
        export FILENAME=`printf "data/20141003_%d_%d_%d_%05d.mat" $levelA $levelB $levelC $SEED`
        if (( RUNS>MAX_RUNS )); then # Run no more than the maximum number of runs.
          break; 
        fi
        if ! [ -a $FILENAME ]; then # If the file already exists, don't need to run the experiment.
          echo "Starting levelA=$levelA levelB=$levelB levelC=$levelC seed=$SEED filename=$FILENAME"
          jsub "experiment_20141003.nbs $SEED $levelA $levelB $levelC" -comment "$SEED $levelA $levelB $levelC" -mfail -email peter.i.frazier@gmail.com -nproc 1 16
          RUNS=$(($RUNS+1))
        fi
      done
    done
  done
done
