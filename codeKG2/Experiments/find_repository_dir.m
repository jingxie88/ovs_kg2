% Searches through a hard-coded set of directories to find the one where the
% repository lives.  If you want to use this experimental framework on a new
% machine, add a hard-coded directory to the variable "dirs".
function dir = find_repository_dir()
  % directories to search for the repository.
  dirs = {};
  dirs{1} = '/fs/home/pf98/git/ovs_kg2/codeKG2'; % directory where the repository lives on icse.
  dirs{2} = '/Users/pfrazier/work/mypapers/2011 JingSteve BayesOpt/git/ovs_kg2/codeKG2'; % directory where the repository lives on peter's laptop.
  for i=1:length(dirs)
      if (exist(dirs{i},'dir')==7)
          dir = dirs{i};
          break;
      end
  end
  if i == 7
      error('Could not find repository.  Please add the location to the hard-coded search directories.')
  end
end
