% Post-processing for experiment_20150805.  
% "treatment" is a letter like "a", "b" or "c".
% Different treatments are different algorithm settings, and are stored with this prefix in data/.  
function [mean_value,mean_time,std_value,std_time,budget,values,oc] = experiment_20150805_finish_one_treatment(treatment)
  disp(sprintf('Loading data for treatment %s', treatment));
  dir = find_repository_dir();
  data_dir = sprintf('%s/Experiments/data',dir); % directory where the data is stored
  seeds  = 1:120; % set of possible seed values.

  % budget with which the experiment was run.
  if (treatment == 'e' || treatment == 'f' || treatment == 'g')
    budget = 1:500;
  elseif (treatment == 'j')
    budget = 1:200;
  else
    budget = 1:1000;
  end

  % Create a function handle that is the true expected value of the
  % RosenbrockXieFrazierChickExact function, times -1.  Maximizing this is
  % equivalent to minimizing the Rosenbrock function.
  q = 6;
  addpath(sprintf('%s/Rosenbrock_SimOpt',dir));
  TrueValue = @(x) -1*RosenbrockXieFrazierChickExact(x,q);
  minimize = 1;
  valuerange = -.8:.3:1.9;
  Best = RosenbrockXieFrazierChickOpt(valuerange,q,minimize);

  % thresholds that we use for our KG stopping rule. 
  % The value is in units of the log of the KG factor
  logKG_thresh = [-4:.1:6];

  % Fill in matrix of solution values by reading the data files.  
  % Each row is a separate seed.  Each column is an entry in budget.
  values=zeros(length(seeds),length(logKG_thresh));
  times=zeros(length(seeds),length(logKG_thresh));
  for i=1:length(seeds)
      seed = seeds(i);
      file = sprintf('%s/20150805%s_%05d.mat',data_dir,treatment,seed);
      if (exist(file,'file')==2) % If the file is there
          data=load(file);
          for j=1:length(logKG_thresh)
              % Find the first time such that the KG value drops below KG_cutoffs[j]
              times_below = find(data.debug.VOI(1:length(budget))<logKG_thresh(j));
              % This is the index of the time in the budget when we would stop
	      % PF: I have observed many of the debug.VOI values equal to NaN.
	      % This could be from pairs, but I'm not sure.
              stop = min([times_below,length(budget)]);
              % If KGvalue never drops below the cutoff, then stop is an empty matric.
              if (all(data.debug.VOI(1:length(budget))>=logKG_thresh(j)))
	         % If our KG value never dropped below the threshold, stop on the last sample taken.
                 warn sprintf('On seed %d KG cutoff %g, KG value was above cutoff for all samples.  Setting stopping rule to the last sample taken.', seed, logKG_thresh(j))
                 stop = length(budget)
              end

              x = data.solutions(stop,:);
              values(i,j) = TrueValue(data.solutions(stop,:));
              times(i,j) = budget(stop); % number of samples taken
              %disp(sprintf('Solution %s in cell (%d,%d) has value %d', mat2str(x), i, j, values(i,j)));
          end
          % pf: should do some error checking on the size of data.solutions, and also to
          % make sure that the other fields in data match what we think they should be. 
      else % If the file is not there
          disp(sprintf('Datafile for seed %d is missing', i));
          values(i,:) = NaN; % Set the missing solutions to be NaN.
          times(i,:) = NaN;
      end
  end

  isok = all(~isnan(values)'); % This is the set of rows that have all entries not NaN.
  values = values(isok,:); % get rid of rows that have NaN.
  times  = times(isok,:);
  nok = sum(isok); % nok = sum(isok) is the number of seeds counted.
  if (nok == 0)
      error('Could not find any seeds with files.')
  elseif (nok < length(seeds))
    warning('Some seeds do not have data files.  Ignoring these.  This may introduce bias.'); 
  end

  % This gives two row-vectors, where each column corresponds to an entry in
  % the budget vector, and is the mean value or std at that budget.
  mean_value = mean(values);
  mean_time =  mean(times);
  oc = Best - mean_value;
  std_value = std(values) / sqrt(sum(isok));
  std_time = std(times) / sqrt(sum(isok));

end

