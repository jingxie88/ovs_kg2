% Create plots for all treatments
function [mean_value,std_value,budget,values] = experiment_20140801_finish(treatment)
  if (nargin == 1)
    experiment_20140801_plot_one_treatment(treatment);
  else
    experiment_20140801_plot_one_treatment('a');
    experiment_20140801_plot_one_treatment('b');
    experiment_20140801_plot_one_treatment('c');
    experiment_20140801_plot_one_treatment('d');
    experiment_20140801_plot_one_treatment('e');
    experiment_20140801_plot_one_treatment('f');
    experiment_20140801_plot_one_treatment('g');
    experiment_20140801_plot_one_treatment('h');
    experiment_20140801_plot_one_treatment('i');
    experiment_20140801_plot_one_treatment('j');
    experiment_20140801_plot_one_treatment('k');
    experiment_20140801_plot_one_treatment('l');
    experiment_20140801_plot_one_treatment('m');
    experiment_20140801_plot_one_treatment('n');
  end
end

% Post-processing for experiment_20140801.
% "treatment" is a letter like "a", "b" or "c".
% Different treatments are different algorithm settings, and are stored with this prefix in data/.  
function experiment_20140801_plot_one_treatment(treatment)
  disp(sprintf('Creating graphs for treatment %s', treatment));

  % This gives two row-vectors, where each column corresponds to an entry in
  % the budget vector, and is the mean value or std at that budget.
  [mean_value, std_value, budget, values, oc] = experiment_20140801_finish_one_treatment(treatment);

  disp(sprintf('Opportunity cost for treatment %s is %s', treatment, mat2str(oc,2)));

  disp(sprintf('Opportunity cost at the final time is %.2f +/- %.2f', oc(end),std_value(end)));

  h=errorbar(budget,oc,2*std_value)
  set(get(h,'Parent'), 'YScale', 'log') % make it a semilog plot.
  xlabel('budget')
  ylabel('opportunity cost')
  axis([0 budget(end) 1 1000])
  title(sprintf('experiment 20140801%s',treatment))

  dir = find_repository_dir();
  print('-dpdf',sprintf('%s/Experiments/experiment_20140801%s.pdf',dir,treatment));

end
