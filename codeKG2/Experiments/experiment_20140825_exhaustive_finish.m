% Runs through all of the files created by experiment_20140825_exhaustive, and
% takes that data and stores it into a single matlab file with a hashtable.
% This avoids the problems that would occur if we stored tens of thousands of
% files, one for each ATO solution visited. 
function experiment_20140825_exhaustive_finish()
  home_dir = find_repository_dir();
  data_dir = sprintf('%s/Experiments/data',home_dir); % directory where the data is stored

  ATO_dir = sprintf('%s/Experiments/data/20140825_exhaustive',home_dir);
  if (exist(ATO_dir,'dir')~=7)
    error(sprintf('Directory %s does not exist.', ATO_dir));
  end
 
  % Load in 
  summary_file = sprintf('%s/20140825_exhaustive.mat', data_dir);
  if (exist(summary_file,'file')==2)
    % The file already exists.  Load it in and don't overwrite its contents.
    data = load(summary_file);
    val = data.val; % pf: shoud do some error checking if values does not exist.
    disp(sprintf('Loaded %d previous entries in %s', val.length, summary_file));
    stderr = data.stderr;
    n = data.n;
  else
    disp(sprintf('%s does not exist.  Creating it.', summary_file));
    val = containers.Map(); % create empty maps to hold each quantity.
    stderr = containers.Map();
    n = containers.Map();
  end

  % Iterate through all of the files in the ATO directory, and add to val,
  % stderr, and n as they are found, using the filename as the key 
  num_updated = 0;
  files = dir(sprintf('%s/*.mat',ATO_dir));
  for file = files'
    full_filename = sprintf('%s/%s',ATO_dir,file.name);
    disp(sprintf('Processing %s', full_filename));
    data = load(full_filename);
    val(file.name)=data.val;
    stderr(file.name)=data.stderr;
    n(file.name)=data.n;
    % pf: I should check that we are not overwriting something
    delete(full_filename);
    num_updated = num_updated + 1;
  end

  if (num_updated > 0)
    save(summary_file,'val','stderr','n');
  end
end
