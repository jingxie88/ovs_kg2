function fig3(use_errorbars)
if (nargin<1)
    use_errorbars=0;
end

KG2_treatment='b';
KG1_treatment='c';

[KG2_mean, KG2_std, KG2_budget, KG2_values] = experiment_20140825_finish_one_treatment(KG2_treatment,0);
[KG1_mean, KG1_std, KG1_budget, KG1_values] = experiment_20140825_finish_one_treatment(KG1_treatment,0);

ISC=load('../ATO/compass_ATO/compass.mat');
ISC_budget = 250:1000;
% ISC_budget starts at 250 because we ignore the first stage of ISC, which is
% samples 1 through 249. This is not actually output by the ISC code, but is
% instead the average performance at a collection of randomly selected points.
ISC_mean = ISC.MISC(ISC_budget);
% We don't have errorbars for ISC.

linewidth=1.5;
if (use_errorbars)
  h=errorbar(KG2_budget,KG2_mean,2*KG2_std,'b-','LineWidth',linewidth)
else
  h=plot(KG2_budget,KG2_mean,'b-','LineWidth',linewidth)
end
set(gca,'Fontsize',18);
set(gca,'YTick',[0:25:150]);
xlabel('Sample Size');
ylabel('Expected Optimal Profit');
axis([0 KG2_budget(end) 0 150])
hold on
if (use_errorbars)
  errorbar(KG1_budget,KG1_mean,2*KG1_std,'k-','LineWidth',linewidth);
  plot(ISC_budget,ISC_mean,'k--','LineWidth',linewidth);
else
  plot(KG1_budget,KG1_mean,'k-','LineWidth',linewidth);
  plot(ISC_budget,ISC_mean,'k--','LineWidth',linewidth);
end
hold off

[hlegend,hobj]=legend('KG^2_1','KG_1','ISC','Location','southeast');
textobj=findobj(hobj,'type','text')
set(textobj,'Fontsize',18)
% The code below makes the legend have the right vertical height, so it doesn't cut off the 2 in KG^2_1.
% For some strange reason, this doesn't put it in the right place when run on the command line, but it does when run in the GUI.
leg_pos = get(hlegend,'position');
set(hlegend,'position',[leg_pos(1),leg_pos(2),leg_pos(3),leg_pos(4)*1.2])


text(1000,141,'X','HorizontalAlignment','center','Color','blue','FontWeight','bold'); % 141 is the value of the best solution found
dir = find_repository_dir();
if use_errorbars
  print('-dpdf',sprintf('%s/Experiments/fig3_Errorbars.pdf',dir));
else
  print('-dpdf',sprintf('%s/Experiments/fig3_NoErrorbars.pdf',dir));
end
