% Produces a version of figure 2, which appears in the paper, and plots the
% opportunity cost for the Rosenbrock function vs. # samples under two
% different treatments, b and k, one of which ignores the first stage of samples,
% and the other of which uses it.
function experiment_20140801jb(use_errorbars)
if (nargin<1)
    use_errorbars=0;
end

IgnoreFirstStage_treatment='b';
UseFirstStage_treatment='j';

[IgnoreFirstStage_mean, IgnoreFirstStage_std, IgnoreFirstStage_budget, IgnoreFirstStage_values, IgnoreFirstStage_oc] = experiment_20140801_finish_one_treatment(IgnoreFirstStage_treatment);
[UseFirstStage_mean, UseFirstStage_std, UseFirstStage_budget, UseFirstStage_values, UseFirstStage_oc] = experiment_20140801_finish_one_treatment(UseFirstStage_treatment);

budget=1:200;

if (use_errorbars)
  h=errorbar(budget,IgnoreFirstStage_oc(budget),2*IgnoreFirstStage_std(budget),'k-');
else
  h = plot(budget,IgnoreFirstStage_oc(budget),'k-');
end
set(get(h,'Parent'), 'YScale', 'log') % make it a semilog plot.
set(gca,'Fontsize',18)
xlabel('Sample Size')
ylabel('Expected Opportunity Cost')
axis([0 budget(end) 1 1000])
hold on
if (use_errorbars)
  errorbar(budget,UseFirstStage_oc(budget),2*UseFirstStage_std(budget),'b-')
else
  plot(budget,UseFirstStage_oc(budget),'b-')
end
hold off
legend('Ignore First Stage','Use First Stage');
dir = find_repository_dir();
print('-dpdf',sprintf('%s/Experiments/experiment_20140801jb.pdf',dir));
