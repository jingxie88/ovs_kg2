% Runs through the output created by experiment_20140825.m (which runs AKG2 on
% the ATO problem) and experiment_20140825_exhaustive.m (which calculates the
% value of a solution to the ATO problem through exhaustive simulation), and
% creates plots.  If the value of some solutions to the ATO problem are missing,
% then these are generated.  We limit the number that we generate to the passed
% max_to_generate argument (default 0), because each one can take up to a few
% minutes to generate.
function [mean_value,std_value,budget,values] = experiment_20140825_finish(treatment,max_to_generate)
  if (nargin < 2)
    max_to_generate = 0;
  end
  % Move any previously generated 20140825_exhaustive/ files with ATO solution
  % values into 20140825_exhaustive.mat so that we can access it in
  % plot_one_treatment.
  experiment_20140825_exhaustive_finish()
  if (nargin >= 1)
    [mean_value,std_value,budget,values] = experiment_20140825_plot_one_treatment(treatment,max_to_generate);
  else % No arguments.  Set max_to_generate to its default of 0.
    experiment_20140825_plot_one_treatment('a',0);
    experiment_20140825_plot_one_treatment('b',0);
    experiment_20140825_plot_one_treatment('c',0);
  end
end



% Post-processing for experiment_20140825a
% "treatment" is a letter like "a", "b" or "c".
% Different treatments are different algorithm settings, and are stored with this prefix in data/.  
% If we are missing solution values for some seeds, it generates up to max_to_generate.
% The number generated (possibly 0) is returned as num_generated.
function [mean_value,std_value,budget,values,num_generated] = experiment_20140825_plot_one_treatment(treatment,max_to_generate)
  [mean_value,std_value,budget,values,num_generated] = experiment_20140825_finish_one_treatment(treatment,max_to_generate);

  h=errorbar(budget,mean_value,2*std_value);
  xlabel('Sample Size')
  ylabel('Estimated Optimal Profit')
  axis([0 budget(end) 1 150])
  legend('KG^2');
  title(sprintf('experiment 20140825%s',treatment))
  print('-dpdf',sprintf('%s/Experiments/experiment_20140825%s.pdf',dir,treatment));
end
