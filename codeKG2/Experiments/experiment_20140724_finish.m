% Post-processing for experiment_20140724
function [mean_value,std_value,budget] = experiment_20140724_finish()
  dir = find_repository_dir();
  data_dir = sprintf('%s/Experiments/data',dir); % directory where the data is stored
  seeds  = 1:100; % set of possible seed values.
  budget = 1:200; % budget with which the experiment was run.

  % Create a function handle that is the true expected value of the Rosenbrock
  % function, (more precisely, -1 * the value of the Rosenbrock, since this is
  % what we did when we ran the problem).
  q = 4;% this is a parameter of the Rosenbrock function determining its dimension.
  dim=2*q;
  TrueValue = @(x) -1*RosenbrockExact(x,q);

  % Fill in matrix of solution values by reading the data files.  
  % Each row is a separate seed.  Each column is an entry in budget.
  values=zeros(length(seeds),length(budget));
  for i=1:length(seeds)
      seed = seeds(i);
      file = sprintf('%s/20140724_%05d.mat',data_dir,seed); 
      if (exist(file,'file')==2) % If the file is there
          data=load(file);
          for j=1:length(budget)
              x = data.solutions(j,:);
              values(i,j) = TrueValue(data.solutions(j,:));
              %disp(sprintf('Solution %s in cell (%d,%d) has value %d', mat2str(x), i, j, values(i,j)));
          end
          % pf: should do some error checking on the size of data.solutions, and also to
          % make sure that the other fields in data match what we think they should be. 
      else % If the file is not there
          values(i,:) = NaN; % Set the missing solutions to be NaN.
      end
  end

  isok = all(~isnan(values)'); % This is the set of rows that have all entries not NaN.
  values = values(isok,:); % get rid of rows that have NaN.
  nok = sum(isok); % nok = sum(isok) is the number of seeds counted.
  if (nok == 0)
      error('Could not find any seeds with files.')
  elseif (nok < length(seeds))
    warning('Some seeds do not have data files.  Ignoring these.  This may introduce bias.'); 
  end

  % This gives two row-vectors, where each column corresponds to an entry in
  % the budget vector, and is the mean value or std at that budget.
  mean_value = mean(values);
  std_value = std(values) / sqrt(sum(isok));

  errorbar(budget,mean_value,std_value)
  xlabel('budget')
  ylabel('solution value')
  axis([0 200 min(mean_value-std_value) 0])
  title('experiment 20140724')
  print('-dpdf',sprintf('%s/Experiments/experiment_20140724.pdf',dir));

end
