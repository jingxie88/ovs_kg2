% Create plots for all treatments
function [mean_value,std_value,budget,values] = experiment_20150805_finish(treatment)
  if (nargin == 1)
    experiment_20150805_plot_one_treatment(treatment);
  else
    experiment_20150805_plot_one_treatment('b');
  end
end

% Post-processing for experiment_20150805.
% "treatment" is a letter like "a", "b" or "c".
% Different treatments are different algorithm settings, and are stored with this prefix in data/.  
function experiment_20150805_plot_one_treatment(treatment)
  disp(sprintf('Creating graphs for treatment %s', treatment));

  % This gives two row-vectors, where each column corresponds to an entry in
  % the budget vector, and is the mean value or std at that budget.
  [mean_value, mean_time, std_value, std_time, budget, values, oc] = experiment_20150805_finish_one_treatment(treatment);

  disp(sprintf('Opportunity cost for treatment %s is %s', treatment, mat2str(oc,2)));
  disp(sprintf('Expected sample size  for treatment %s is %s', treatment, mat2str(mean_time,2)));

  disp(sprintf('Opportunity cost at the smallest KG threshold is %.2f +/- %.2f', oc(end),std_value(end)));
  disp(sprintf('Expected sample size at the smallest KG threshold  is %.2f +/- %.2f', mean_time(end),std_time(end)));

  h=errorbar(mean_time,oc,2*std_value)
  set(get(h,'Parent'), 'YScale', 'log') % make it a semilog plot.
  xlabel('expected sample size')
  ylabel('opportunity cost')
  axis([0 mean_time(1) 1 1000])
  title(sprintf('experiment 20150805%s',treatment))

  dir = find_repository_dir();
  print('-dpdf',sprintf('%s/Experiments/experiment_20150805%s.pdf',dir,treatment));

end
