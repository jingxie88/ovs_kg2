#!/bin/sh
# Start many worker threads, each of which will save output to a separate file
# in the directory specified within experiment_20140724.nbs
for n in {1..100}
do
	# Add a job, which is to run experiment_20140724.nbs with argument n, to the long NBS queue.
	jsub "experiment_20140724.nbs $n" -mail nostart end -mfail
done
