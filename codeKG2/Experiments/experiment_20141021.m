% function experiment_20141021(seed,treatment)
%
% This code takes as argument a single seed value, runs the AKG2 algorithm on
% the ATO problem, and saves the output to a file, including the "times" return value of AKG2.  
% The name of the file is 20141021?_seed.mat, where seed is the seed values
% passed to this function, and ? is the treatment, which is typically a single
% letter, like b or c.
function experiment_20141021(seed,treatment)
  dir = find_repository_dir();
  addpath(genpath(dir)); % Make sure all needed commands are in the path.

  % data in which the data will be stored 
  data_dir = sprintf('%s/Experiments/data',dir);
  if (exist(data_dir,'dir')~=7)
    error(sprintf('Data directory %s does not exist.', data_dir));
  end

  % file where we will save the output.
  outfile = sprintf('%s/Experiments/data/20141021%s_%05d.mat',dir,treatment,seed); 
  if (exist(outfile,'file')==2)
    error(sprintf('Data file %s already exists.', outfile));
  end

  dim=8;
  x0 = 20*ones(1,dim); % initial starting point (full inventory for each dimension)
  runlength = 70; % parameter used by the simopt AssembleOrder function
  problem = @AssembleOrder;
  valuerange = 0:20; % range of each component of the search space
  probparam = struct('dim',dim,'runlength',runlength,'other',0);

  % These are treatments b and c from 0825.
  if treatment == 'b'
    % KG2, like treatment l from 0801, which is the planned one for production.
    budget = 1:1000;
    other = struct('NumRandomStartS',4,'NumRandomStartP',2,'UseFirstStageSamples',1);
    solverparam = struct('numinitsols',1,'numfinalsols',length(budget),'other',other);
  elseif treatment == 'c'
    % KG1, like treatment m from 0801, which is the planned one for production.
    budget = 1:1000;
    other = struct('NumRandomStartS',4,'NumRandomStartP',0,'IncludeBestSecondBestPair',0,'UseFirstStageSamples',1);
    solverparam = struct('numinitsols',1,'numfinalsols',length(budget),'other',other);
  else
    error(sprintf('Treatment %s is not valid', treatment));
  end

  % Using git, check the status of the repository, to see what version of the
  % code we are using, and whether there are any uncommitted changes.
  % We save this in the outfile.
  [exit_code, git_rev] = system('git rev-parse HEAD'); % Get the current revision.
  [exit_code, git_status] = system('git status'); % Get whether there are any uncommitted changes.
  [exit_code, hostname] = system('hostname'); % Get the machine on which the code was run.
  [exit_code, uptime] = system('uptime'); % Get information about system load

  % Actually run the algorithm, and save the times in addition to the solutoins.
  [solutions,times] = AKG2(x0,budget,problem,valuerange,probparam,solverparam,seed,seed);

  save(outfile,'solutions','times','x0','budget','problem','valuerange','probparam','solverparam','seed','git_rev','git_status','treatment','hostname','uptime');

end
