% function experiment_20150804()
%
% This code runs the ATO problem on a collection of randomly selected points in
% the input space, and produces a histogram for each of them.

function experiment_20150804()
  dir = find_repository_dir();
  addpath(genpath(dir)); % Make sure all needed commands are in the path.

  % data in which the data will be stored 
  data_dir = sprintf('%s/Experiments/data',dir);
  if (exist(data_dir,'dir')~=7)
    error(sprintf('Data directory %s does not exist.', data_dir));
  end

  % file where we will save the output.
  outfile = sprintf('%s/Experiments/data/20150804.mat',dir); 
  if (exist(outfile,'file')==2)
    error(sprintf('Data file %s already exists.', outfile));
  end

  dim=8;
  x0 = 20*ones(1,dim); % initial starting point (full inventory for each dimension)
  runlength = 70; % parameter used by the simopt AssembleOrder function
  problem = @AssembleOrder;
  valuerange = 0:20; % range of each component of the search space
  probparam = struct('dim',dim,'runlength',runlength,'other',0);

  % Using git, check the status of the repository, to see what version of the
  % code we are using, and whether there are any uncommitted changes.
  % We save this in the outfile.
  [exit_code, git_rev] = system('git rev-parse HEAD'); % Get the current revision.
  [exit_code, git_status] = system('git status'); % Get whether there are any uncommitted changes.
  [exit_code, hostname] = system('hostname'); % Get the machine on which the code was run.
  [exit_code, uptime] = system('uptime'); % Get information about system load

  % Generate N random solutions.
  N = 5;
  LD = length(valuerange); % length of the valuerange
  solutions = valuerange(randi(LD,[N dim]));

  seed = 1;
  num_reps = 1000;
  for i=1:N
    for r=1:num_reps
      y(i,r) = problem(solutions(i,:),probparam.runlength,seed,probparam.other);
      seed = seed+1;
    end
  end

  save(outfile,'solutions','y','N','num_reps','problem','valuerange','probparam','git_rev','git_status','hostname','uptime');

end
