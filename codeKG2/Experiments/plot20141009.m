% a one-off plot
function plot20141009(outcome_budget)
  if (nargin ==0)
    outcome_budget = 100; 
  end

  levelsUseFirstStage = [0,1];
  levelA = 1; % Use local search.
  levelsB = [0,1];
  levelsC = [0, 1, 2, 3, 5, 10, 100, 1000];
  y = zeros(length(levelsUseFirstStage),length(levelsB),length(levelsC));
  s = zeros(length(levelsUseFirstStage),length(levelsB),length(levelsC));
  n = zeros(length(levelsUseFirstStage),length(levelsB),length(levelsC));

  for u = 1:length(levelsUseFirstStage)
    for b = 1:length(levelsB)
      for c = 1:length(levelsC)

        if (levelsUseFirstStage(u))
          [oc,stderr,nreps] = experiment_20141003_finish_one_treatment(levelA,levelsB(b),levelsC(c));
        else
          [oc,stderr,nreps] = experiment_20140803_finish_one_treatment(levelA,levelsB(b),levelsC(c));
        end
	    y(u,b,c)=oc(outcome_budget);
        s(u,b,c) = stderr(outcome_budget);
        n(u,b,c) = nreps;
      end
    end
  end

  % PF: warning, the following code is hard-coded in the way it treats
  % levelsA and levelsB.
  h = errorbar(levelsC+1,y(1,1,:),s(1,1,:),'b-')
  set(get(h,'Parent'), 'XScale', 'log')
  hold on
  errorbar(levelsC+1,y(1,2,:),s(1,2,:),'k-')
  errorbar(levelsC+1,y(2,1,:),s(2,1,:),'b--')
  errorbar(levelsC+1,y(2,2,:),s(2,2,:),'k--')
  hold off
  legend('UseFirstStage=0 levelB=0', 'UseFirstStage=0 levelB=1', 'UseFirstStage=1 levelB=0', 'UseFirstStage=1 levelB=1');
  title(sprintf('plot 20141009, budget=%d',outcome_budget));
  xlabel('levelC+1 (so levelC=0 maps to 10^0)');
  ylabel('OC');
  print('-dpdf',sprintf('plot20141009_%d.pdf',outcome_budget))

end


% Post-processing for experiment_20141003
function [oc,stderr,nreps] = experiment_20141003_finish_one_treatment(levelA,levelB,levelC)
  disp(sprintf('Processing treatment levelA=%d levelB=%d levelC=%d', levelA,levelB,levelC));
  dir = find_repository_dir();
  data_dir = sprintf('%s/Experiments/data',dir); % directory where the data is stored
  max_seed = 100;
  seeds  = 1:max_seed; % set of possible seed values.
  budget = 1:250; % budget with which the experiment was run.

  % Create a function handle that is the true expected value of the
  % RosenbrockXieFrazierChickExact function, times -1.  Maximizing this is
  % equivalent to minimizing the Rosenbrock function.
  q = 6;
  addpath(sprintf('%s/Rosenbrock_SimOpt',dir));
  TrueValue = @(x) -1*RosenbrockXieFrazierChickExact(x,q);
  minimize = 1;
  valuerange = -.8:.3:1.9;
  Best = RosenbrockXieFrazierChickOpt(valuerange,q,minimize);

  % Fill in matrix of solution values by reading the data files.  
  % Each row is a separate seed.  Each column is an entry in budget.
  missing = [];
  values=zeros(length(seeds),length(budget));
  for i=1:length(seeds)
      seed = seeds(i);
      file = sprintf('%s/20141003_%d_%d_%d_%05d.mat',data_dir,levelA,levelB,levelC,seed); 
      if (exist(file,'file')==2) % If the file is there
          data=load(file);
          for j=1:length(budget)
              x = data.solutions(j,:);
              values(i,j) = TrueValue(data.solutions(j,:));
              %disp(sprintf('Solution %s in cell (%d,%d) has value %d', mat2str(x), i, j, values(i,j)));
          end
          % pf: should do some error checking on the size of data.solutions, and also to
          % make sure that the other fields in data match what we think they should be. 
      else % If the file is not there
          % disp(sprintf('Datafile for seed %d is missing', i));
          missing = [missing, i];
          values(i,:) = NaN; % Set the missing solutions to be NaN.
      end
  end

  isok = all(~isnan(values)'); % This is the set of rows that have all entries not NaN.
  values = values(isok,:); % get rid of rows that have NaN.
  nreps = sum(isok); % nreps is the number of seeds counted.
  if (nreps == 0)
      disp('No seeds available for this treatment.') 
      oc = NaN * zeros(1,length(budget));
      stderr = NaN *zeros(1,length(budget));
      return;
  elseif (nreps < length(seeds))
      disp('The following seeds do not have data files.  Ignoring these.  This may introduce bias.'); 
      disp(sprintf('Missing seeds: %s', mat2str(missing)));
  end

  % This gives two row-vectors, where each column corresponds to an entry in
  % the budget vector, and is the mean value or std at that budget.
  if (nreps>1)
    mean_value = mean(values);
    stderr = std(values) / sqrt(nreps);
  else
    mean_value = values;
    stderr = values * NaN;
  end
  oc = Best - mean_value;
end


function [oc,stderr,nreps] = experiment_20140803_finish_one_treatment(levelA,levelB,levelC)
  disp(sprintf('Processing treatment levelA=%d levelB=%d levelC=%d', levelA,levelB,levelC));
  dir = find_repository_dir();
  data_dir = sprintf('%s/Experiments/data',dir); % directory where the data is stored
  max_seed = 100;
  seeds  = 1:max_seed; % set of possible seed values.
  budget = 1:200; % budget with which the experiment was run.

  % Create a function handle that is the true expected value of the
  % RosenbrockXieFrazierChickExact function, times -1.  Maximizing this is
  % equivalent to minimizing the Rosenbrock function.
  q = 6;
  addpath(sprintf('%s/Rosenbrock_SimOpt',dir));
  TrueValue = @(x) -1*RosenbrockXieFrazierChickExact(x,q);
  minimize = 1;
  valuerange = -.8:.3:1.9;
  Best = RosenbrockXieFrazierChickOpt(valuerange,q,minimize);

  % Fill in matrix of solution values by reading the data files.  
  % Each row is a separate seed.  Each column is an entry in budget.
  missing = [];
  values=zeros(length(seeds),length(budget));
  for i=1:length(seeds)
      seed = seeds(i);
      file = sprintf('%s/20140803_%d_%d_%d_%05d.mat',data_dir,levelA,levelB,levelC,seed); 
      if (exist(file,'file')==2) % If the file is there
          data=load(file);
          for j=1:length(budget)
              x = data.solutions(j,:);
              values(i,j) = TrueValue(data.solutions(j,:));
              %disp(sprintf('Solution %s in cell (%d,%d) has value %d', mat2str(x), i, j, values(i,j)));
          end
          % pf: should do some error checking on the size of data.solutions, and also to
          % make sure that the other fields in data match what we think they should be. 
      else % If the file is not there
          % disp(sprintf('Datafile for seed %d is missing', i));
          missing = [missing, i];
          values(i,:) = NaN; % Set the missing solutions to be NaN.
      end
  end

  isok = all(~isnan(values)'); % This is the set of rows that have all entries not NaN.
  values = values(isok,:); % get rid of rows that have NaN.
  nreps = sum(isok); % nreps is the number of seeds counted.
  if (nreps == 0)
      disp('No seeds available for this treatment.') 
      oc = NaN * zeros(1,length(budget));
      stderr = NaN *zeros(1,length(budget));
      return;
  elseif (nreps < length(seeds))
      disp('The following seeds do not have data files.  Ignoring these.  This may introduce bias.'); 
      disp(sprintf('Missing seeds: %s', mat2str(missing)));
  end

  % This gives two row-vectors, where each column corresponds to an entry in
  % the budget vector, and is the mean value or std at that budget.
  mean_value = mean(values);
  oc = Best - mean_value;
  stderr = std(values) / sqrt(nreps);
end
