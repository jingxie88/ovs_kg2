% function experiment_20140825(seed,treatment)
% treatment is one of "a", "b", "c", etc.
% each treatment corresponds to a different version of the algorithm.
%
% This code takes as argument a single seed value, runs the AKG2 algorithm on
% the ATO problem, and saves the output to a file.  The name of the file is
% 20140825?_seed.mat, where seed is the seed values passed to this function,
% and ? is the treatment, which is typically a single letter, like a,b, or c.
% Given the seed, the output is deterministic.
function experiment_20140825(seed,treatment)
  dir = find_repository_dir();
  addpath(genpath(dir)); % Make sure all needed commands are in the path.

  % data in which the data will be stored 
  data_dir = sprintf('%s/Experiments/data',dir);
  if (exist(data_dir,'dir')~=7)
    error(sprintf('Data directory %s does not exist.', data_dir));
  end

  % file where we will save the output.
  outfile = sprintf('%s/Experiments/data/20140825%s_%05d.mat',dir,treatment,seed); 
  if (exist(outfile,'file')==2)
    error(sprintf('Data file %s already exists.', outfile));
  end

  dim=8;
  x0 = 20*ones(1,dim); % initial starting point (full inventory for each dimension)
  runlength = 70; % parameter used by the simopt AssembleOrder function
  problem = @AssembleOrder;
  valuerange = 0:20; % range of each component of the search space
  probparam = struct('dim',dim,'runlength',runlength,'other',0);

  if treatment == 'a'
    %KG2, only 1 random start.
    budget = 1:200; % Short budget for testing
    other = struct('NumRandomStartS',1,'NumRandomStartP',1);
    solverparam = struct('numinitsols',1,'numfinalsols',length(budget),'other',other);
  elseif treatment == 'b'
    % KG2, like treatment l from 0801, which is the planned one for production.
    budget = 1:1000;
    other = struct('NumRandomStartS',4,'NumRandomStartP',2,'UseFirstStageSamples',1);
    solverparam = struct('numinitsols',1,'numfinalsols',length(budget),'other',other);
  elseif treatment == 'c'
    % KG1, like treatment m from 0801, which is the planned one for production.
    budget = 1:1000;
    other = struct('NumRandomStartS',4,'NumRandomStartP',0,'IncludeBestSecondBestPair',0,'UseFirstStageSamples',1);
    solverparam = struct('numinitsols',1,'numfinalsols',length(budget),'other',other);
  else
    error(sprintf('Treatment %s is not valid', treatment));
  end

  % Using git, check the status of the repository, to see what version of the
  % code we are using, and whether there are any uncommitted changes.
  % We save this in the outfile.
  [exit_code, git_rev] = system('git rev-parse HEAD'); % Get the current revision.
  [exit_code, git_status] = system('git status'); % Get whether there are any uncommitted changes.

  % Actually run the algorithm.
  solutions = AKG2(x0,budget,problem,valuerange,probparam,solverparam,seed,seed);

  save(outfile,'solutions','x0','budget','problem','valuerange','probparam','solverparam','seed','git_rev','git_status','treatment');

end
