This directory contains a framework for performing experiments on the ICSE cluster at Cornell.
There are a few different kinds of experiments that can be run.

They use the notion of a "treatment", which specifies an algorithm, parameters
for that algorithm, a problem, parameters for that problem (if any), and any
other parameters associated with running the algorithm on that problem (like
budget).  It also uses the notion of a "seed", which is really a substream of
random numbers to use when running the treatment.  We get an estimate of the
expected output corresponding to a treatment by running many seeds and
averaging.

20140825
This is for running the ATO problem, and is set up to handle the fact that we
do not know the true value of points in the ATO problem, and must use
exhaustive simulation to evaluate them.
This set of experiments uses the following files:

  experiment_20140825.m  
  This is a matlab script that takes a seed, and a treatment, as an input, and
  runs that treatment with that seed, storing the output in a file of the form
  data/20140825?_[seed].mat, where ? is the treatment, typically a single letter
  like a, b, or c.   The output contains the sequence of solutions visited by
  that algorithm, but not the values of those solutions.

  experiment_20140825.nbs
  This is a shell script that the NBS system can run, which runs
  experiment_20140825.m on a single treatment and seed passed on the command
  line.

  experiment_20140825.sh
  This is a convenience shell script that runs the NBS system on many seeds.
  It calls experiment_20140825.nbs.

  experiment_20140825_exhaustive.m
  This is a matlab script that performs exhaustive simulation to evaluate the
  mean value of one particular solution to the ATO problem.  It then saves the
  output to data/20140825_exhaustive/ATO_?_?_?_?_?_?_?_?.mat, where the question
  marks indicate which solution is being computed.

  experiment_20140825_exhaustive_finish.m
  Goes through all of the files of the form data/20140825_exhaustive/ATO_?_?_?_?_?_?_?_?.mat,
  (which store values of solutions to the ATO problem and are created by
  experiment_20140825_exhaustive.m) and puts their values into a single file
  data/20140825_exhaustive.mat.

  experiment_20140825_finish.m
  Processes all of the datafiles of the form data/20140825?_[seed].mat created
  by experiment_20140825.m, and generates plots.  Also, in doing this, it 
  searches through all ATO solutions visited in these datafiles,
  checks for each of them whether a value for it exists in
  data/20140825_exhaustive.mat, and if not, outputs a line of
  shell script that will run experiment_20140825_exhaustive on the NBS to
  evaluate it.  Then we can run the outputted shell script at our leisure. 
