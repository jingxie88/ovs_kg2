% Produces a figure that was included in the response to referees for revision 2. 
% Plots opportunity cost for the Rosenbrock function vs. # samples under KG2
% with deterministic stopping rule, vs KG2 with the KG1 stopping rule.
function fig_r2(use_errorbars)
if (nargin<1)
    use_errorbars=0;
end

linewidth=1.5;

[KG2_mean, KG2_std, KG2_budget, KG2_values, KG2_oc] = experiment_20140801_finish_one_treatment('l');
[stop_mean_value,stop_mean_time,stop_std_value,stop_std_time,stop_budget,stop_values,stop_oc] = experiment_20150805_finish_one_treatment('l')

if (use_errorbars)
  h=errorbar(KG2_budget,KG2_oc,2*KG2_std,'b-','LineWidth',linewidth);
else
  h=plot(KG2_budget,KG2_oc,'b-','LineWidth',linewidth);
end
set(get(h,'Parent'), 'YScale', 'log') % make it a semilog plot.
set(gca,'Fontsize',18)
xlabel('Sample Size')
ylabel('Expected Opportunity Cost')
axis([0 max(stop_mean_time) 1 1000])
hold on
if (use_errorbars)
  errorbar(stop_mean_time,stop_oc,2*stop_std_value,'k--','LineWidth',linewidth)
else
  plot(stop_mean_time,stop_oc,'k--','LineWidth',linewidth)
end
hold off
legend('KG^2_1 fixed stopping', 'KG^2_1 adaptive stopping');
dir = find_repository_dir();
if use_errorbars
  print('-dpdf',sprintf('%s/Experiments/fig_r2_Errorbars.pdf',dir));
else
  print('-dpdf',sprintf('%s/Experiments/fig_r2_NoErrorbars.pdf',dir));
end

disp(sprintf('max(stop_std_value)=%f\n', max(stop_std_value)));
disp(sprintf('max(stop_std_time)=%f\n' , max(stop_std_time)));
