% Performs exhaustive simulation to get a high accuracy estimate of the value
% of a solution to the ATO problem, and saves the result in
% Experiments/data/ATO.
%
% Use this together with 20140825_experiment, which runs algorithms and
% collects the solutions visited, but does not know their values.
function [val,stderr,n] = experiment_20140825_exhaustive(solution,n)
  if (nargin < 2)
    n = 1000;
  end
  dir = find_repository_dir();
  addpath(genpath(dir)); % Make sure all needed commands are in the path.

  ATO_dir = sprintf('%s/Experiments/data/20140825_exhaustive',dir);
  if (exist(ATO_dir,'dir')~=7)
    [ok, msg] = mkdir (ATO_dir);
    if (~ok)
        disp(sprintf('Directory %s for storing ATO results does not exist.  Tried to create it, but had an error', ATO_dir));
        error(msg);
    end
  end

  dim=8;
  assert(length(solution)==dim);
  runlength = 70; % parameter used by the simopt AssembleOrder function
  y = zeros(1,n);
  parfor seed = 1:n
      y(seed) = AssembleOrder(solution,runlength,seed);
  end

  % double check that solution only has integers.

  % Construct a filename to store the solution.
  filename = sprintf('ATO_%d', solution(1));
  for i=2:length(solution)
      filename = sprintf('%s_%d',filename,solution(i));
  end
  outfile = sprintf('%s/%s.mat',ATO_dir,filename);
  if (exist(outfile,'file'))
      warning(sprintf('output file, listed below, already exists.  Overwriting.\n%s',outfile));
  end

  val = mean(y);
  stderr = std(y)/sqrt(n);

  save(outfile,'val','stderr','n');
  disp(sprintf('Saved results to %s',outfile));

end
