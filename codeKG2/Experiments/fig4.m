% Plots CPU time spent in a sample path of AKG2, using data recorded in
% data/20141021b_00002.mat, which I (Peter) ran on my laptop.
% Puts the plot in fig4.pdf, which is a plot that will go into the paper.
%
% Updates occurred at samples 97, 138, 193, 273, 385, 545, 769.
% This is because 
%    1. the size of the first stage, N, was 96.
%    2. UpdateTimes was calculated by the code as ceil(N*2.^([1:t]/2)-N)
%    3. Calculating N+UpdateTimes gives [136,192,272,384,544,768,1087]
%    4. The AKG2 code runs the MLE at the next decision point *after* each
%    sample n+N, where n is either 0 or in UpdateTimes.  In most cases, then,
%    we add one to N+UpdateTimes to get when the MLE occurred.  A special case
%    is: samples 136 and 137 were a pair, so the next decision after sample 136
%    was at sample 138.
%
%    The elapsed CPU time at sample t is the VOI computations used to decide
%    what to sample before (to take the t-1 sample), and to compute the
%    posterior (including any MLE updates) that will be used to decide how to
%    take sample t.  This is why the elapsed CPU time for sample 97 is not very
%    large --- it includes an MLE update, but not a VOI computation.
% 
function fig4()

  % Load the datafile that contains "times", which is the total cumulative
  % number of seconds spent in a run of AKG2.
  s=load('data/20141021b_00002.mat')

  linewidth=1.5;

  % Plot cumulative time
  set(gca,'FontSize',18)
  plot(1:length(s.times),s.times,'LineWidth',linewidth)
  xlabel('Sample Size')
  ylabel('Total CPU Time (sec)')
  set(gca,'FontSize',18)
  print -dpdf 'fig4a.pdf'


  delta = diff([0;s.times]);
  for i=2:length(delta)
      if (delta(i)==0)
          % A delta of 0 occurred because we sampled a pair.  Assign each
          % element in the pair the same time.
          delta(i) = delta(i-1);
      end
  end

  plot(delta,'LineWidth',linewidth)
  MLEupdates = [97,138,193,273,385,545,769]; % This is when we updated the MLE.
  for i=1:length(MLEupdates)
      h=line([MLEupdates(i),MLEupdates(i)],[0 350]);
      set(h,'LineStyle','--');
  end
  xlabel('Sample Size')
  ylabel('CPU Time per Decision (sec)')
  axis([0 1000 0 350])
  set(gca,'FontSize',18)
  print -dpdf 'fig4b.pdf'


end
