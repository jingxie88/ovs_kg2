% Post-processing for experiment_20140825.  Also used by fig3.
% "treatment" is a letter like "a", "b" or "c".
% Different treatments are different algorithm settings, and are stored with this prefix in data/.
% If we are missing solution values for some seeds, it generates up to max_to_generate.
% The number generated (possibly 0) is returned as num_generated.
function [mean_value,std_value,budget,values,num_generated] = experiment_20140825_finish_one_treatment(treatment,max_to_generate)
  disp(sprintf('Loading data for treatment %s', treatment));
  dir = find_repository_dir();
  data_dir = sprintf('%s/Experiments/data',dir); % directory where the data is stored
  seeds  = 1:100; % set of possible seed values.

  % budget with which the experiment was run.
  if (treatment == 'a')
    budget = 1:200;
  else
    budget = 1:1000; % PF: we currently don't have other treatments.
  end

  dim = 8; % dimension of the ATO problem

  % Pull in a map containing the value of ATO solutions.
  solutionValueFile = sprintf('%s/20140825_exhaustive.mat', data_dir);
  if (exist(solutionValueFile,'dir')~=2)
    error(sprintf('% does not exist.  Run 20140825_exhaustive_finish.m'));
  end
  solutionValue = load(solutionValueFile);

  % Fill in matrix of solution values for each seed, by reading the data
  % files.  Each row is a separate seed.  Each column is an entry in budget.
  values=zeros(length(seeds),length(budget));
  values_stderr=zeros(length(seeds),length(budget));
  values_n=zeros(length(seeds),length(budget));
  num_generated = 0;
  for i=1:length(seeds)
      seed = seeds(i);
      file = sprintf('%s/20140825%s_%05d.mat',data_dir,treatment,seed); 
      if (exist(file,'file')==2) % If the file is there
          data=load(file);
          for j=1:length(budget)
              sol = data.solutions(j,1:dim);

              % Convert sol, which is an array, to a string with format ATO_?_?_?_?_?_?.mat
              % We will use this as a dictionary key.
              solname = 'ATO';
              for k=1:length(sol)
                  solname = sprintf('%s_%d',solname,sol(k));
              end
              solname = sprintf('%s.mat',solname);
 
              if (~solutionValue.val.isKey(solname))
                  if (num_generated < max_to_generate)
                      disp(sprintf('Missing %s.  Generating it.',solname));
                      [values(i,j), values_stderr(i,j), values_n(i,j)] = experiment_20140825_exhaustive(sol);
                      solutionValue.val(solname) = values(i,j);
                      solutionValue.stderr(solname) = values_stderr(i,j);
                      solutionValue.n(solname) = values_n(i,j);
                      num_generated = num_generated + 1;
                  else 
                      disp(sprintf('Missing %s, and already generated max_to_generate.  Skipping it.',solname));
                      values(i,j) = NaN;
                      values_stderr(i,j) = NaN;
                      values_n(i,j) = NaN;
                  end
              else
		  values(i,j) = solutionValue.val(solname);
		  values_stderr(i,j) = solutionValue.stderr(solname);
		  values_n(i,j) = solutionValue.n(solname);
              end
          end
          % pf: should do some error checking on the size of data.solutions, and also to
          % make sure that the other fields in data match what we think they should be. 
      else % If the file is not there
          disp(sprintf('Datafile for seed %d is missing', i));
          values(i,:) = NaN; % Set the missing solutions to be NaN.
          stderr(i,:) = NaN;
          n(i,:) = NaN;
      end
  end

  isok = all(~isnan(values)'); % This is the set of rows that have all entries not NaN.
  values = values(isok,:); % get rid of rows that have NaN.
  nok = sum(isok); % nok = sum(isok) is the number of seeds counted.
  if (nok == 0)
      warning('Could not find any seeds with files.  Exiting.')
      mean_value = NaN;
      std_value = NaN;
      return;
  elseif (nok < length(seeds))
    warning('Some seeds do not have data files.  Ignoring these.  This may introduce bias.'); 
  end

  % This gives two row-vectors, where each column corresponds to an entry in
  % the budget vector, and is the mean value or std at that budget.
  mean_value = mean(values);
  std_value = std(values) / sqrt(sum(isok));
end
