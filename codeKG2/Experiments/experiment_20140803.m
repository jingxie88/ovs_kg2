% function experiment_20140803(seed,levelA,levelB,levelC)
%
% levelA is 0 or 1, and specifies DoLocalSearch.  Right now only 1 is
%   implemented.
% levelB is 0 or 1, and specifies whether to IncludeBestSingleton,
%   IncludeSecondBestSingleton, and IncludeBestSecondBestPair.
% levelC is a non-negative integer and specifies the value for NumRandomStartS
%   and NumRandomStartP.
%
% This code takes as argument a single seed value, runs the AKG2 algorithm on
% the RosenbrockXieChickFrazier function, and saves the output to a file.  The
% name of the file is 20140803_levelA_levelB_levelC_seed.mat.  Given the seed,
% the output is deterministic.
%
% We run with a budget of 200, which makes this code run quickly, in contrast
% with experiment_20140803.  Also, in our "finish" code, we focus on the
% oppontunity cost after 100 samples, and after 200 samples, rather than on the
% whole sample path.
%
% Also, our treatments are factors: high/low 
function experiment_20140803(seed,levelA,levelB,levelC)
  if (levelA~=0 && levelA~=1)
    error('levelA must be 0 or 1.');
  end
  if (levelB~=0 && levelB~=1)
    error('levelB must be 0 or 1.');
  end
  if (round(levelC)~=levelC || levelC<0)
    error('levelC must be a non-negative integer.');
  end

  % directory in which the repository lives.
  dir = find_repository_dir();
  addpath(genpath(dir)); % Make sure all needed commands are in the path.
 
  % data in which the data will be stored 
  data_dir = sprintf('%s/Experiments/data',dir);
  if (exist(data_dir,'dir')~=7)
    error(sprintf('Data directory %s does not exist.', data_dir));
  end

  % file where we will save the output.
  outfile = sprintf('%s/Experiments/data/20140803_%d_%d_%d_%05d.mat',dir,levelA,levelB,levelC,seed); 
  if (exist(outfile,'file')==2)
    error(sprintf('Data file %s already exists.', outfile));
  end

  q = 6;% this is a parameter of the Rosenbrock function determining its dimension.
  dim=q;
  x0 = []; % no initial samples.

  % Independent random number stream used by RosenbrockXieChickFrazier.  See
  % experiment_20140801.m for a detailed explanation as to why we use the
  % second substream, and why this is safe.
  stream = RandStream.create('mrg32k3a', 'NumStreams',2,'StreamIndices',2); 

  probother = struct('q',q,'stream',stream);
  probparam = struct('dim',dim,'runlength',1,'other',probother);
  valuerange = -.8:.3:1.9;
  % Our problem function handle returns the negative function value of the
  % RosenbrockXieFrazierChick function. Maximizing this is the same as
  % minimizing RosenbrockXieFrazierChick.
  problem = @(x,runlength,seed,other) -1*RosenbrockXieFrazierChick(x,runlength,seed,other);
  budget = 1:200;

  other = struct('DoLocalSearch',levelA,...
		  'IncludeBestSingleton',levelB,...
		  'IncludeSecondBestSingleton',levelB,...
		  'IncludeBestSecondBestPair',levelB,...
		  'NumRandomStartS',levelC,'NumRandomStartP',levelC,...
                  'UseFirstStageSamples',0); 
  % We turn UseFirstStageSamples off because these experiments were started
  % before UseFirstStageSamples was implemented.
  solverparam = struct('numinitsols',0,'numfinalsols',length(budget),'other',other);

  % Using git, check the status of the repository, to see what version of the
  % code we are using, and whether there are any uncommitted changes.
  % We save this in the outfile.
  [exit_code, git_rev] = system('git rev-parse HEAD'); % Get the current revision.
  [exit_code, git_status] = system('git status'); % Get whether there are any uncommitted changes.

  % Actually run the algorithm.  Use the same seed for solver and problem.
  solutions = AKG2(x0,budget,problem,valuerange,probparam,solverparam,seed,seed);

  save(outfile,'solutions','x0','budget','problem','valuerange','probparam','solverparam','seed','git_rev','git_status','levelA','levelB','levelC');

end
