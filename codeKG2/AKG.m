function A = AKG(x0,budget,problem,valuerange,probparam,solverparam,probseed,solverseed)
% Inputs: 

% "problem" has input (x,runlength,seed). 

% "valuerange" is a vector in ascending order specifying the discrete value set 
% of each dimension of the solution space. 

% "probparam" = [dim, runlength] in the ATO example.

% "solverparam" = struct('numinitsols',0, 'numfinalsols',lengh(budget), 'other',others);
% others = struct('n1',10*dim,'n2',2*dim,'NumStarts',5,...
% 'rescale',2,'UpdateTimes',[30,60,100,150,200:100:N]);

% Output A: list of solutions given "budget"


probstream = RandStream('mt19937ar','Seed',probseed); 
solverstream = RandStream('mt19937ar','Seed',solverseed); 

% Parameter Settings
dim = probparam(1);
runlength = probparam(2);
others = solverparam.other;
N = max(size(x0,1),others.n1)+others.n2;
if N>= budget(end)
    error('budget too small!')
end
LD = length(valuerange);
scale0=abs(valuerange(end)-valuerange(1))*ones(1,dim); % initial scaling vector in MLE.
options = optimset('GradObj','on','Algorithm','active-set','TolFun',1e-8,'Display','off');%,'MaxFunEvals',1000);


% Initial stage of samples and parameter estimation
RandStream.setGlobalStream(solverstream);
FirstStageAlts1=valuerange(randi(LD,[max(others.n1-size(x0,1),0) dim]));
if others.n1 == 1
    FirstStageAlts1=(FirstStageAlts1(:))';
else if dim == 1
        FirstStageAlts1=FirstStageAlts1(:);
    end
end
FirstStageAlts1 = [FirstStageAlts1;x0]; % include x0 in the initial stage

RandStream.setGlobalStream(probstream);
FirstStageObs1 = zeros(size(FirstStageAlts1,1),1);
for wind = 1:size(FirstStageAlts1,1)
    FirstStageObs1(wind)=feval(problem,FirstStageAlts1(wind,:),runlength,randi(999999999));
end
[~,Best]=sort(FirstStageObs1,'descend');
FirstStageAlts2=FirstStageAlts1(Best(1:others.n2),:);
FirstStageObs2 = zeros(size(FirstStageAlts2,1),1);
for wind = 1:size(FirstStageAlts2,1)
    FirstStageObs2(wind)=feval(problem,FirstStageAlts2(wind,:),runlength,randi(999999999));
end

FirstStageAlts=[FirstStageAlts1;FirstStageAlts2];
FirstStageObs=[FirstStageObs1;FirstStageObs2(:)];

[PriorCov,SampVarHat,PriorScale,mu0]= MLEofHyperparameters_IndepSamp(FirstStageAlts,FirstStageObs,.5,scale0,others.rescale);

RandStream.setGlobalStream(solverstream);
A = valuerange(randi(LD,[N dim]));

% Standard (sequential) sampling stage
SecondStageAlts = valuerange(randi(LD,[1 dim])); % first sample at random
RandStream.setGlobalStream(probstream);
SecondStageObs=feval(problem,SecondStageAlts,runlength,randi(999999999));
sigma0=PriorCov;
Cov21 = PriorCov*GPCov(1,PriorScale,SecondStageAlts,FirstStageAlts);
fprintf('sample 1 - %d:\n',N+1)
disp([FirstStageAlts;SecondStageAlts])
    
for n = 1:budget(end)-N
    
    RandStream.setGlobalStream(solverstream);
    AllAlts=[FirstStageAlts;SecondStageAlts];
        
    % Occasionally update the MLEs.
    if sum(n==others.UpdateTimes)>0
        AllObs=[FirstStageObs;SecondStageObs];
        [PriorCov,SampVarHat,PriorScale,mu0]= MLEofHyperparameters_IndepSamp(AllAlts,AllObs,.5,scale0,others.rescale);
        
        sigma0=eye(n);
        for i=2:n
            sigma0(i,1:i-1)=GPCov(1,PriorScale,SecondStageAlts(i,:),SecondStageAlts(1:i-1,:));
            sigma0(1:i-1,i)=sigma0(i,1:i-1)';%'
        end
        sigma0=sigma0*PriorCov;
        Cov21 = PriorCov*GPCov(1,PriorScale,SecondStageAlts,FirstStageAlts);
    end

    % Posteriors.
    L=eye(n)*SampVarHat;
    L=sparse(L);

    Yt=SecondStageObs-mu0;
    invS=inv(sigma0+L);

    [Mu,sigma]=Update_Kalman(mu0,sigma0,n,invS,Yt);
    AllMu = [mu0+Cov21'/(sigma0+L)*Yt;Mu]; % posterior means of sampled alternatives (Stage 1+2)

    x01=ChooseRandomElement(find(AllMu==max(AllMu)));
    CurrentBest=AllAlts(x01,:); % current solution
    fprintf('Solution:\n')
    disp(CurrentBest)
    A = [A;CurrentBest];
    
    if n >= budget(end)-N
        break
    end
    
    Bests= sum(abs(AllAlts-repmat(CurrentBest,N+n,1)),2)==0;
    temp=1:N+n;temp(Bests)=[];
    
    % Choose starting points for VOI gradient ascent.
    if (sum(isnan(Mu))>0)
        Cofx=valuerange(randi(LD,[1 dim]));Cofx=(Cofx(:))'; 
    else
            if ~isempty(temp)
                x02=temp(ChooseRandomElement(find(AllMu(temp)==max(AllMu(temp)))));
                SecondBest=AllAlts(x02,:);
                if sum(sum(abs(SecondStageAlts-repmat(SecondBest,n,1)),2)==0)+...
               sum(sum(abs(SecondStageAlts-repmat(CurrentBest,n,1)),2)==0)<length(temp)
                    rdpts = [CurrentBest;SecondBest;valuerange(randi(LD,[998 dim]))];
                else
                    rdpts = [CurrentBest;valuerange(randi(LD,[999 dim]))];
                end
            else
                rdpts = valuerange(randi(LD,[1000 dim]));
            end
        
        vrdpts = zeros(1,1000);
        for start=1:1000
            vrdpts(start)=VOI_TestSingular_Conly(rdpts(start,:),n,Mu,sigma,mu0,sigma0,PriorCov,PriorScale,SampVarHat,invS,SecondStageAlts,Yt);
        end
        [~,order]=sort(vrdpts,'ascend');
        CofX0 = rdpts(order(1:others.NumStarts),:);
                    
        if others.NumStarts == 1
            CofX0=(CofX0(:))';
        else if dim == 1
                CofX0=CofX0(:);
            end
        end

        % Find a point with high VOI using gradient ascent.

        CofX=zeros(others.NumStarts,dim);
        Fval=zeros(others.NumStarts,1);
    
        for start=1:others.NumStarts
            CofX(start,:) = fmincon(@(cc)VOI_TestSingular_Conly(cc,n,Mu,sigma,mu0,sigma0,PriorCov,PriorScale,SampVarHat,invS,SecondStageAlts,Yt),CofX0(start,:),[],[],[],[],valuerange(ones(1,dim)),valuerange(end)*ones(1,dim),[],options);%,'Display','Iter'));
            if (sum(isnan(CofX(start,:)))>0)
                CofX(start,:)=CofX0(start,:);
            else
                CofXD=max(min(valuerange(end),CofX(start,:)),valuerange(1));
                [~,Index]=min(abs(bsxfun(@minus,CofXD,valuerange(:))));
                CofXD=valuerange(Index);
                CofX(start,:)=(CofXD(:))';
            end
            Fval(start) = VOI_TestSingular_Conly(CofX(start,:),n,Mu,sigma,mu0,sigma0,PriorCov,PriorScale,SampVarHat,invS,SecondStageAlts,Yt);
        end
        [~,IS]=min(Fval);
        Cofx=CofX(IS,:);
    end

        % Sampling decision: Cofx. 
        % Sample from a new singleton or a new pair of alternatives.
        fprintf('sample %d:\n',N+n+1)
        disp(Cofx)
        RandStream.setGlobalStream(probstream); 
        
        AlreadySampled=find(sum(abs(SecondStageAlts-repmat(Cofx,n,1)),2)==0);
        y = feval(problem,Cofx,runlength,randi(999999999));

        if (~isempty(AlreadySampled))
            sigma0=[[sigma0,sigma0(:,AlreadySampled(1))];[sigma0(AlreadySampled(1),:),PriorCov]];
            Cov21 = [Cov21;Cov21(AlreadySampled(1),:)];
        else
            RU=GPCov(PriorCov,PriorScale,SecondStageAlts,Cofx);
            sigma0=[[sigma0,RU];[RU',PriorCov]];
            Cov21 = [Cov21;GPCov(PriorCov,PriorScale,Cofx,FirstStageAlts)];
        end
        
        SecondStageAlts=[SecondStageAlts;Cofx];
        SecondStageObs=[SecondStageObs;y];
end
    
% Report the final solutions. size(A,1)=length(budget). 
A = A(budget,:);
end