hold on

plot(1:1000,M)
plot(1:1000,MM)

plot(1:1000,-data32(1:1000))
plot(1:1000,-data31(1:1000))
plot(1:1000,-data33(1:1000))
plot(1:1000,-data34(1:1000))
plot(1:1000,-data21(1:1000))
plot(1:1000,-data22(1:1000))
plot(1:1000,-data23(1:1000))
plot(1:1000,-data24(1:1000))

ISC = [data32(1:1000),data31(1:1000),data34(1:1000),data33(1:1000),...
    data21(1:1000),data22(1:1000),data23(1:1000),data24(1:1000),...
    data11(1:1000),data12(1:1000),data13(1:1000),data14(1:1000),data15(1:1000),...
    d10(1:1000),d11(1:1000),d12(1:1000),d13(1:1000),d14(1:1000),d15(1:1000),...
    d16(1:1000),d17(1:1000),d18(1:1000),d19(1:1000),d110(1:1000),d111(1:1000),...
    d112(1:1000),d113(1:1000),d114(1:1000),d115(1:1000),d116(1:1000),d117(1:1000),...
    d118(1:1000),d119(1:1000),d120(1:1000),d121(1:1000),d122(1:1000),d123(1:1000),...
    d124(1:1000),d125(1:1000),d126(1:1000),d127(1:1000),d128(1:1000),d129(1:1000),...
    d80(1:1000), d81(1:1000), d82(1:1000), d83(1:1000), d84(1:1000), d85(1:1000),...
    d86(1:1000), df0(1:1000), ds0(1:1000), ds1(1:1000), ds2(1:1000),...
    ds3(1:1000), ds4(1:1000)];
MISC = -mean(ISC,2);MISC(1:249)=36*ones(249,1);
plot(1:1000,MISC,'r')

plot(1000,141,'x')

ISCend = [data32(end),data31(end),data34(end),data33(end),...
    data21(end),data22(end),data23(end),data24(end),...
    data11(end),data12(end),data13(end),data14(end),data15(end),...
    d10(end),d11(end),d12(end),d13(end),d14(end),d15(end),...
    d16(end),d17(end),d18(end),d19(end),d110(end),d111(end),...
    d112(end),d113(end),d114(end),d115(end),d116(end),d117(end),...
    d118(end),d119(end),d120(end),d121(end),d122(end),d123(end),...
    d124(end),d125(end),d126(end),d127(end),d128(end),d129(end),...
    d80(end), d81(end), d82(end), d83(end), d84(end), d85(end),...
    d86(end), df0(end), ds0(end), ds1(end), ds2(end),...
    ds3(end), ds4(end)];
MISCend = -mean(ISCend);%115.5336
ISClen = [length(data32),length(data31),length(data34),length(data33),...
    length(data21),length(data22),length(data23),length(data24),...
    length(data11),length(data12),length(data13),length(data14),length(data15),...
    length(d10),length(d11),length(d12),length(d13),length(d14),length(d15),...
    length(d16),length(d17),length(d18),length(d19),length(d110),length(d111),...
    length(d112),length(d113),length(d114),length(d115),length(d116),length(d117),...
    length(d118),length(d119),length(d120),length(d121),length(d122),length(d123),...
    length(d124),length(d125),length(d126),length(d127),length(d128),length(d129),...
    length(d80), length(d81), length(d82), length(d83), length(d84), length(d85),...
    length(d86), length(df0), length(ds0), length(ds1), length(ds2),...
    length(ds3), length(ds4)];
MISClen = -mean(ISClen);%1083.7
