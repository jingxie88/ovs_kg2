function [candidate,num_true_candidate]=true_candidate(candidate0,x0)

[num_candidate,temp]=size(candidate0);
candidate=[];
num_true_candidate=0;
for i=1:num_candidate
    x=candidate0(i,:);
    if sum(x~=x0)>0
        flag=1;
        j=1;
        while (j<=num_true_candidate)&(flag==1)
            if sum(x~=candidate(j,:))==0
                flag=0;
            end
            j=j+1;
        end
        if flag==1
            candidate=[candidate;x];
            num_true_candidate=num_true_candidate+1;
        end
    end
end
