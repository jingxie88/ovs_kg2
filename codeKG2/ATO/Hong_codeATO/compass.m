function result=compass(A,b,x0,budget)

% COMPASS algorithm for constrained problem with equal-sample-allocation
% rule. Constraints take the form Ax>=b. x0 is the starting point,
% x0=[x01,...x0d]
% this is for the assemble to order problem only


visited=x0;     % list of all visited solutions
num_visited=1;
result=[];      % information on the optimization process
interval=50;   % collect information every 500 samples
limit=interval;


n0=5;
temp=[];
for count=1:n0
    temp1=AssembleToOrder(x0);
    temp=[temp;temp1];
end
visited_info=[n0,sum(temp),sum(temp.^2)];
    % sample size, sum and sum of square of all visited solutions
total_samplesize=n0;
x_star=x0;      % x_star is the current best

num_candidate=5;
Ak=A; bk=b;     % constraints for the most promising region
Nk=n0;

iteration=1;
%x_star0=x0;

while total_samplesize<budget
    
    % generate candidate solution 
    
    candidate=RMD(Ak,bk,x_star',num_candidate,30);
    
    
    % eliminate duplicate candidate solution from candidate set, eliminate
    % candidates which are same to x_star
    
    [true_Candidate,num_true_candidate]=true_candidate(candidate,x_star);
    
    % sample true candidates
    
    if num_true_candidate>0.5
        for count=1:num_true_candidate
            x=true_Candidate(count,:);
            visited=[visited;x];
            num_visited=num_visited+1;
            temp=[];
			for count1=1:n0
                temp1=AssembleToOrder(x);
                temp=[temp;temp1];
			end
            total_samplesize=total_samplesize+n0;
            visited_info=[visited_info;[n0,sum(temp),sum(temp.^2)]];
        end
        Nk=max(Nk,ceil(n0*(log(iteration))^1.01));       % update simulation-allocation rule
    else
        singularity=singularity_check(Ak,bk,x_star');
        if singularity==1
            total_samplesize;
            value;
        end
        Nk=Nk+1;  % update simulation-allocation rule
    end     
    
    for count=1:num_visited
        if visited_info(count,1)<Nk
            x=visited(count,:);
			temp=[];
			for count1=1:Nk-visited_info(count,1)
                temp=[temp;AssembleToOrder(x)];
			end
            total_samplesize=total_samplesize+Nk-visited_info(count,1);
            visited_info(count,1)=Nk;
            visited_info(count,2)=visited_info(count,2)+sum(temp);
            visited_info(count,3)=visited_info(count,3)+sum(temp.^2);
        end
    end
    
    % finding current best solution
    
    temp=visited_info(:,2)./visited_info(:,1);
    [value,star_index]=min(temp);
    x_star=visited(star_index,:);
    
    while total_samplesize>=limit
        %if sum(x_star==x_star0)==8
        %    temp=(limit-interval)/interval;
        %    value=result(temp);
        %else
        %     value=ATOobjective(x_star);
        %end
        result=[result;value];
        limit=limit+interval;
        
        x_star0=x_star;
        %x_star
    end
    
    
    % construct the most promising region
    
    Ak=A;
    bk=b;
    for count=1:num_visited
        if count~=star_index
            temp=x_star-visited(count,:);
            Ak=[Ak;temp];
            temp=temp*(x_star+visited(count,:))'/2;
            bk=[bk;temp];
        end
    end
    
  
    iteration=iteration+1;
end
result=x_star;     % this provide the solution; otherwise it is the opt
                    %process will be reported