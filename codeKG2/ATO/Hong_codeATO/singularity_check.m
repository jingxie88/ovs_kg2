function flag=singularity_check(A,b,x0)

% Ax>=b;

b=b-1e-5;  %numerical stability

[num, dim]=size(A);     % num--number of constaints, dim -- dimensionality

i=1;
flag=1;
while (flag==1)&(i<=dim)
    A1=A(:,i);
    A2=A;
    A2(:,i)=zeros(num,1);
    b1=b-A2*x0;
    upper=1e10;    % distance to the polyhedron from + side
    lower=1e10;
    for j=1:num
        if abs(A1(j))>1e-10
            temp=b1(j)/A1(j);
        else
            temp=1e11;
        end
        if temp>x0(i)
            if temp-x0(i)<upper
                upper=temp-x0(i);
            end
        else
            if x0(i)-temp<lower
                lower=x0(i)-temp;
            end
        end
    end
    if (upper>=1)|(lower>=1)
        flag=0;
    end
    i=i+1;
end
        
    

    

