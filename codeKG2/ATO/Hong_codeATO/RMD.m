function candidate=RMD(A,b,X0,n,T)

% Ax>=b;
% x0-- starting solution
% n -- number of candidate solution
% T -- warm-up period

b=b-1e-5;               % numerical stability
[num, dim]=size(A);     % num--number of constaints, dim -- dimensionality

candidate=zeros(dim,n);     % X is column vector
Xcurrent=X0;
for count1=1:n
    for count2=1:T
        I=ceil(rand*dim);
        Atemp=A;
        Atemp(:,I)=[];
        Xtemp=Xcurrent;
        Xtemp(I)=[];
        btemp=b-Atemp*Xtemp;
        Xi=zeros(num,1);
        for count4=1:num
            if A(count4,I)==0;
                Xi(count4)=1e3;
            else
                Xi(count4)=btemp(count4)/A(count4,I);
            end
        end
        maxXi=max(Xi);
        minXi=min(Xi);
        for count3=1:num
            if (Xi(count3)>Xcurrent(I))&(Xi(count3)<maxXi);
                maxXi=Xi(count3);
            end
            if (Xi(count3)<Xcurrent(I))&(Xi(count3)>minXi);
                minXi=Xi(count3);
            end
        end
        if maxXi>0.5*1e10
            maxXi=Xcurrent(I);
        end
        maxXi=floor(maxXi);
        minXi=ceil(minXi);
        Xcurrent(I)=minXi+floor((maxXi-minXi+1)*rand);
        
        
    end
    candidate(:,count1)=Xcurrent;
end
candidate=candidate';