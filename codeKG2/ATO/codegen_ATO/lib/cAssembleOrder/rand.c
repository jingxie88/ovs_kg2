/*
 * rand.c
 *
 * Code generation for function 'rand'
 *
 * C source code generated on: Mon Feb 18 00:32:05 2013
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "cAssembleOrder.h"
#include "rand.h"
#include "randn.h"
#include "cAssembleOrder_data.h"

/* Type Definitions */

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
static void eml_rand_mt19937ar_stateful(real_T r[50005]);

/* Function Definitions */
static void eml_rand_mt19937ar_stateful(real_T r[50005])
{
  int32_T k;
  real_T d0;
  for (k = 0; k < 50005; k++) {
    d0 = genrandu(state);
    r[k] = d0;
  }
}

void b_rand(real_T r[50005])
{
  if (!method_not_empty) {
    method_not_empty = TRUE;
  }

  eml_rand_mt19937ar_stateful(r);
}

/* End of code generation (rand.c) */
