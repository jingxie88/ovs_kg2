/*
 * rand.h
 *
 * Code generation for function 'rand'
 *
 * C source code generated on: Mon Feb 18 00:32:05 2013
 *
 */

#ifndef __RAND_H__
#define __RAND_H__
/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"

#include "rtwtypes.h"
#include "cAssembleOrder_types.h"

/* Type Definitions */

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern void b_rand(real_T r[50005]);
#endif
/* End of code generation (rand.h) */
