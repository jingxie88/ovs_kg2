/*
 * cAssembleOrder.h
 *
 * Code generation for function 'cAssembleOrder'
 *
 * C source code generated on: Mon Feb 18 00:32:05 2013
 *
 */

#ifndef __CASSEMBLEORDER_H__
#define __CASSEMBLEORDER_H__
/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"

#include "rtwtypes.h"
#include "cAssembleOrder_types.h"

/* Type Definitions */

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern real_T cAssembleOrder(const real_T x[8], real_T runlength);
#endif
/* End of code generation (cAssembleOrder.h) */
