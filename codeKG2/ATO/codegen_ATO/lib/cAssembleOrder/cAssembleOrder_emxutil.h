/*
 * cAssembleOrder_emxutil.h
 *
 * Code generation for function 'cAssembleOrder_emxutil'
 *
 * C source code generated on: Mon Feb 18 00:32:05 2013
 *
 */

#ifndef __CASSEMBLEORDER_EMXUTIL_H__
#define __CASSEMBLEORDER_EMXUTIL_H__
/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"

#include "rtwtypes.h"
#include "cAssembleOrder_types.h"

/* Type Definitions */

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern void emxEnsureCapacity(emxArray__common *emxArray, int32_T oldNumel, int32_T elementSize);
extern void emxFree_real_T(emxArray_real_T **pEmxArray);
extern void emxInit_real_T(emxArray_real_T **pEmxArray, int32_T numDimensions);
#endif
/* End of code generation (cAssembleOrder_emxutil.h) */
