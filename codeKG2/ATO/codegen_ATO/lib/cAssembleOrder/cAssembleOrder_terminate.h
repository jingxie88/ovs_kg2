/*
 * cAssembleOrder_terminate.h
 *
 * Code generation for function 'cAssembleOrder_terminate'
 *
 * C source code generated on: Mon Feb 18 00:32:05 2013
 *
 */

#ifndef __CASSEMBLEORDER_TERMINATE_H__
#define __CASSEMBLEORDER_TERMINATE_H__
/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"

#include "rtwtypes.h"
#include "cAssembleOrder_types.h"

/* Type Definitions */

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern void cAssembleOrder_terminate(void);
#endif
/* End of code generation (cAssembleOrder_terminate.h) */
