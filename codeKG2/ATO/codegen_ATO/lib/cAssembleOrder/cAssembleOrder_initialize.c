/*
 * cAssembleOrder_initialize.c
 *
 * Code generation for function 'cAssembleOrder_initialize'
 *
 * C source code generated on: Mon Feb 18 00:32:05 2013
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "cAssembleOrder.h"
#include "cAssembleOrder_initialize.h"
#include "cAssembleOrder_data.h"

/* Type Definitions */

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */

/* Function Definitions */
void cAssembleOrder_initialize(void)
{
  uint32_T r;
  int32_T mti;
  rt_InitInfAndNaN(8U);
  method_not_empty = FALSE;
  memset(&state[0], 0, 625U * sizeof(uint32_T));
  r = 5489U;
  state[0] = 5489U;
  for (mti = 0; mti < 623; mti++) {
    r = ((r ^ r >> 30U) * 1812433253U + (uint32_T)mti) + 1U;
    state[1 + mti] = r;
  }

  memset(&state[0], 0, 625U * sizeof(uint32_T));
  r = 5489U;
  state[0] = 5489U;
  for (mti = 0; mti < 623; mti++) {
    r = ((r ^ r >> 30U) * 1812433253U + (uint32_T)mti) + 1U;
    state[1 + mti] = r;
  }

  state[624] = 624U;
}

/* End of code generation (cAssembleOrder_initialize.c) */
