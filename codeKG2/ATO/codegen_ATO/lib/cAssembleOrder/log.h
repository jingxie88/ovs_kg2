/*
 * log.h
 *
 * Code generation for function 'log'
 *
 * C source code generated on: Mon Feb 18 00:32:05 2013
 *
 */

#ifndef __LOG_H__
#define __LOG_H__
/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"

#include "rtwtypes.h"
#include "cAssembleOrder_types.h"

/* Type Definitions */

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern void b_log(real_T x[10000]);
#endif
/* End of code generation (log.h) */
