/*
 * cAssembleOrder_terminate.c
 *
 * Code generation for function 'cAssembleOrder_terminate'
 *
 * C source code generated on: Mon Feb 18 00:32:05 2013
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "cAssembleOrder.h"
#include "cAssembleOrder_terminate.h"

/* Type Definitions */

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */

/* Function Definitions */
void cAssembleOrder_terminate(void)
{
  /* (no terminate code required) */
}

/* End of code generation (cAssembleOrder_terminate.c) */
