/*
 * log.c
 *
 * Code generation for function 'log'
 *
 * C source code generated on: Mon Feb 18 00:32:05 2013
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "cAssembleOrder.h"
#include "log.h"

/* Type Definitions */

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */

/* Function Definitions */
void b_log(real_T x[10000])
{
  int32_T k;
  for (k = 0; k < 10000; k++) {
    x[k] = log(x[k]);
  }
}

/* End of code generation (log.c) */
