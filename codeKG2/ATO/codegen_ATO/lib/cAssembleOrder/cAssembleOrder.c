/*
 * cAssembleOrder.c
 *
 * Code generation for function 'cAssembleOrder'
 *
 * C source code generated on: Mon Feb 18 00:32:05 2013
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "cAssembleOrder.h"
#include "log.h"
#include "cAssembleOrder_emxutil.h"
#include "randn.h"
#include "rand.h"

/* Type Definitions */

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
static void eml_sort(const real_T x_data[8000], const int32_T x_size[2], real_T
                     y_data[8000], int32_T y_size[2]);
static void eml_sort_idx(const real_T x_data[1000], const int32_T x_size[1],
  int32_T idx_data[1000], int32_T idx_size[1]);

/* Function Definitions */
static void eml_sort(const real_T x_data[8000], const int32_T x_size[2], real_T
                     y_data[8000], int32_T y_size[2])
{
  real_T vwork_data[1000];
  int32_T vwork_size[1];
  int32_T i1;
  int32_T j;
  int32_T ix;
  int32_T k;
  int32_T iidx_size[1];
  int32_T iidx_data[1000];
  vwork_size[0] = (int16_T)x_size[1];
  for (i1 = 0; i1 < 2; i1++) {
    y_size[i1] = x_size[i1];
  }

  i1 = -1;
  for (j = 0; j < 8; j++) {
    i1++;
    ix = i1;
    for (k = 0; k <= x_size[1] - 1; k++) {
      vwork_data[k] = x_data[ix];
      ix += 8;
    }

    eml_sort_idx(vwork_data, vwork_size, iidx_data, iidx_size);
    ix = i1;
    for (k = 0; k <= x_size[1] - 1; k++) {
      y_data[ix] = vwork_data[iidx_data[k] - 1];
      ix += 8;
    }
  }
}

static void eml_sort_idx(const real_T x_data[1000], const int32_T x_size[1],
  int32_T idx_data[1000], int32_T idx_size[1])
{
  int32_T np1;
  int32_T k;
  boolean_T p;
  int32_T i;
  int32_T idx0_data[1000];
  int32_T i2;
  int32_T j;
  int32_T pEnd;
  int32_T b_p;
  int32_T q;
  int32_T qEnd;
  int32_T kEnd;
  idx_size[0] = (int16_T)x_size[0];
  np1 = x_size[0] + 1;
  if (x_size[0] == 0) {
  } else {
    for (k = 1; k <= x_size[0]; k++) {
      idx_data[k - 1] = k;
    }

    for (k = 1; k <= x_size[0] - 1; k += 2) {
      if ((x_data[k - 1] <= x_data[k]) || rtIsNaN(x_data[k])) {
        p = TRUE;
      } else {
        p = FALSE;
      }

      if (p) {
      } else {
        idx_data[k - 1] = k + 1;
        idx_data[k] = k;
      }
    }

    k = x_size[0] - 1;
    for (i = 0; i <= k; i++) {
      idx0_data[i] = 1;
    }

    i = 2;
    while (i < x_size[0]) {
      i2 = i << 1;
      j = 1;
      for (pEnd = 1 + i; pEnd < np1; pEnd = qEnd + i) {
        b_p = j;
        q = pEnd - 1;
        qEnd = j + i2;
        if (qEnd > np1) {
          qEnd = np1;
        }

        k = 0;
        kEnd = qEnd - j;
        while (k + 1 <= kEnd) {
          if ((x_data[idx_data[b_p - 1] - 1] <= x_data[idx_data[q] - 1]) ||
              rtIsNaN(x_data[idx_data[q] - 1])) {
            p = TRUE;
          } else {
            p = FALSE;
          }

          if (p) {
            idx0_data[k] = idx_data[b_p - 1];
            b_p++;
            if (b_p == pEnd) {
              while (q + 1 < qEnd) {
                k++;
                idx0_data[k] = idx_data[q];
                q++;
              }
            }
          } else {
            idx0_data[k] = idx_data[q];
            q++;
            if (q + 1 == qEnd) {
              while (b_p < pEnd) {
                k++;
                idx0_data[k] = idx_data[b_p - 1];
                b_p++;
              }
            }
          }

          k++;
        }

        for (k = -1; k + 2 <= kEnd; k++) {
          idx_data[j + k] = idx0_data[k + 1];
        }

        j = qEnd;
      }

      i = i2;
    }
  }
}

real_T cAssembleOrder(const real_T x[8], real_T runlength)
{
  emxArray_real_T *itemAvailability;
  int32_T ix;
  real_T Inventory[8];
  real_T ProfitAfter20;
  static real_T OandA[50005];
  real_T Orders[5];
  static real_T Arrival[50000];
  int32_T k;
  static const real_T items[45] = { 3.6, 3.0, 2.4, 1.8, 1.2, 1.0, 1.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 0.0, 1.0, 1.0,
    0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.0, 0.0,
    1.0, 0.0, 0.0, 0.0, 1.0, 0.0 };

  real_T dv0[10000];
  real_T A[5];
  static real_T ProdTime[20000];
  int32_T p;
  int32_T q;
  real_T prevMinTime;
  emxArray_real_T *b_itemAvailability;
  int32_T exitg1;
  int32_T ixstart;
  real_T keyavail;
  boolean_T exitg2;
  real_T minTime;
  int32_T minProd;
  int32_T tmp_size[2];
  real_T tmp_data[8000];
  int32_T loop_ub;
  int32_T i0;
  int32_T sizeIA;
  static const real_T parameters[40] = { 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0,
    2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 0.15, 0.4, 0.25, 0.15, 0.25, 0.08,
    0.13, 0.4, 0.0225, 0.06, 0.0375, 0.0225, 0.0375, 0.012, 0.0195, 0.06, 20.0,
    20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0 };

  /*    *************************************** */
  /*    *** Code written by German Gutierrez*** */
  /*    ***         gg92@cornell.edu        *** */
  /*    *************************************** */
  /* Returns Profit and Profit after T = 20. */
  /* , seed) */
  /*  function [fn, FnVar, FnGrad, FnGradCov, constraint, ConstraintCov, ConstraintGrad, ConstraintGradCov] = AssembleOrder(x, runlength, seed, other); */
  /*  x is a vector containing the amounts of intermediate products to be processed ahead of time  */
  /*  runlength is the number of hours of simulated time to simulate */
  /*  seed is the index of the substreams to use (integer >= 1) */
  /*  other is not used */
  /*  main simulation */
  /* Both matrices as defined in problem statement */
  /* Any proposed solution must satisfy bk<=ck  */
  /* # of products, key items and non key items respectively. */
  /* 70                                         %Length of simulation */
  /*  Starting Sol: parameters(:,5) */
  emxInit_real_T(&itemAvailability, 2);

  /* Next order arrival times */
  /*  ItemAvailability contains the time at which a replenishment order for a */
  /*  given item(row) will be ready. */
  ix = itemAvailability->size[0] * itemAvailability->size[1];
  itemAvailability->size[0] = 8;
  itemAvailability->size[1] = 1;
  emxEnsureCapacity((emxArray__common *)itemAvailability, ix, (int32_T)sizeof
                    (real_T));
  for (ix = 0; ix < 8; ix++) {
    itemAvailability->data[ix] = 0.0;
  }

  memcpy(&Inventory[0], &x[0], sizeof(real_T) << 3);

  /* Tracks available inventory for each item */
  ProfitAfter20 = 0.0;

  /* number of generated outputs */
  /* randn('state',seed);rand('twister',seed); */
  /* Generate time of next order arrival per product */
  b_rand(OandA);
  for (k = 0; k < 5; k++) {
    Orders[k] = -log(OandA[k]) / items[k];

    /* exprnd(1/items(k,1)); */
    for (ix = 0; ix < 10000; ix++) {
      dv0[ix] = OandA[k + 5 * (1 + ix)];
    }

    b_log(dv0);
    for (ix = 0; ix < 10000; ix++) {
      Arrival[k + 5 * ix] = -dv0[ix] / items[k];
    }

    /* exprnd(1/items(k,1),1,nGen); */
  }

  for (ix = 0; ix < 5; ix++) {
    A[ix] = 1.0;
  }

  randn(ProdTime);
  p = 0;

  /* Generate Uniforms used to generate orders */
  q = 0;
  prevMinTime = 0.0;

  /* Holds the time of previous order fulfilled (to calculate holding cost) */
  /* **** Loop through orders, as they happened (smallest time first) and identify **** */
  /* **** whether key and non-key items are available, calculate profit, etc.     **** */
  /* While there are orders to be satisfied. */
  emxInit_real_T(&b_itemAvailability, 2);
  do {
    exitg1 = 0;
    ixstart = 1;
    keyavail = Orders[0];
    if (rtIsNaN(Orders[0])) {
      ix = 2;
      exitg2 = FALSE;
      while ((exitg2 == 0U) && (ix < 6)) {
        ixstart = ix;
        if (!rtIsNaN(Orders[ix - 1])) {
          keyavail = Orders[ix - 1];
          exitg2 = TRUE;
        } else {
          ix++;
        }
      }
    }

    if (ixstart < 5) {
      while (ixstart + 1 < 6) {
        if (Orders[ixstart] < keyavail) {
          keyavail = Orders[ixstart];
        }

        ixstart++;
      }
    }

    if (keyavail <= runlength) {
      /*  find next order to satisfy, i.e. smallest time of orders not yet */
      /*  satisfied. */
      minTime = runlength;

      /* keeps track of minimum order time found so far. */
      for (ixstart = 0; ixstart < 5; ixstart++) {
        if (Orders[ixstart] <= minTime) {
          minTime = Orders[ixstart];

          /*  Time of next order to fulfill */
          minProd = ixstart;

          /*  Product of next order to fulfill */
        }
      }

      Orders[minProd] += Arrival[minProd + 5 * ((int32_T)A[minProd] - 1)];

      /* generate time of next order */
      A[minProd]++;

      /* update inventory levels up to time of next order (order to be */
      /* fulfilled in this iteration) i.e. add however many items are available */
      /* by minTime. */
      eml_sort(itemAvailability->data, itemAvailability->size, tmp_data,
               tmp_size);
      ix = itemAvailability->size[0] * itemAvailability->size[1];
      itemAvailability->size[0] = 8;
      itemAvailability->size[1] = tmp_size[1];
      emxEnsureCapacity((emxArray__common *)itemAvailability, ix, (int32_T)
                        sizeof(real_T));
      loop_ub = tmp_size[0] * tmp_size[1] - 1;
      for (ix = 0; ix <= loop_ub; ix++) {
        itemAvailability->data[ix] = tmp_data[ix];
      }

      ixstart = 0;

      /* Delete all zero columns */
      for (loop_ub = 0; loop_ub < 8; loop_ub++) {
        ix = 0;
        for (k = 0; k <= tmp_size[1] - 1; k++) {
          if (tmp_data[loop_ub + tmp_size[0] * k] != 0.0) {
            ix++;
          }
        }

        if (ix >= ixstart) {
          ixstart = 0;
          for (k = 0; k <= tmp_size[1] - 1; k++) {
            if (tmp_data[loop_ub + tmp_size[0] * k] != 0.0) {
              ixstart++;
            }
          }
        }
      }

      if (ixstart > 0) {
        ix = (tmp_size[1] - ixstart) + 1;
        if (ix > tmp_size[1]) {
          ix = 0;
          i0 = 0;
        } else {
          ix--;
          i0 = tmp_size[1];
        }

        /* (1:end,1:sizeIA-maxEntries)=[]; */
        ixstart = itemAvailability->size[0] * itemAvailability->size[1];
        itemAvailability->size[0] = 8;
        itemAvailability->size[1] = i0 - ix;
        emxEnsureCapacity((emxArray__common *)itemAvailability, ixstart,
                          (int32_T)sizeof(real_T));
        loop_ub = (i0 - ix) - 1;
        for (i0 = 0; i0 <= loop_ub; i0++) {
          for (ixstart = 0; ixstart < 8; ixstart++) {
            itemAvailability->data[ixstart + itemAvailability->size[0] * i0] =
              tmp_data[ixstart + tmp_size[0] * (ix + i0)];
          }
        }
      }

      sizeIA = itemAvailability->size[1];

      /* Add inventory that is now ready to be used. */
      for (loop_ub = 0; loop_ub < 8; loop_ub++) {
        for (ixstart = 0; ixstart <= sizeIA - 1; ixstart++) {
          if ((itemAvailability->data[loop_ub + itemAvailability->size[0] *
               ixstart] != 0.0) && (itemAvailability->data[loop_ub +
               itemAvailability->size[0] * ixstart] <= minTime)) {
            Inventory[loop_ub]++;
            itemAvailability->data[loop_ub + itemAvailability->size[0] * ixstart]
              = 0.0;
          }
        }
      }

      /* if(all key items available) make product, calculate profit, update */
      /* inventory and trigger replenishment orders */
      /* loop to check if all key items are available: */
      keyavail = 0.0;
      for (ixstart = 0; ixstart < 6; ixstart++) {
        if (items[minProd + 5 * (1 + ixstart)] <= Inventory[ixstart]) {
          keyavail++;
        }
      }

      if (keyavail == 6.0) {
        /* all key items available */
        /* Add profit made from making this product */
        if (minTime >= 20.0) {
          /* To keep track of profit made after time 20 */
          keyavail = 0.0;
          for (ix = 0; ix < 6; ix++) {
            keyavail += items[minProd + 5 * (1 + ix)] * (1.0 + (real_T)ix);
          }

          ProfitAfter20 += keyavail;
        }

        for (k = 0; k < 6; k++) {
          /*  Decrease inventory and place replenishment orders for the amount of key items used */
          if (items[minProd + 5 * (1 + k)] == 1.0) {
            Inventory[k]--;
            if (itemAvailability->size[1] < sizeIA + 1) {
              ix = b_itemAvailability->size[0] * b_itemAvailability->size[1];
              b_itemAvailability->size[0] = itemAvailability->size[0];
              b_itemAvailability->size[1] = itemAvailability->size[1] + 1;
              emxEnsureCapacity((emxArray__common *)b_itemAvailability, ix,
                                (int32_T)sizeof(real_T));
              loop_ub = itemAvailability->size[1] - 1;
              for (ix = 0; ix <= loop_ub; ix++) {
                ixstart = itemAvailability->size[0] - 1;
                for (i0 = 0; i0 <= ixstart; i0++) {
                  b_itemAvailability->data[i0 + b_itemAvailability->size[0] * ix]
                    = itemAvailability->data[i0 + itemAvailability->size[0] * ix];
                }
              }

              for (ix = 0; ix < 8; ix++) {
                b_itemAvailability->data[ix + b_itemAvailability->size[0] *
                  itemAvailability->size[1]] = 0.0;
              }

              ix = itemAvailability->size[0] * itemAvailability->size[1];
              itemAvailability->size[0] = b_itemAvailability->size[0];
              itemAvailability->size[1] = b_itemAvailability->size[1];
              emxEnsureCapacity((emxArray__common *)itemAvailability, ix,
                                (int32_T)sizeof(real_T));
              loop_ub = b_itemAvailability->size[1] - 1;
              for (ix = 0; ix <= loop_ub; ix++) {
                ixstart = b_itemAvailability->size[0] - 1;
                for (i0 = 0; i0 <= ixstart; i0++) {
                  itemAvailability->data[i0 + itemAvailability->size[0] * ix] =
                    b_itemAvailability->data[i0 + b_itemAvailability->size[0] *
                    ix];
                }
              }
            }

            keyavail = itemAvailability->data[k + itemAvailability->size[0] *
              (sizeIA - 1)];
            if ((minTime >= keyavail) || rtIsNaN(keyavail)) {
              keyavail = minTime;
            }

            itemAvailability->data[k + itemAvailability->size[0] * sizeIA] =
              keyavail + (parameters[24 + k] * ProdTime[p] + parameters[16 + k]);
            p++;
          }
        }

        /*  For each non-key item available, use it, decrease inventory, increase profit and place replenishment order. */
        for (ixstart = 0; ixstart < 2; ixstart++) {
          if ((items[minProd + 5 * (7 + ixstart)] <= Inventory[6 + ixstart]) &&
              (items[minProd + 5 * (7 + ixstart)] == 1.0)) {
            /* Update profit, inventory and place replenishment orders */
            Inventory[6 + ixstart]--;
            keyavail = itemAvailability->data[(ixstart + itemAvailability->size
              [0] * (sizeIA - 1)) + 6];
            if ((minTime >= keyavail) || rtIsNaN(keyavail)) {
              keyavail = minTime;
            }

            itemAvailability->data[(ixstart + itemAvailability->size[0] * sizeIA)
              + 6] = keyavail + (parameters[ixstart + 30] * ProdTime[q + 10000]
                                 + parameters[ixstart + 22]);
            q++;
            if (minTime >= 20.0) {
              ProfitAfter20 += parameters[6 + ixstart] * items[minProd + 5 * (7
                + ixstart)];
            }
          }
        }
      }

      if (minTime >= 20.0) {
        keyavail = 0.0;
        for (k = 0; k < 8; k++) {
          keyavail += Inventory[k] * 2.0;
        }

        ProfitAfter20 -= keyavail * (minTime - prevMinTime);
      }

      prevMinTime = minTime;
    } else {
      exitg1 = 1;
    }
  } while (exitg1 == 0U);

  emxFree_real_T(&b_itemAvailability);
  emxFree_real_T(&itemAvailability);

  /* fn=Profit; */
  return ProfitAfter20 / 50.0;
}

/* End of code generation (cAssembleOrder.c) */
