/*
 * cAssembleOrder_data.h
 *
 * Code generation for function 'cAssembleOrder_data'
 *
 * C source code generated on: Mon Feb 18 00:32:05 2013
 *
 */

#ifndef __CASSEMBLEORDER_DATA_H__
#define __CASSEMBLEORDER_DATA_H__
/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"

#include "rtwtypes.h"
#include "cAssembleOrder_types.h"

/* Type Definitions */

/* Named Constants */

/* Variable Declarations */
extern boolean_T method_not_empty;
extern uint32_T state[625];

/* Variable Definitions */

/* Function Declarations */
#endif
/* End of code generation (cAssembleOrder_data.h) */
