%   ***************************************
%   *** Code written by German Gutierrez***
%   ***         gg92@cornell.edu        ***
%   ***************************************

%Returns Profit and Profit after T = 20.
function [fn, FnVar, FnGrad, FnGradCov, constraint, ConstraintCov, ConstraintGrad, ConstraintGradCov] = AssembleOrder(X, runlength, seed, other)
% function [fn, FnVar, FnGrad, FnGradCov, constraint, ConstraintCov, ConstraintGrad, ConstraintGradCov] = AssembleOrder(x, runlength, seed, other);
% x is a vector containing the amounts of intermediate products to be processed ahead of time 
% runlength is the number of hours of simulated time to simulate
% seed is the index of the substreams to use (integer >= 1)
% other is not used

FnGrad = NaN;
FnGradCov = NaN;
constraint = NaN;
ConstraintCov = NaN;
ConstraintGrad = NaN;
ConstraintGradCov = NaN;

if (sum(X(:) < 0)>0 ) || (runlength <= 0) || (runlength ~= round(runlength)) || (seed <= 0) || (round(seed) ~= seed),
    fprintf('x (row vector with %d components)\nx components should be between 0 and 1\nrunlength should be positive and real,\nseed should be a positive integer\n', nAmbulances*2);
    fn = NaN;
    FnVar = NaN;
    
else % main simulation

parameters = [  1,      2,      .15,   .0225,   20;
                2,      2,      .40,    .06,    20;
                3,      2,      .25,    .0375,  20;
                4,      2,      .15,    .0225,  20;
                5,      2,      .25,    .0375,  20;
                6,      2,      .08,    .012,   20;
                7,      2,      .13,    .0195,  20;
                8,      2,      .40,    .06,    20];

items = [   3.6,    1,  0,  0,  1,  0,  1,  1,  0;
            3,      1,  0,  0,  0,  1,  1,  1,  0;
            2.4,    0,  1,  0,  1,  0,  1,  0,  0;
            1.8,    0,  0,  1,  1,  0,  1,  0,  1;
            1.2,    0,  0,  1,  0,  1,  1,  1,  0];
%Both matrices as defined in problem statement

%Any proposed solution must satisfy bk<=ck 
nProducts=5; numberkey=6; numbernk=2;            %# of products, key items and non key items respectively.
Tmax=runlength; %70                                         %Length of simulation
fn=[];

for count = 1:size(X,1)
    x = X(count,:); 
bk=x'; % Starting Sol: parameters(:,5)

Orders=zeros(nProducts,1);                       %Next order arrival times
% ItemAvailability contains the time at which a replenishment order for a
% given item(row) will be ready.
itemAvailability=zeros(numberkey+numbernk,1);  

Inventory= bk;   %Tracks available inventory for each item
Profit=0;
ProfitAfter20=0;

nGen=10000; %number of generated outputs

% Generate new streams for call arrivals, call 
[ArrivalStream, ProductionKeyStream, ProductionNonKeyStream] = RandStream.create('mrg32k3a', 'NumStreams', 3);

% Set the substream to the "seed"
ArrivalStream.Substream = seed;
ProductionKeyStream.Substream = seed;
ProductionNonKeyStream.Substream = seed;

% Generate random data
OldStream = RandStream.setGlobalStream(ArrivalStream); % Temporarily store old stream

Arrival=zeros(nProducts,nGen);
%Generate time of next order arrival per product
for k=1:nProducts
    Orders(k,1)=exprnd(1/items(k,1));
    Arrival(k,:)=exprnd(1/items(k,1),1,nGen);
end

A=ones(1,nProducts);

% Generate production times
RandStream.setGlobalStream(ProductionKeyStream);
ProdTimeKey= normrnd(0,1,1,nGen);
p=1;

%Generate Uniforms used to generate orders
RandStream.setGlobalStream(ProductionNonKeyStream);
ProdTimeNonKey= normrnd(0,1,1,nGen);
q=1;

% Restore old random number stream
RandStream.setGlobalStream(OldStream);



prevMinTime=0;      %Holds the time of previous order fulfilled (to calculate holding cost)

%**** Loop through orders, as they happened (smallest time first) and identify ****
%**** whether key and non-key items are available, calculate profit, etc.     ****

%While there are orders to be satisfied.
while(min(Orders)<=Tmax)    
    % find next order to satisfy, i.e. smallest time of orders not yet
    % satisfied.
    minTime=Tmax;                            %keeps track of minimum order time found so far.
    for j=1:nProducts
        if(Orders(j)<=minTime)
            minTime=Orders(j);                            % Time of next order to fulfill
            minProd=j;                                    % Product of next order to fulfill
        end
    end
    Orders(minProd)=Orders(minProd)+Arrival(minProd,A(minProd)); %generate time of next order
    A(minProd)=A(minProd)+1; 

    %update inventory levels up to time of next order (order to be
    %fulfilled in this iteration) i.e. add however many items are available
    %by minTime.
    itemAvailability=sort(itemAvailability,2);
    maxEntries=0;
    %Delete all zero columns
    for i=1:numberkey+numbernk
        if nnz(itemAvailability(i,:)) >= maxEntries
            maxEntries=nnz(itemAvailability(i,:));
        end
    end
    sizeIA=size(itemAvailability,2);
    if maxEntries>0
        itemAvailability(:,1:sizeIA-maxEntries)=[];
    end
    sizeIA=size(itemAvailability,2);
    %Add inventory that is now ready to be used.
    for i=1:numberkey+numbernk
        for j=1:sizeIA
            if(itemAvailability(i,j)~=0 && itemAvailability(i,j)<=minTime)
                Inventory(i)=Inventory(i)+1;
                itemAvailability(i,j)=0;
            end
        end
    end

    %if(all key items available) make product, calculate profit, update
    %inventory and trigger replenishment orders

    %loop to check if all key items are available:
    keyavail=0;
    for j=1:numberkey
        if items(minProd,j+1)<=Inventory(j)
            keyavail=keyavail+1;
        end
    end
    if(keyavail==numberkey)             %all key items available
        Profit=Profit+items(minProd,2:numberkey+1)*parameters(1:numberkey,1);               %Add profit made from making this product
        if(minTime>=20)
            %To keep track of profit made after time 20
            ProfitAfter20=ProfitAfter20+items(minProd,2:numberkey+1)*parameters(1:numberkey,1);
        end
        for k=1:numberkey
            % Decrease inventory and place replenishment orders for the amount of key items used
            if items(minProd,k+1)==1
                Inventory(k)=Inventory(k)-1;
                itemAvailability(k,sizeIA+1)=max(minTime,itemAvailability(k,sizeIA))+(parameters(k,4)*ProdTimeKey(p)+parameters(k,3));
                p=p+1;
            end
        end
        % For each non-key item available, use it, decrease inventory, increase profit and place replenishment order.
        for j=1:numbernk
            if(items(minProd,j+numberkey+1)<=Inventory(j+numberkey) && items(minProd,j+numberkey+1)==1)
                %Update profit, inventory and place replenishment orders
                Profit=Profit+parameters(j+numberkey,1);
                Inventory(j+numberkey)=Inventory(j+numberkey)-1;
                itemAvailability(j+numberkey,sizeIA+1)=max(minTime,itemAvailability(j+numberkey,sizeIA))+(parameters(j+numberkey,4)*ProdTimeNonKey(q)+parameters(j+numberkey,3));
                q=q+1;
                if(minTime>=20)
                    ProfitAfter20=ProfitAfter20+parameters(j+numberkey,1)*items(minProd,j+numberkey+1);
                end
            end
        end
    end
    Profit=Profit-sum(Inventory'*parameters(:,2))*(minTime-prevMinTime);
    if(minTime>=20)
        ProfitAfter20=ProfitAfter20-sum(Inventory'*parameters(:,2))*(minTime-prevMinTime);
    end
    prevMinTime=minTime;
end

%fn=Profit;
fn = [fn;ProfitAfter20/50];
end

FnVar = NaN;
end



        
