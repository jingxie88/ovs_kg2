function [solutions,times,debug] = AKG2(x0,budget,problem,valuerange,probparam,solverparam,probseed,solverseed)
% Outputs:
% "solutions" is a list containing solutions to the simulation optimization
% problem, calculated by the algorithm at each of the times specified in the
% vector "budget".  It has length(budget) rows, and a number of columns equal
% to the dimension of the problem's feasible space.
%
% "times" is a vector with length(budget) rows.  times(i) contains the elapsed
% time between the start of AKG2 and the end of sample number budget(i).
%
% "debug" is a structure that can contain algorithm specific debugging
% information.  In the case of AKG2, it returns this quantity:
%   VOI: a vector of the value of information of the chosen sampling decision.
%
% Inputs:
%
% "x0" is a set of starting values to include in our initial stage of samples.
% The number of rows should be equal to the value of 'numinitsols' passed in
% solverparam.
% The number of columns should be equal to the problem dimension, as passed in
% the value of 'dim' in probparam.
% If there are no initial solutions, then this can be set to an empty vector.
%
% "budget" is a list of times (in terms of number of samples taken) at which we
% would like the algorithm to report its current solution.  If you just want
% the solution after 100 samples, set budget = [100].  If you want solutions at
% all times, from 1 to 100, set budget=[1:100].
%
% "problem" is a function handle, in the simopt format, that allows taking a
% sample.  It takes input (x,runlength,seed,other).
% In the simopt format, the output vector is
% [fn, FnVar, FnGrad, FnGradCov,...
%       constraint, ConstraintCov, ConstraintGrad, ConstraintGradCov]
% However, we only use the first output argument, the function value.
%
% "valuerange" is a vector in ascending order specifying the discrete value set
% of each dimension of the solution space.   For example, if our feasible space
% is the 8-dimensional collection of points {0,1,...,20}^8 considered in the ATO
% problem, then valuerange would be [0:20].
%
% pf: if the valuerange is different in each dimension, the current version of
% this code allow that.  A potential to-do item would be to tweak the code in a
% few places to allow this, but it would not be an impossible task.
%
% "probparam" is a struct containing three fields, dim, runlength, and other.
% probparam.dim is the dimension of the problem's feasible space.
% probparam.runlength is the runlength argument passed to the problem function handle.
% probparam.other is the other argument passed to the problem function handle.
% For example, we could specify,
% probparam = struct('dim',8,'runlength',10,'other',0);
%
% "solverparam" is a structure that specifies the way in which the solver will
% be run.  If you don't have any "other" arguments to passed, it can be left
% blank, as in this example: solverparam = struct();
%
% "solverparam.numinitsols" is the number of initial solutions x0 passed to the
% algorithm. Our algorithm uses these initial solutions in the first stage of
% samples. If passed, this should be equal to the number of rows in x0.  This
% argument is optional.
%
% 'solverparam.numfinalsols' is the number of final solutions that will be
% produced by the algorithm.  If passed, this should be the length of budget.
% This argument is optional.
%
% "solverparam.other" is a structure that specifies the tunable parameters of
% the algorithm.  See the example below for how to set parameters, and for an
% explanation of each parameter. "other"" is optional, and if left blank,
% reasonable defaults will be used.  In specifying "other", not all parameters
% need to be filled in --- those parameters left out will be filled in with
% reasonable defaults.
%
% Here is an example of one possible setting for solverparam.other:
% solverparam.other = struct('n1',10*dim,'n2',2*dim,'rescale',1,...
% 'UpdateTimes',[30,60,100,150,200:100:N],...
% 'NumRandomStartS',1,...
% 'NumRandomStartP',2,...
% 'NumRandomScreenS',998,...
% 'NumRandomScreenP',499,...
% 'IncludeBestSingleton',1,...
% 'IncludeSecondBestSingleton',1,...
% 'IncludeBestSecondBestPair',1,...
% 'DoLocalSearch',1,...
% 'UseFirstStageSamples',1);
%
% To run AKG (not AKG2), set IncludeBestSecondBestPair to 0, set
% NumRandomStartP to 0, and set NumRandomScreenP' to 499.
%
% n1,n2: Together, n1 and n2 specify the number of samples to do in a first
% stage of sampling.  First, n1 points are selected uniformly at random and
% sampled together with one seed.  Then, the n2 points among the first n2 with
% the best performance are selected to be sampled again, with a second seed.
% The default values are n1=10*dim, n2=2*dim.
% 
% rescale: this is passed to MLEofHyperparameters, and divides the estimate of
% the scale parameter.  By default, this is set to 1, so that it does not alter
% the output of MLEofHyperparameters.  At various points in the development of
% this code, we also played around with rescale=2, and rescale=sqrt(2).
% Sometimes doing this improves performance.
%
% UpdateTimes: this is a vector containing the times at which we should update
% the MLE estimates of the hyperparameters.  It is expressed in terms of the
% number of iterations in the second stage of samples.  This does not include
% the update done at the beginning of the second stage of sampling, before any
% second-stage samples are taken.  So if n indexes the number of samples that
% have already been taken in the second stage, then updates are done at times
% n=0, n=UpdateTimes(1), n=UpdateTimes(2), ...
% all the way until n reaches budget(end)-N, where N=other.n1+other.n2 is the
% number of samples in the first stage.
%
% NumRandomStartS, NumRandomStartP: this is the number of random singletons
% (for NumRandomStartS) or pairs (for NumRandomStartP) from which to
% start local search (in addition to any local searches started at the
% singletons and pairs implied by the Include* flags) when finding the
% singleton or pair with largest VOI.
%
% pf: what should the new default value be?
%
% In the past, when Jing ran the experiments, we set default values of
% NumRandomStartS = 1 and NumRandomStartP = 2, so that so that NumStartS and
% NumStartP were both 3 when both IncludeBestSingleton and
% IncludeSecondBestSingleton were true.
%
% NumRandomScreenS, NumRandomScreenP: this is the number of random singletons
% (for NumRandomScreenS) or pairs (for NumRandomScreenP) to generate and
% screen, when looking for those singletons and pairs at which to start local
% searches.  To screen, we first calculate the VOI for all generated singletons
% and pairs, including both randomly generated singletons/pairs and those singletons/pairs
% created by the Include* flags, and take the top
% NumStartS singletons, and the top NumStartP pairs, where we calculate
% NumStartS = IncludeBestSingleton + IncludeSecondBestSingleton + NumRandomStartS.
% NumStartP = IncludeBestSecondBestPair + IncludeSecondBestSingleton + NumRandomStartP.

% If NumRandomScreenS = NumRandomStartS and NumRandomScreenP = NumRandomStartP,
% then we do not do any additional screening.
% If NumRandomScreenS < NumRandomScreenP, we generate a
% warning message, and set it equal, with similar behavior for NumRandomScreenP.

% In the past, when Jing ran these experiments, the default value of
% NumRandomScreenS was 998, so that NumStartS was 1000 when both
% IncludeBestSingleton and IncludeSecondBestSingleton were true.  Also, the
% default value of NumScreenP was 499, so that NumStarP was 500 when
% IncludeBestSecondBestPair was true.
%
% IncludeBestSingleton: whether to include the best alternatives in the set of
% singletons screened.  Default TRUE.
%
% IncludeSecondBestSingleton: whether to include the second-best alternatives
% in the set of singletons screened.  Default TRUE.
%
% IncludeBestSecondBestPair: whether to include the pair given by the best and
% second-best in the set of pairs screened.  Default TRUE.
%
% DoLocalSearch: whether or not to do a gradient-based local search starting at
% the measurements produced by the screening process (which is NumScreenS
% singletons and NumScreenP pairs).  If we don't do a gradient-based local
% search, then our "local search" returns back the starting point, and we will
% choose the singleton or pair to measure that has the largest VOI among those
% generated in the screening stage.  Default TRUE.
%
% UseFirstStageSamples: whether or not to include the first-stage samples when
% updating the posterior mean.  If this is false, then we only use them to
% estimate the hyperparameters.  If this is true, it is used both to estimate
% hyperparameters, and to update the posterior mean.  When Jing ran her
% experiments, she set this flag to be false.
%
% MaxSeed: The maximum value for probseed and solverseed is MaxSeed-1.  If you
% want to perform m independent replications of this algorithm on a problem, then 
% should set MaxSeed to be m or larger.  MaxSeed defaults to 1000.
%
% probseed: "seed" (really an initial substream index) for random numbers used
% to evaluate the objective function.  Must be between 0 and MaxSeed-1.  The
% nth objective function evaluation (counting pairs as a single evaluation) is
% performed by passing passed_seed=(probseed + n*MaxSeed) as the seed to the
% problem function handle, where n starts at 1.  This function handle is
% expected to take this passed seed and use the random number substream given
% by this chunk of code:
%   probstream = RandStream.create('mrg32k3a', 'NumStreams', 1);
%   probstream.Substream = passed_seed;
% We rely on the objective function to put back the old GlobalStream after exiting.
%
% solverseed: "seed" (really an initial substream index) for random numbers
% used for doing randomization within the algorithm.  Must be between 0 and
% MaxSeed-1.  The substream used is the one given by
%   solverstream = RandStream.create('mrg32k3a', 'NumStreams', 1);
%   solverstream.Substream = solverseed;
% With this construction, seeds 0 through MaxSeed-1 are reserved for the solver
% (one seed per run of the solver), and seeds equal to MaxSeed or larger are
% reserved to be passed to the objective function.  Moreorover, there is no
% overlap in the seeds passed to the objective function across different calls
% to the solver code, as long as probseed is different.

% Save the time at which we start.  Later, we compare against this to report
% elapsed time in the "times" return value.
start_time = tic;

% Parameter Settings

% Read in and check probparam
if (~isfield(probparam,'dim'))
    error('The probparam struct does not specify the dim field.')
end
if (~isfield(probparam,'runlength'))
    error('The probparam struct does not specify the runlength field.')
end
if (~isfield(probparam,'other'))
    % pf: should we make this argument optional, and not pass it if it is not specified?
    error('The probparam struct does not specify the "other" field.')
end
dim = probparam.dim;
runlength = probparam.runlength;
if (dim<=0 || length(dim)>1 || fix(dim)~=dim)
    error('probparam(1) should be a scalar strictly positive integer.  It is the problem dimension.')
end
if (runlength<=0 || length(runlength)>1 || ~isnumeric(runlength) || fix(runlength)~=runlength)
    error('probparam(2) should be a scalar strictly positive integer.  It is the problem runlength.')
end
if (size(x0,1)>0 && size(x0,2) ~= dim)
    error('The number of columns in x0 is not equal to the problem dimension from probparam(1).')
end

LD = length(valuerange); % length of the valuerange
scale0=abs(valuerange(end)-valuerange(1))*ones(1,dim); % initial scaling vector in MLE.


% Read in and check the elements of solverparam, filling in default values where needed.
if (~isfield(solverparam,'numinitsols'))
    solverparam.numinitsols = size(x0,1);
end
if (~isfield(solverparam,'numfinalsols'))
    solverparam.numinitsols = length(budget);
end
if (solverparam.numinitsols ~= size(x0,1))
    warning('solverparam.numinitsols is not equal to the number of rows in x0.  Setting it equal, and continuing.');
    solverparam.numinitsols = size(x0,1);
end
if (solverparam.numfinalsols ~= length(budget))
    warning('solverparam.numfinalsols is not equal to the length of budget.  Setting it equal, and continuing.');
    solverparam.numinitsols = length(budget);
end
if (~isfield(solverparam,'other'))
    other = struct(); % empty struct
else
    other = solverparam.other;
end

% Check the elements of solverparam.other, and fill in default values if they
% are not specified.
if (~isfield(other,'n1'))
    other.n1 = max(10*dim,size(x0,1)); % default value
end
if (~isfield(other,'n2'))
    other.n2 = 2*dim; % default value
end
if (other.n1 < size(x0,1))
    warning('Increasing other.n1 because the number of initial points to sample from x0 is larger than other.n1.')
    other.n1 = size(x0,1);
end
if other.n1 + other.n2 >= budget(end)
    error('The final value in budget is smaller than the number of samples in the first stage.');
end
min_n1 = 2; % pf: is this a good minimum value for n1 and n2?
min_n2 = 2;
if other.n1 < min_n1
    warning(sprintf('other.n1 is set too low.  Setting it to its minimum value of %d',min_n1));
    other.n1 = min_n1;
end
if other.n2 < min_n2
    warning(sprintf('other.n2 is set too low.  Setting it to its minimum value of %d',min_n2));
    other.n2 = min_n2;
end
if other.n1 < other.n2
    warning(sprintf('We require n1 >= n2.  Increasing n1.'));
    other.n1 = other.n2;
end




if(~isfield(other,'rescale'))
    other.rescale = 1; % default value.
end

if(~isfield(other,'UpdateTimes'))
    N = other.n1 + other.n2; % N is the number of samples in the first stage.

    % By default, we update at times N*2.^[0.0,0.5,1.0,1.5,...], expressed in
    % terms of the overall number of samples, and rounded to make them
    % integers.  The update at time N happens automatically, and should not be
    % included in UpdateTimes, and UpdateTimes is expressed in terms of the
    % number of samples in the second stage, so except for rounding, is
    % N*2.^[0.5,1.0,1.5,...] - N.

    % t is an integer such that N*2^(t/2) >= budget(end)
    t = ceil(2*log2(budget(end)/N));
    other.UpdateTimes = ceil(N*2.^([1:t]/2)-N);
end

if (other.UpdateTimes(1)==1)
    % pf: fix this known bug.
    warning('AKG2 has a known bug, in which it crashes if solverparam.other.UpdateTimes starts with 1.  Incrementing all elements in UpdateTimes by 1 to avoid this.')
    other.UpdateTimes = other.UpdateTimes+1;
end

if(~isfield(other,'NumRandomStartS'))
    other.NumRandomStartS = 1; % default value
    % pf: is this the default we want?
end
if(~isfield(other,'NumRandomStartP'))
    other.NumRandomStartP = 1; % default value
    % pf: is this the default we want?
end

if(~isfield(other,'NumRandomScreenS'))
    other.NumRandomScreenS = other.NumRandomStartS; % by default, do not screen.
end
if(~isfield(other,'NumRandomScreenP'))
    other.NumRandomScreenP = other.NumRandomStartP; % by default, do not screen.
end
if(other.NumRandomScreenS < other.NumRandomStartS)
    warning('solverparam.other.NumRandomScreenS is less than solverparam.other.NumRandomStartS.  Increasing NumRandomScreenS to be equal.');
    other.NumRandomScreenS = other.NumRandomStartS;
end
if(other.NumRandomScreenP < other.NumRandomStartP)
    warning('solverparam.other.NumRandomScreenP is less than solverparam.other.NumRandomStartP.  Increasing NumRandomScreenP to be equal.');
    other.NumRandomScreenP = other.NumRandomStartP;
end

if(~isfield(other,'IncludeBestSingleton'))
    other.IncludeBestSingleton = 1; % by default, include the best singleton
end
if(~isfield(other,'IncludeSecondBestSingleton'))
    other.IncludeSecondBestSingleton = 1; % by default, include the second best singleton
end
if(~isfield(other,'IncludeBestSecondBestPair'))
    other.IncludeBestSecondBestPair = 1; % by default, include this best/second-best pair.
end

if (other.IncludeBestSingleton + other.IncludeSecondBestSingleton + other.NumRandomStartS == 0)
    % We require that we consider at least one singleton when deciding where to measure.
    % Obviously, we would have issues if we had 0 starting singletons and 0
    % starting pairs (because then we wouldn't be able to find anything to measure).
    % We additionally require that there is at least one starting singleton
    % because, when the budget only has one measurement left, we only look over
    % singletons.
    error('At least one of the following fields in solverparam.other needs to be strictly positive: IncludeBestSingleton, IncludeSecondBestSingleton, NumRandomStartS.');
end


% We treat these flags both like logicals, and integers, so we need them to
% be either 0 or 1.
if(other.IncludeBestSingleton ~= 0 && other.IncludeBestSingleton ~= 1)
    error('other.IncludeBestSingleton should either be 0 or 1.')
end
if(other.IncludeSecondBestSingleton ~= 0 && other.IncludeSecondBestSingleton ~= 1)
    error('other.IncludeSecondBestSingleton should either be 0 or 1.')
end
if(other.IncludeBestSecondBestPair ~= 0 && other.IncludeBestSecondBestPair ~= 1)
    error('other.IncludeBestSecondBestPair should either be 0 or 1.')
end

if(~isfield(other,'DoLocalSearch'))
    other.DoLocalSearch = 1; % by default, do local search
end

if(~isfield(other,'UseFirstStageSamples'))
    other.UseFirstStageSamples = 1; % by default, use first-stage samples.
end

if(~isfield(other,'MaxSeed'))
    other.MaxSeed = 1000; % default value.
end

if probseed<0 || probseed > other.MaxSeed
  error('probseed must be between 0 and other.MaxSeed.')
end

if solverseed<0 || solverseed > other.MaxSeed
  error('solverseed must be between 0 and other.MaxSeed.')
end


% Checks the struct "s" to see if it has any additional fields not in the cell
% array of strings, "fields". If so, a warning message is issued, containing a
% meaningful name of the structure being checked in "struct_name".
function check_struct(struct_name,s,fields)
  for i=1:length(fields)
    if (isfield(s,fields{i}))
        s = rmfield(s,fields{i});
    end
  end
  leftover = fieldnames(s);
  if (length(leftover)>0)
      leftover_str = leftover{1};
      for i=2:length(leftover)
          leftover_str = sprintf('%s, %s', leftover_str, leftover{i});
      end
      warning('The following field(s) in the struct %s are extra, and will not be used: %s', struct_name, leftover_str);
  end
end

% Check whether any unused fields have been set, either in probparam,
% solverparam, or solverparam.other.
check_struct('probparam', probparam, {'dim','runlength','other'});
check_struct('solverparam', solverparam, {'numinitsols','numfinalsols','other'});
check_struct('solverparam.other', other, {'n1','n2','rescale','UpdateTimes','NumRandomStartS','NumRandomStartP','NumRandomScreenS','NumRandomScreenP','IncludeBestSingleton','IncludeSecondBestSingleton','IncludeBestSecondBestPair','DoLocalSearch','UseFirstStageSamples','MaxSeed'});

% This is the stream that we will use for generating random numbers within the solver.
solverstream = RandStream.create('mrg32k3a','NumStreams',1);
solverstream.Substream = solverseed;
oldstream = RandStream.setGlobalStream(solverstream); % put this back when we exit.

% Each time we call the objective function, we pass a seed of 
% probseed + probseed_n * other.MaxSeed
% and then increment probseed_n.
% This way, the seeds used are all unique given a value of probseed, and also
% do not overlap with solverseed (since probseed_n starts at 1, not 0). 
probseed_n = 1;

% Check that the problem function handle works by trying it on a random pair of
% alterantives.
test_alts=valuerange(randi(LD,[2 dim])); % random pair of alternative from solverstream.
test_seed=1;
try
    y = problem(test_alts,probparam.runlength,test_seed,probparam.other);
catch mException
    RandStream.setGlobalStream(oldstream); 
    error(sprintf('The passed function handle "problem" does not have the right format.  The error "%s" arose when testing it with x=%s, runlength=%d, seed=%d.', mException.message,mat2str(test_alts),probparam.runlength,test_seed));
end


% These are the options that we will pass to our local solver, fmincon
% We do not set MaxFunEvals
options = optimset('GradObj','on','Algorithm','active-set','TolFun',1e-8,'Display','off');

% The following sub-function uses the passed function handle, "problem", which is
% in the simopt format, and makes it easier for us to use.
% The passed function handle, "problem", supports sampling of multiple
% alternatives using CRN: alternatives corresponding to each row of the input
% matrix are evaluated with the same seed; output should be a column vector
% with length equal to the number of alternatives.
% Inputs:
%       problem is the original simopt problem
%       probx is a matrix of values at which I want to evaluate problem
%       probparam is the passed probparam structure, containing runlength and other
%       probsd is the seed that I should use
function output = prob(problem,probx,probparam,probsd)
    % debug output
    % disp(sprintf('evaluating problem at x=%s runlength=%d seed=%d other=%s',mat2str(probx),probparam.runlungth,probsd,mat2str(probparam.other)));
    output = zeros(size(probx,1),1);
    for ii = 1:size(probx,1)
        output(ii) = feval(problem,probx(ii,:),probparam.runlength,probsd,probparam.other);
    end
end


%
% Do the initial stage of N samples using CRN, and estimate hyperparameters using these samples.
%
N = other.n1+other.n2; % N is the number of samples in the first stage.
assert(other.n1>=size(x0,1)); % This should be assured by the check we do when reading in parameters.
% Generate random alternatives.  The number generated is n1, minus the number in x0.
% Our goal is to produce a matrix with a number of rows equal to the number of
% alternatives, and a number of columns equal to the dimension.  If dim is 1,
% then this code actually produces a matrix with 1 row, rather than 1 column.
% This is matlab trying to be helpful, but we don't want this behavior.  We
% correct this below.
FirstStageAlts1=valuerange(randi(LD,[other.n1-size(x0,1) dim]));
if dim == 1
        FirstStageAlts1=FirstStageAlts1'; % Make the number of rows equal to the number of alternatives.
end
% Now FirstStageAlts should be a matrix with other.n1-size(x0,1) rows, and dim columns. 
assert(all(size(FirstStageAlts1)==[other.n1-size(x0,1) dim])); % Check that the size is correct
FirstStageAlts1 = [FirstStageAlts1;x0]; % include x0 in the initial stage
assert(all(size(FirstStageAlts1)==[other.n1 dim])); % Check that the size is correct again.

% We take all of these observations using the same seed.
FirstStageObs1=prob(problem,FirstStageAlts1,probparam,probseed + probseed_n * other.MaxSeed);
probseed_n = probseed_n + 1; % we used a substream from our problem.

% Take additional evaluations from the best n2 points
% We use the same seed for all of these observations.
[~,Best]=sort(FirstStageObs1,'descend');
FirstStageAlts2=FirstStageAlts1(Best(1:other.n2),:);
FirstStageObs2=prob(problem,FirstStageAlts2,probparam,probseed + probseed_n * other.MaxSeed);
probseed_n = probseed_n + 1; % we used a substream from our problem.

FirstStageAlts=[FirstStageAlts1;FirstStageAlts2];
FirstStageObs=[FirstStageObs1;FirstStageObs2];

% This is used to calculate the MLE estimate of hyperparameters.
% It records which alternatives were sampled as pairs, and which were sampled as singletons.
% It is a square matrix, whose size is the number of alternative sampled.
% It has a 1 on every off-diagonal element corresponding to observations that
% were taken using the same seed.  The diagonal elements are 0.
FL=zeros(N,N);
FL(1:other.n1,1:other.n1)=ones(other.n1)-eye(other.n1);
FL(other.n1+1:end,other.n1+1:end)=ones(other.n2)-eye(other.n2);

% solutions is the solution set.  In the initial stage, we don't have a solution
% according to our algorithm, since we haven't fit a GP to it, so we just
% choose random points.
solutions = valuerange(randi(LD,[N dim]));
times = (toc(start_time))*ones(N,1);
VOI = nan*ones(N,1);

% LI is a two-row matrix, which samples pairs of alternatives sampled together.
% Every time a pair gets sampled, say samples n and n-1, we add two columns to
% this matrix, one containing entries n and n-1; the other containing entries
% n-1 and n.
LI = []; 

% Fit the hyperparameters.  This call to MLEofHyperparameters, as well as one
% below, hard-codes initial estimates g0 and noisecorr0, which are used by this
% function as starting points to optimize the MLE.  
% pf: It would be better not to hard-code g0 and noisecorr0.
% Note that the MLE outputs some text to the screen, which occurs before we
% display the FirstStageAlts a few lines below, even though the FirstStageAlts
% are computed before.
[PriorCov,SampVarHat,PriorScale,SampCorrHat,mu0]= MLEofHyperparameters(FirstStageAlts,FirstStageObs,FL,.5,.5,scale0,other.rescale);

UpInd = 1; % UpdateTimes[UpInd] is the index in the second stage of samples when we will next update hyperparameters.

if (other.UseFirstStageSamples)
    n = 0; % Set the counter of samples in the second stage.
    SecondStageAlts = [];  % Set of alternatives sampled in the second stage.
    SecondStageObs=[]; % Set of observations in the second stage.
    sigma0=GPCov(PriorCov,PriorScale,FirstStageAlts,FirstStageAlts); % prior covariance matrix.
    L = SampVarHat*(eye(N)+FL*SampCorrHat); % sampling covariance matrix.
    disp(FirstStageAlts)
else
    % When UseFirstStageSamples is off, we take a single additional random
    % sample before entering the sequential sampling stage.  We do this because
    % our code in this second stage only uses information from the second stage
    % to update the GP (although information from all stages is used to update
    % the MLE of the hyperparameters).  
    SecondStageAlts = valuerange(randi(LD,[1 dim])); % take the first sample at random
    SecondStageObs=prob(problem,SecondStageAlts,probparam,probseed + probseed_n * other.MaxSeed);
    probseed_n = probseed_n + 1; % we used a substream from our problem.
    L=SampVarHat; % L is the lambda matrix from our paper.
    sigma0=PriorCov;
    n = 1; % n is the number of samples in the second stage, including the one we just took.
    Cov21 = PriorCov*GPCov(1,PriorScale,SecondStageAlts,FirstStageAlts);
    fprintf('sample 1 - %d:\n',N+n)
    disp([FirstStageAlts;SecondStageAlts])
end

% Initiialize a vector that will contain the value of information for the
% sampled decisions.  Fill it in with NaNs for the samples taken thus far,
% because we did not do a value of information calculation for these samples. 
VOI = nan * zeros(1,budget(end));

while N+n <= budget(end) % N+n is the total number of samples taken thus far.

    AllAlts=[FirstStageAlts;SecondStageAlts];

    % Occasionally update the MLEs.
    % This part of the code takes a significant amount of CPU time.
    if UpInd<=length(other.UpdateTimes)&&n>=other.UpdateTimes(UpInd)
        AllAlts=[FirstStageAlts;SecondStageAlts];
        AllObs =[FirstStageObs;SecondStageObs];
        % AL is a square binary matrix that captures the structure of all of
        % the samples we have taken, including the first stage of samples.
        % Off-diagonal entries are 1 if they correspond to pairs of
        % alternatives that have been sampled together.  They are 0 otherwise.
        % Diagonal elements are 0.  
        % 
	% In constructing this matrix, FL is NxN and represents the first stage
	% of samples.  There are n additional samples.  The off-diagonal
	% elements for pairs sampled together are added using information
	% stored in the matrix LI.
	if (size(LI,2) == 0) % i.e., if we have not sampled any pairs yet.
            SecondStageFL = sparse(n,n); % No pairs were sampled together.
	    AL=full([FL,zeros(N,n);zeros(n,N),SecondStageFL]);
	else
	    % The matrix LI contains two columns for every pair of alternatives
	    % sampled together.  For example, we would have LI=[3 4; 4 3] if
	    % the 3rd and 4th alternatives were sampled together, and they were
	    % the only pair sampled.  In this example, the code
	    % sparse(LI(1,:),LI(2,:),ones(1,size(LI,2)),n,n) would then create
	    % an nxn matrix where entries (3,4) and (4,3) were 1s, and all
	    % other entries were 0.   The indices of LI are in terms of the
	    % second stage of sampling.
            SecondStageFL = sparse(LI(1,:),LI(2,:),ones(1,size(LI,2)),n,n);
	    AL=full([FL,zeros(N,n);zeros(n,N),SecondStageFL]);
	end

        % Fit the hyperparameters using all observations (stage 1 and stage 2).
        [PriorCov,SampVarHat,PriorScale,SampCorrHat,mu0]= MLEofHyperparameters(AllAlts,AllObs,AL,.5,.5,scale0,other.rescale);
        UpInd=UpInd+1;

	% Create the covariance matrix for the prior, and the covariance matrix
	% of the sampling noise, across all sampled alternatives.  If
	% UseFirstStageSamples is on, then we use stage 1 and stage 2, which
	% is a total of N+n alternatives.  If it is off, we just use stage 2,
	% which is just n alternatives.  Also, if UseFirstStageSamples is off,
	% we construct the covariance between the alternatives sampled in the
	% first stage, and those in the second stage. 
        if (other.UseFirstStageSamples)
            % prior covariance matrix.
	    sigma0=GPCov(PriorCov,PriorScale,AllAlts,AllAlts);

            % sampling covariance matrix.
            L = SampVarHat*(eye(N+n)+AL*SampCorrHat);

            % We do not construct Cov21.
        else
	    % Construct the covariance matrix under the prior for all
	    % alternatives sampled in the second stage.  The block of code
	    % below is equivalent to doing the following line of code, 
	    %    sigma0=GPCov(PriorCov,PriorScale,SecondStageAlts,SecondStageAlts);
            % but may be faster.
            sigma0=eye(n);
            for i=2:n
                sigma0(i,1:i-1)=GPCov(1,PriorScale,SecondStageAlts(i,:),SecondStageAlts(1:i-1,:));
                sigma0(1:i-1,i)=sigma0(i,1:i-1)';
            end
            sigma0=sigma0*PriorCov; 

	    % Construct the covariance matrix between those alternatives
	    % sampled in the first and second stages.
            Cov21 = PriorCov*GPCov(1,PriorScale,SecondStageAlts,FirstStageAlts);

	    % Contruct the covariance of the sampling distribution for all
	    % samples in the second stage.
            L = SampVarHat*(eye(n)+AL(end-n+1:end,end-n+1:end)*SampCorrHat);
        end

	% AL takes a lot of memory, and we don't need it until the next update,
	% so we free that memory here.
        clear AL 
    end

    % Calculate the posteriors (Mu and AllMu), and associated quantities like
    % AltsForInference.
    if (other.UseFirstStageSamples)
        AltsForInference=[FirstStageAlts;SecondStageAlts];
        Yt=[FirstStageObs;SecondStageObs]-mu0;
	NumAltsForInference=length(Yt); % N+n
	invS=inv(sigma0+L);
        [Mu,sigma]=Update_Kalman(mu0,sigma0,length(Yt),invS,Yt);
        AllMu = Mu;
    else 
        AltsForInference=SecondStageAlts;
        Yt=SecondStageObs-mu0;
	NumAltsForInference=length(Yt); % n
        invS=inv(sigma0+L);
        [Mu,sigma]=Update_Kalman(mu0,sigma0,length(Yt),invS,Yt); % posterior means of just Stage 1 alternatives.
        AllMu = [mu0+Cov21'/(sigma0+L)*Yt;Mu]; % posterior means of sampled alternatives (Stage 1+2)
    end 

    % Find an element that maximizes the posterior mean, among all sampled
    % alternatives, and display it as the current solution.
    x01=ChooseRandomElement(find(AllMu==max(AllMu)));
    CurrentBest=AllAlts(x01,:); % current solution, as a vector
    fprintf('Current Best Solution:\n')
    disp(CurrentBest)
    solutions = [solutions;CurrentBest];
    times = [times;toc(start_time)];

    % Bests is a binary vector, that is equal to 1 at positions whose evaluated
    % alternative is the current best, and 0 elsewhere.
    % AllAltsExceptBest is the set of all indices of evaluated alternatives,
    % with the current best removed, including the first stage.
    % SecondBest is the alternative, in vector form, with the second-best
    % posterior mean.
    Bests= sum(abs(AllAlts-repmat(CurrentBest,N+n,1)),2)==0;
    AllAltsExceptBest=1:N+n;AllAltsExceptBest(Bests)=[];
    if (length(AllAltsExceptBest)>0)
        ValueOfSecondBest = max(AllMu(AllAltsExceptBest));
        x01=ChooseRandomElement(find(AllMu==ValueOfSecondBest));
        SecondBest=AllAlts(x01,:); % current solution, as a vector
    else
        SecondBest=[];
    end

    if N+n>=budget(end)
	break; % Stop if we have exceeded our final budget.
    end

    if (sum(isnan(Mu))>0)
        % Occasionally Update_Kalman returns Mu that has one or more NaN elements.
        % In this case we choose a random point to evaluate next.
        % pf: The fact that occasionally Update_Kalman returns NaN means that
        % we should look at the numerical precision of this code.
        warning('One or more elements of Mu is NaN.  Choosing a random point to evaluate next and moving on.')
        Cofx=valuerange(randi(LD,[1 dim]));Cofx=(Cofx(:))';
    end

    % pf: from this line down to where the sampling decision is made, could
    % be put into a function that computes the sampling decision.  Or, it
    % could be broken into one function that computes the best singleton,
    % and another that computes the best pair.

    % Choose those singletons at which to do screening, before starting VOI
    % gradient ascent.
    screeningS = [];
    NumScreenS = other.IncludeBestSingleton + other.NumRandomScreenS; % IncludeSecondBestSingleton is accounted for below.
    NumStartS = other.IncludeBestSingleton + other.NumRandomStartS; % IncludeSecondBestSingleton is accounted for below.
    if (other.IncludeBestSingleton)
        screeningS = [screeningS;CurrentBest];
    end
    if (other.IncludeSecondBestSingleton && ~isempty(SecondBest))
        screeningS = [screeningS;SecondBest];
        NumScreenS = NumScreenS + 1;
        NumStartS = NumStartS + 1;
    end
    if (other.NumRandomScreenS>0)
        % add NumRandomScreenS points to the set to screen.
        screeningS = [screeningS;valuerange(randi(LD,[other.NumRandomScreenS dim]))];
    end
    % The number of points in screeningS is now NumScreenS.


    % Do screening: calculate the VOI for each of the points in screeningS,
    % takes the top NumStartS, and puts them in CofX0.  These are the ones
    % with which we want to do gradient ascent.
    % pf: if NumScreenS is equal to NumStartS, I think we can skip this next block of code.
    voiScreeningS = zeros(1,NumScreenS); % the VOI of the points in screeningS
    for start=1:NumScreenS
        % There are two functions
        %   Algorithm/VOI_TestSingularConly
        %   Miscellaneous/Hessian/VOI_TestSingular_Conly,
        % Make sure to use the one in Algorithm, since the one in Hessian
        % returns the Hessian, and we don't need that.
        % pf: I should change the name of the one in Miscellaneous.
        voiScreeningS(start)=VOI_TestSingular_Conly(screeningS(start,:),length(Mu),Mu,sigma,mu0,sigma0,PriorCov,PriorScale,SampVarHat,invS,AltsForInference,Yt);
    end
    [~,order]=sort(voiScreeningS,'ascend');
    CofX0 = screeningS(order(1:NumStartS),:);

    % Do gradient ascent on all singletons in CofX0.
    % pf: we could do the following step in parallel to save time.
    CofX=zeros(NumStartS,dim);
    Fval=zeros(NumStartS,1);
    for start=1:NumStartS
        if (other.DoLocalSearch)
            % Do a local search starting at CofX0, and store the results in CofX.
            CofX(start,:) = fmincon(@(cc)VOI_TestSingular_Conly(cc,length(Mu),Mu,sigma,mu0,sigma0,PriorCov,PriorScale,SampVarHat,invS,AltsForInference,Yt),CofX0(start,:),[],[],[],[],valuerange(ones(1,dim)),valuerange(end)*ones(1,dim),[],options);%,'Display','Iter'));
        else
            % Do not do a local search.
            CofX(start,:)=CofX0(start,:);
        end
        if (sum(isnan(CofX(start,:)))>0)
            warning('The local search produced NaNs.  Skipping the local search.');
            CofX(start,:)=CofX0(start,:);
        else
            CofXD=max(min(valuerange(end),CofX(start,:)),valuerange(1));
            [~,Index]=min(abs(bsxfun(@minus,CofXD,valuerange(:))));
            CofXD=valuerange(Index);
            CofX(start,:)=(CofXD(:))';
        end
        Fval(start) = VOI_TestSingular_Conly(CofX(start,:),length(Mu),Mu,sigma,mu0,sigma0,PriorCov,PriorScale,SampVarHat,invS,AltsForInference,Yt);
    end

    % After having done gradient ascent from all singletons in CofX0,
    % and storing the results of these local searches in CofX, choose the
    % one with the highest VOI, store it in Cofx, and put VOI in SF.
    [SF,IS]=min(Fval);SF=-SF;
    Cofx=CofX(IS,:);
    % pf: it is dangeround that Cofx and CofX have different
    % capitalizations.  Either use the same capitalization, or use entirely
    % different variable names.

    % Store the maximal value of information (to be returned to the user).
    % Below this will be overwritten if we choose to sample a pair.
    VOI(N+n) = SF;

    % This checks whether there are at least two measurements remaining.
    % If there are, we look for pairs to measure.
    % pf: do we really want to not look at pairs if there is only one point
    % left?  It seems like it makes things more complicated than necessary.
    if N+n+2<=budget(end)

	% Choose those pairs at which to do screening, before starting VOI
	% gradient ascent.
        screeningP = [];
	NumScreenP = other.NumRandomScreenP; % other.IncludeBestSecondBestPair is accounted for below.
	NumStartP  = other.NumRandomStartP; % other.IncludeBestSecondBestPair is accounted for below.
        if (other.IncludeBestSecondBestPair && ~isempty(SecondBest))
            screeningP = [CurrentBest,SecondBest];
            NumScreenP = NumScreenP + 1;
            NumStartP  = NumStartP + 1;
        end
        if (other.NumRandomScreenP>0)
            % add NumRandomScreenS points to the set to screen.
            % NumScreenP and NumStartP is already incremented to account for this above.
            screeningP = [screeningP;valuerange(randi(LD,[other.NumRandomScreenP 2*dim]))];
        end
        % The number of points in screeningP should now be NumScreenP.
        assert(size(screeningP,1)==NumScreenP)

        % Calculate the pairwise-VOI for these NumScreenP pairs, take the top
        % NumStartP, and put them in CofX0.  We will do gradient ascent
        % with these.
        % Pay attention to matrix size when NumStartP is 1 or 2.
        voiScreeningP = zeros(1,NumScreenP);
        for start=1:NumScreenP
            % pf: make sure to pick up the one in Algorithm, rather than the one in Miscellaneous.
            voiScreeningP(start)=VOI_TestPair_Conly_HomoCorr(screeningP(start,:),dim,length(Mu),Mu,sigma,mu0,sigma0,PriorCov,PriorScale,SampVarHat,SampCorrHat,invS,AltsForInference,Yt);
        end
        [~,order]=sort(voiScreeningP,'ascend');
        CofX0 = screeningP(order(1:NumStartP),:);

        if NumStartP == 1
            CofX0 = (CofX0(:))';
        end

        % Do gradient ascent on all pairs in CofX0.
        % pf: we could do the following step in parallel to save time.
        CofX=zeros(NumStartP,2*dim);
        Fval=zeros(NumStartP,1);
        for start=1:NumStartP
            if (other.DoLocalSearch)
                % Do local search starting at CofX0(start,:) , and put the results in CofX(start,:).
                CofX(start,:) = fmincon(@(cc)VOI_TestPair_Conly_HomoCorr(cc,dim,length(Mu),Mu,sigma,mu0,sigma0,PriorCov,PriorScale,SampVarHat,SampCorrHat,invS,AltsForInference,Yt),CofX0(start,:),[],[],[],[],valuerange(ones(1,2*dim)),valuerange(end)*ones(1,2*dim),[],options);
            else
                % Do not do a local search.
                CofX(start,:) = CofX0(start,:);
            end
            if (sum(isnan(CofX(start,:)))>0)
                warning('There are NaNs in CofX in gradient ascent for pairs.')
                CofX(start,:)=CofX0(start,:);
            else
                CofXD=max(min(valuerange(end),CofX(start,:)),valuerange(1));
                [~,Index]=min(abs(bsxfun(@minus,CofXD,valuerange(:))));
                CofXD=valuerange(Index);
                CofX(start,:)=(CofXD(:))';
            end
            Fval(start) = VOI_TestPair_Conly_HomoCorr(CofX(start,:),dim,length(Mu),Mu,sigma,mu0,sigma0,PriorCov,PriorScale,SampVarHat,SampCorrHat,invS,AltsForInference,Yt);
        end

        % Compare the VOI between the highest obtained from a singleton, and
        % from a pair, and if the pair is better, use that instead.
        [F,IS]=min(Fval);F=-F;
        if F > SF
            VOI(N+n) = F; % Overwrite our stored value of the maximal value of information.
            VOI(N+n+1) = F; % Store it for both entries.
            Cofx=[CofX(IS,1:dim);CofX(IS,dim+1:end)];
        end
    end % End code that decides whether/which pairs to sample.

    % Our sampling decision is in Cofx.
    % Sample from a new singleton or a new pair of alternatives.

    % Sample a singleton.
    % The first condition in the if statement checks if we really are sampling
    % a singleton.  The second condition checks whether we are sampling the
    % same alternative twice, which we treat as a singleton.  We do this
    % because sampling the same alternative twice using the same common random
    % number seed gives exactly the same answer, and doesn't provide any info.

    % pf: This way in which we handle sampling the same alternative twice is
    % something we should mention in the paper.  Is there some assumption we
    % are making in our math that makes it possible for us to ask for this?  Is
    % it even possible for us to ask for this?
    if size(Cofx,1)==1 || (sum(Cofx(1,:)==Cofx(2,:))==dim)
        Cofx=Cofx(1,:);

        y = prob(problem,Cofx,probparam,probseed + probseed_n * other.MaxSeed);
        probseed_n = probseed_n + 1; % we used a substream from our problem.

        n = n+1;
        fprintf('sample %d:\n',N+n)
        disp(Cofx)

        % If we have already computed it, we can re-use the corresponding
	% portion of the covariance matrix.  This saves time.  
        AlreadySampled=find(sum(abs(AltsForInference-repmat(Cofx,NumAltsForInference,1)),2)==0);
        if (~isempty(AlreadySampled))
            sigma0=[[sigma0,sigma0(:,AlreadySampled(1))];[sigma0(AlreadySampled(1),:),PriorCov]];
            if (~other.UseFirstStageSamples)
              Cov21 = [Cov21;Cov21(AlreadySampled(1),:)];
            end
        else
            RU=GPCov(PriorCov,PriorScale,AltsForInference,Cofx);
            sigma0=[[sigma0,RU];[RU',PriorCov]];
            if (~other.UseFirstStageSamples)
              Cov21 = [Cov21;GPCov(PriorCov,PriorScale,Cofx,FirstStageAlts)];
            end
        end

        SecondStageAlts=[SecondStageAlts;Cofx];
        SecondStageObs=[SecondStageObs;y];

        L=[[L,zeros(NumAltsForInference,1)];[zeros(1,NumAltsForInference),SampVarHat]];
        L=sparse(L);

    % Sample a pair
    else
        n = n+2;
        solutions = [solutions;solutions(end,:)];
        times = [times;times(end)];
        fprintf('sample %d - %d:\n',N+n-1,N+n)
        disp(Cofx)
        Y = prob(problem,Cofx,probparam,probseed + probseed_n * other.MaxSeed);
        probseed_n = probseed_n + 1; % we used a substream from our problem.

        SecondStageObs=[SecondStageObs;Y];

        % For each alternative in the pair, figure out if we have already sampled it. 
        % If we have, store its index.
        AlreadySampled=zeros(1,2);
        for j=1:2
            temp=find(sum(abs(AltsForInference-repmat(Cofx(j,:),NumAltsForInference,1)),2)==0);
            if (~isempty(temp))
                AlreadySampled(j)=temp(1);
            end
        end

        % Update sigma0 and Cov21.

        temp=GPCov(PriorCov,PriorScale,Cofx(1,:),Cofx(2,:));
        RD=[PriorCov,temp;temp,PriorCov];
        RD3=SampVarHat*[1,SampCorrHat;SampCorrHat,1];

        if (AlreadySampled(1)~=0)
            if (AlreadySampled(2)~=0)
                sigma0=[[sigma0,sigma0(:,[AlreadySampled(1),AlreadySampled(2)])];[sigma0([AlreadySampled(1),AlreadySampled(2)],:),RD]];
                if (~other.UseFirstStageSamples)
                  Cov21 = [Cov21;Cov21(AlreadySampled(1),:);Cov21(AlreadySampled(2),:)];
                end
            else
                RU=GPCov(PriorCov,PriorScale,AltsForInference,Cofx(2,:));
                TEMP=[sigma0(:,AlreadySampled(1)),RU];
                sigma0=[[sigma0,TEMP];[TEMP',RD]];%'
                if (~other.UseFirstStageSamples)
                  Cov21 = [Cov21;Cov21(AlreadySampled(1),:);GPCov(PriorCov,PriorScale,Cofx(2,:),FirstStageAlts)];
                end
            end
        else 
            if (AlreadySampled(2)~=0)
                RU=GPCov(PriorCov,PriorScale,AltsForInference,Cofx(1,:));
                TEMP=[RU,sigma0(:,AlreadySampled(2))];
                sigma0=[[sigma0,TEMP];[TEMP',RD]];
                if (~other.UseFirstStageSamples)
                  Cov21 = [Cov21;GPCov(PriorCov,PriorScale,Cofx(1,:),FirstStageAlts);Cov21(AlreadySampled(2),:)];
                end
            else
                RU=GPCov(PriorCov,PriorScale,AltsForInference,Cofx);
                sigma0=[[sigma0,RU];[RU',RD]];
                if (~other.UseFirstStageSamples)
                  Cov21 = [Cov21;GPCov(PriorCov,PriorScale,Cofx,FirstStageAlts)];
                end
            end
        end

        SecondStageAlts=[SecondStageAlts;Cofx];
        L=[[L,zeros(NumAltsForInference,2)];[zeros(2,NumAltsForInference),RD3]];
        L=sparse(L);

	% Add two entries to LI.  These are indexed in temrs of second stage
	% samples, not first stage ones.
        LI=[LI,[n-1,n;n,n-1]]; 
    end
end

% Report the final solutions, times, and VOI. size(solutions,1)=length(budget).
solutions = solutions(budget,:);
times = times(budget,:);
debug = struct('VOI',VOI);

% Put back the old random number stream
RandStream.setGlobalStream(oldstream); 
end
