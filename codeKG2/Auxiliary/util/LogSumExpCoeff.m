% Computes log(sum(c*exp(x))) for a vector x and positive coefficients c, but in a numerically careful way.
function y=LogSumExpCoeff(x,c)
xmax = max(x);
y = xmax + log(sum(c.*exp(x-xmax)));