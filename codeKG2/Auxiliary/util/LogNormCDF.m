% Returns the log of the normal cdf evaluated at -abs(s).  s can be a vector or a scalar.
function logy = LogNormCDF(s)
logy=s;
% Use Mill's Ratio.
i=find(abs(s)>10);
if (~isempty(i))
    logy(i) = LogNormPDF(s(i)) - log(s(i).^2 + 1)+log(abs(s(i)));
end

% Use straightforward routines for s in the more numerically stable region.
i=find(abs(s)<=10);
if (~isempty(i))
    logy(i) = log(normcdf(-abs(s(i))));
end
