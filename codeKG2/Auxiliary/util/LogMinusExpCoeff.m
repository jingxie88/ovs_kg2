% y=LogMinusExp(a,b)
% Computes y=log(ca*exp(a)-cb*exp(b)) in a numerically stable manner.  Requires that ca>0,cb>0.
function [y,sgny]=LogMinusExpCoeff(a,ca,b,cb)
%assert(a>=b);
if (a==b)
    sgny = sign(ca-cb);
    if ca == cb
        y = -Inf;
    else
        y = log(abs(ca-cb))+a;
    end
else if a>b
        tmp = ca-cb*exp(b-a);
        sgny = sign(tmp);
        if tmp == 0 
            y = -inf;
        else
            y = a+log(abs(tmp));%log1p(-exp(b-a));
        end
    else
        tmp = ca*exp(a-b)-cb;
        sgny = sign(tmp);
        if tmp == 0 
            y = -inf;
        else
            y = b+log(abs(tmp));%log1p(-exp(b-a));
        end
    end
end