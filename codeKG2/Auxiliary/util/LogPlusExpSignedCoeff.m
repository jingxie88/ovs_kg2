% Let Y= sgna*exp(a)+sgnb*exp(b), where sgn[ab] are the coefficients.  Then
% y=log(abs(Y)) and sgny=sign(Y).  This is computed in a numerically stable
% way.
function [y,sgny]=LogPlusExpSignedCoeff(a,sgna,b,sgnb)
if (sgna==0)
    if sgnb == 0
        y = -inf;
    else
        y = log(abs(sgnb))+b;
    end
    sgny = sign(sgnb);
elseif (sgnb==0)
    y = log(abs(sgna))+a;
    sgny = sign(sgna);
elseif (sign(sgna) == sign(sgnb))
    sgny = sign(sgna);
    y = LogSumExpCoeff([a b],abs([sgna sgnb]));
elseif (a==b) % and signs are opposite
    if sgna+sgnb == 0
        y = -inf;
    else
        y = log(abs(sgna+sgnb))+a;
    end
    sgny = sign(sgna+sgnb);
else
    [y,sgny] = LogMinusExpCoeff(a,abs(sgna),b,abs(sgnb));
    sgny = sgny*sign(sgna);
end