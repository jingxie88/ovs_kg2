% Let Y= sum_i ca_i*exp(a_i).  Then
% y=log(abs(Y)) and sgny=sign(Y).  This is computed in a numerically stable
% way.
function [y,sgny]=LogSumExpSignedCoeff(a,ca)
assert(length(a) == length(ca));
y = a(1)+log(abs(ca(1)));
sgny = sign(ca(1));
for i=2:length(a)
	[y,sgny] = LogPlusExpSignedCoeff(y,sgny,a(i),ca(i));
end