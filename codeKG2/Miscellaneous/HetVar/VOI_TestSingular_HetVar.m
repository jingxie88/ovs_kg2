function [V,g]=VOI_TestSingular_HetVar(Cofx,n,Mu,Sigma,mu0,Sigma0,cov0hat,scalehat,Noisevarhat,invS,CofAltsSampled,Yt,kriging,batch)
Dim=length(Cofx);        
V=-inf;
        AlreadySampled=find(sum(abs(CofAltsSampled-ones(n,1)*Cofx),2)==0);
        TMP=1:n;
        TMP(AlreadySampled)=[];
        if (~isempty(TMP))
            m= Mu(TMP)==max(Mu(TMP)); 
            mi=ChooseRandomElement(TMP(m));            
        if (~isempty(AlreadySampled))
           A=[AlreadySampled(1),mi];
           istar=AlreadySampled(1);
           [V,st]=VOI(istar,A,1,Mu,Sigma,Noisevarhat);
           if nargout>1
               if isnan(st)
                   g=zeros(Dim,1);
               else
                   g = -SingularGradient_VOI(1,Dim,n,CofAltsSampled,istar,Sigma0,Sigma,invS,Yt,Noisevarhat(istar,istar),...
					Cofx,scalehat.^(-2),1,A,Mu,st,1,V);
               end
           end
        else         
        %Cofx=Coordinates(x,Dim);
        if nargin>12 && kriging == 1
            if nargin<14
                batch = 1;
            end
            [MuP,SigmaP,Sigmat0,AK]=Update_KalmanPlus_Conly_Abr(Mu,Sigma,mu0,Sigma0,cov0hat,scalehat,n,invS,CofAltsSampled,Yt,Cofx);
            if n<2*batch            
                sampvar = exp(AK*log(diag(Noisevarhat)));
            else
            [~, ~, ~, dmodel, ~] = EstimateHyperparametersNoNoise(CofAltsSampled(1:batch:end),log(diag(Noisevarhat(1:batch:end,1:batch:end))), 1, 1e-1000, 1e100);
            %[dmodel,pv]=mydacefit(Inputs(I,:), Outputs0(I),@regpoly0,@corrgauss,theta,thetaL,thetaU);
            tmp=(Cofx-mean(CofAltsSampled))./std(CofAltsSampled(1:batch:end));
            sampvar = exp(predictor(tmp, dmodel)*std(log(diag(Noisevarhat(1:batch:end,1:batch:end))))+mean(log(diag(Noisevarhat))));
            end
        else
            [MuP,SigmaP,Sigmat0]=Update_KalmanPlus_Conly_Abr(Mu,Sigma,mu0,Sigma0,cov0hat,scalehat,n,invS,CofAltsSampled,Yt,Cofx);
            %m=MuP(end);
            fan = zeros(1,n);
            for j = 1:n
                fan(j) = norm(Cofx-CofAltsSampled(j,:));
            end
            [~,fana] = min(fan);
            sampvar = Noisevarhat(fana,fana);
        end
        A=[n+1,mi];
        [V,st]=VOI(n+1,A,1,MuP,SigmaP,diag([diag(Noisevarhat);sampvar]));
        if nargout>1
            if isnan(st)
                g=zeros(Dim,1);
            else
                g = -SingularGradient_VOI(1,Dim,n,CofAltsSampled,n+1,Sigmat0,SigmaP,invS,Yt,sampvar,...
						 Cofx,scalehat.^(-2),1,A,MuP,st,1,V);
            end
        end
        end
        else 
            g=zeros(Dim,1);
        end
        V=-V; % we return -log(VOI), since we optimize using 'fmincon'.
%disp(V)
%disp(g)
end