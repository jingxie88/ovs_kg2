function [OC,F,IND] = CompIA_EstHetVar_Batch(rep,NumStarts)
load('CompIA_HetVar.mat')
beta=1;%N=100;
OC = zeros(1,N);
F = zeros(1,N);
%SF = zeros(1,N);
mu0 = 0;
Dim = 1;
%for rep = 1:REP
theta=(Theta(rep,:))';
Domain = Domain(:);
options = optimset('GradObj','on','Algorithm','active-set','TolFun',1e-8);%,'Algorithm','active-set','MaxFunEvals',1000);
scale0=.5*ones(1,Dim);%abs(Domain(end)-Domain(1))*ones(1,Dim);

batch = 5;
IND = randi(LD,[1 Dim])*ones(batch,1);
%HetVar = unifrnd(0,200,[1,length(theta)]);

IND = randperm(LD);IND=IND(1:4);IND=IND(:);%randi(LD,[10 Dim]);
SecondStageAlts=Domain(IND);
SecondStageObs=mvnrnd(theta(IND),diag(HetVar(IND)));
SecondStageObs = SecondStageObs(:);
[~,fan] = max(SecondStageObs);
IND = [IND;IND(fan)];
SecondStageAlts=[SecondStageAlts;SecondStageAlts(fan,:)];
SecondStageObs=[SecondStageObs;normrnd(theta(IND(fan)),sqrt(HetVar(IND(fan))))];
sigma0=PriorCov*GPCov(1,PriorScale,SecondStageAlts,SecondStageAlts);

for n = batch:batch:N 
    if sum(n==[batch:batch:200,210:10:300,320:20:400,420:40:500])>0
        [~,SampVarHat]= MLEofHyperparameters_IndepSamp(SecondStageAlts,SecondStageObs,.5,scale0,scale0);
    end
    L = eye(n)*SampVarHat;
    L=sparse(L);

    Yt=SecondStageObs-mu0;
    invS=inv(sigma0+L);

    [Mu,sigma]=Update_Kalman(mu0,sigma0,n,invS,Yt);

    x01=ChooseRandomElement(find(Mu==max(Mu)));
    CurrentBest=SecondStageAlts(x01,:);
    
    Bests= sum(abs(SecondStageAlts-repmat(CurrentBest,n,1)),2)==0;
    temp=1:n;temp(Bests)=[];
    
    OC(n:min(N,n+batch-1))=ones(1,min(N,n+batch-1)-n+1)*(max(theta)-theta(IND(x01)));
    
    if (sum(isnan(Mu))>0)
        Cofx=Domain(randi(LD,[1 Dim]));
    else
        if ~isempty(temp)
            if NumStarts>2
                x02=temp(ChooseRandomElement(find(Mu(temp)==max(Mu(temp)))));
                SecondBest=SecondStageAlts(x02,:);
                if sum(sum(abs(SecondStageAlts(temp,:)-repmat(SecondBest,length(temp),1)),2)==0)<length(temp)
                    CofX0=[CurrentBest;SecondBest;Domain(randi(LD,[NumStarts-2 Dim]))];
                else
                    CofX0=[CurrentBest;Domain(randi(LD,[NumStarts-1 Dim]))];
                end
                
            else if NumStarts == 2
                    CofX0=[CurrentBest;Domain(randi(LD,[NumStarts-1 Dim]))];
                else
                    CofX0=Domain(randi(LD,[NumStarts Dim]));
                end
            end
        else
            CofX0=Domain(randi(LD,[NumStarts Dim]));
        end
        
        % pay attention to matrix size when NumStarts=1or2.
    
        CofX=zeros(NumStarts,Dim);
        Fval=zeros(NumStarts,1);
    
        for start=1:NumStarts
            CofX(start,:) = fmincon(@(cc)VOI_TestSingular_Conly(cc,n,Mu,sigma,mu0,sigma0,PriorCov,PriorScale,SampVarHat,invS,SecondStageAlts,Yt),CofX0(start,:),[],[],[],[],Domain(ones(1,Dim)),Domain(end)*ones(1,Dim),[],options);%,'Display','Iter'));
            if (sum(isnan(CofX(start,:)))>0)
                CofX(start,:)=CofX0(start,:);
            else
                CofXD=max(min(Domain(end),CofX(start,:)),Domain(1));
                [~,Index]=min(abs(bsxfun(@minus,CofXD,Domain)));
                CofXD=Domain(Index);
                CofX(start,:)=(CofXD(:))';
            end
            Fval(start) = VOI_TestSingular_Conly(CofX(start,:),n,Mu,sigma,mu0,sigma0,PriorCov,PriorScale,SampVarHat,invS,SecondStageAlts,Yt);
        end
        [F(n),IS]=min(Fval);F(n)=-F(n);
        Cofx=CofX(IS,:);
    end
        disp(n)
        ind = (round((Cofx-Domain(1))/.1)+1)*ones(batch,1);
        IND = [IND;ind];
        AlreadySampled=find(sum(abs(SecondStageAlts-repmat(Cofx,n,1)),2)==0);

        % SF(n) = VOIStar_Singular(Cofx,n,Mu,sigma,mu0,sigma0,PriorCov,PriorScale,SampVar,invS,SecondStageAlts,Yt,1000,SearchLength);
        y=mvnrnd(theta(ind),diag(HetVar(ind)));y=y(:);
        
        if (~isempty(AlreadySampled))
            sigma0=[[sigma0,repmat(sigma0(:,AlreadySampled(1)),1,batch)];[repmat(sigma0(AlreadySampled(1),:),batch,1),PriorCov*ones(batch)]];
        else
            RU=GPCov(PriorCov,PriorScale,SecondStageAlts,Cofx);
            sigma0=[[sigma0,repmat(RU,1,batch)];[repmat(RU',batch,1),PriorCov*ones(batch)]];
        end
        
        SecondStageAlts=[SecondStageAlts;Cofx*ones(batch,1)];
        SecondStageObs=[SecondStageObs;y];
end
