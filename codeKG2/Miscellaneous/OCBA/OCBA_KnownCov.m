function OC=OCBA_KnownCov(n,T,Sigma,Mu,c,c_0,num)
% 'n' is times of run for parameter estimation
% 'T' is the budget constraint
% 'Sigma' is the matrix of covariance
% 'Mu' is the espectation
% 'c' is the vector of costs for each design
% 'num' is the times of run for estimating PCS
% 'Test==1' means optimization using real parameters,otherwise,using estimated parameters
tic
covar = Sigma;
OC = zeros(1,num);

if size(Mu,2)>1
    Mu=Mu';
end
if size(c,2)>1
    c=c';
end
k=length(Mu);
%Sum_scba=0;
for i=1:num
    Repl_num=n;
    run=n;
    Init_samp=randvar_gen(Sigma,Mu,run,k);
    [Mean,~]=para_estim(Init_samp,Repl_num);
 
    [N,b]=main_prog(T,covar,Mean,ones(k,1),0,1);
    N=N./c;
    RT=T+c_0*max(N);
    N=(T/RT)*N;
    Repl_num=Roundoff(N,T,n,c_0,c,b);
    run=max(Repl_num);
    Y=randvar_gen(Sigma,Mu,run,k);
    M_C=Mecomp(Y,Repl_num,Mean,k,n);
    OC(i) = max(Mu)-Mu(ChooseRandomElement(find(M_C==max(M_C))));
end
%PCS_scba=Sum_scba/num;