function [PCS_rcba,PCS_cba,PCS_iba,PCS_eba,PCS_scba,PCS_siba]=PCS_results(n,T,Sigma,Mu,c,c_0,num,delta)
% 'n' is times of run for parameter estimation
% 'T' is the budget constraint
% 'Sigma' is the matrix of covariance
% 'Mu' is the espectation
% 'c' is the vector of costs for each design
% 'num' is the times of run for estimating PCS
% 'Test==1' means optimization using real parameters,otherwise,using estimated parameters
tic
if size(Mu,2)>1
    Mu=Mu';
end
if size(c,2)>1
    c=c';
end
k=length(Mu);
Sum_rcba=0;
Sum_cba=0;
Sum_iba=0;
Sum_eba=0;
Sum_scba=0;
Sum_siba=0;
for i=1:num
 Repl_num=n;
 run=n;
Init_samp=randvar_gen(Sigma,Mu,run,k);
[Mean,covar]=para_estim(Init_samp,Repl_num);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[N,b]=main_prog(T,covar,Mean,c,c_0,1);
Repl_num=Roundoff(N,T,n,c_0,c,b);
run=max(Repl_num);
Y=randvar_gen(Sigma,Mu,run,k);
M_C=Mecomp(Y,Repl_num,Mean,k,n);
 if find(M_C==max(M_C))==find(Mu==max(Mu))
     Sum_rcba=Sum_rcba+1;
 end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[N,b]=main_prog(T,covar,Mean,ones(k,1),0,1);
N=N./c;
RT=T+c_0*max(N);
N=(T/RT)*N;
Repl_num=Roundoff(N,T,n,c_0,c,b);
run=max(Repl_num);
Y=randvar_gen(Sigma,Mu,run,k);
M_C=Mecomp(Y,Repl_num,Mean,k,n);
 if find(M_C==max(M_C))==find(Mu==max(Mu))
     Sum_cba=Sum_cba+1;
 end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[N,b]=main_prog(T,covar,Mean,ones(k,1),0,0);
N=N./c;
RT=T+c_0*max(N);
N=(T/RT)*N;
Repl_num=Roundoff(N,T,n,c_0,c,b);
run=max(Repl_num);
Y=randvar_gen(Sigma,Mu,run,k);
M_C=Mecomp(Y,Repl_num,Mean,k,n);
 if find(M_C==max(M_C))==find(Mu==max(Mu))
     Sum_iba=Sum_iba+1;
 end
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
N=((T/k)*ones(k,1))./c;
RT=T+c_0*max(N);
Repl_num=fix((T/RT)*N);
run=max(Repl_num);
Y=randvar_gen(Sigma,Mu,run,k);
M_C=Mecomp(Y,Repl_num,0,k,0);
 if find(M_C==max(M_C))==find(Mu==max(Mu))
     Sum_eba=Sum_eba+1;
 end
 if delta>0
%SeqCBA performance
N_seq=n*ones(k,1);
TC=n*(sum(c)+c_0);
Samples=zeros(k,T);
Tseq=T-TC;
Samples(:,1:n)=Init_samp;
loop=1;
while 1>0
if loop>1
[Mean,covar]=para_estim(Samples,N_seq);
end
loop=loop+1;
TC=TC+delta;
[N,b]=main_prog(TC,covar,Mean,c,c_0,1);
N_old=N_seq;
N_seq=max(N_seq,N);
N_seq=fix(N_seq);
N_add=N_seq-N_old;
Tseq=Tseq-(sum(c.*N_add)+c_0*max(N_add));
if Tseq<=0
TR=Tseq+(sum(c.*N_add)+c_0*max(N_add));
lamda=TR/(sum(c.*N_add)+c_0*max(N_add));
N_add=lamda*N_add;
N_add=fix(N_add);
N_seq=N_old+N_add;
Res=T-(sum(c.*N_seq)+c_0*max(N_seq));
if Res/c(b)>=1
    [~,B]=sort(N_seq,'descend');
    if b==B(1)
    Ra=fix(Res/(c_0+c(b)));
    N_seq(b)=N_seq(b)+Ra;
     N_add(b)=N_add(b)+Ra;
    Res1=Res-Ra*(c_0+c(b));
    if  Res1/c(B(2))>=1
    Ra1=fix(Res1/c(B(2)));
    Ra2=min(Ra1,N_seq(b)-N_seq(B(2)));
   N_seq(B(2))=N_seq(B(2))+Ra2;  
   N_add(B(2))=N_add(B(2))+Ra2;  
    Res2=Res1-Ra2*c(B(2));
    if  Res1/c(B(3))>=1
    Ra3=fix(Res2/c(B(3)));
    Ra4=min(Ra3,N_seq(b)-N_seq(B(3)));
    N_seq(B(3))=N_seq(B(3))+Ra4; 
    N_add(B(3))=N_add(B(3))+Ra4; 
    end
    end
    else
    Ra=fix(Res/(c(b)));
    Ra1=min(Ra,N_seq(B(1))-N_seq(b));
    Ra2=fix((Res-c(b)*Ra1)/(c_0+c(b)));
    N_seq(b)=N_seq(b)+Ra1+Ra2;
    N_add(b)=N_add(b)+Ra1+Ra2;
    Res1=Res-Ra1*c(b)-Ra2*(c_0+c(b));
       if  Res1/c(B(2))>=1
    if b==B(2)
    Ra3=fix(Res1/c(B(3)));
    Ra4=min(Ra3,N_seq(b)-N_seq(B(3)));
    N_seq(B(3))=N_seq(B(3))+Ra4; 
    N_add(B(3))=N_add(B(3))+Ra4; 
    else        
    Ra3=fix(Res1/c(B(2)));
    Ra4=min(Ra3,N_seq(b)-N_seq(B(2)));
    N_seq(B(2))=N_seq(B(2))+Ra4; 
    N_add(B(2))=N_add(B(2))+Ra4; 
    end
       end
    end
end
Repl_num=N_add;
run=max(Repl_num);
Y=randvar_gen(Sigma,Mu,run,k);
for r=1:k   
Samples(r,N_old(r)+1:N_seq(r))=Y(r,1:Repl_num(r));
end
  break  
end
l=max(N_add);
if l>0
Repl_num=N_add;
run=max(Repl_num);
Y=randvar_gen(Sigma,Mu,run,k);
for r=1:k   
Samples(r,N_old(r)+1:N_seq(r))=Y(r,1:Repl_num(r));
end
end
end
Mean_scba=sum(Samples,2)./N_seq;
 if find(Mean_scba==max(Mean_scba))==find(Mu==max(Mu))
     Sum_scba=Sum_scba+1;
 end
%SeqIBA performance
N_seq=n*ones(k,1);
TC=n*(sum(c)+c_0);
Samples=zeros(k,T);
Tseq=T-TC;
Samples(:,1:n)=Init_samp;
loop=1;
while 1>0
if loop>1
[Mean,covar]=para_estim(Samples,N_seq);
end
loop=loop+1;
TC=TC+delta;
[N,b]=main_prog(TC,covar,Mean,c,c_0,0);
N_old=N_seq;
N_seq=max(N_seq,N);
N_seq=fix(N_seq);
N_add=N_seq-N_old;
Tseq=Tseq-(sum(c.*N_add)+c_0*max(N_add));
if Tseq<=0
TR=Tseq+(sum(c.*N_add)+c_0*max(N_add));
lamda=TR/(sum(c.*N_add)+c_0*max(N_add));
N_add=lamda*N_add;
N_add=fix(N_add);
N_seq=N_old+N_add;
Res=T-(sum(c.*N_seq)+c_0*max(N_seq));
if Res/c(b)>=1
    [~,B]=sort(N_seq,'descend');
    if b==B(1)
    Ra=fix(Res/(c_0+c(b)));
    N_seq(b)=N_seq(b)+Ra;
     N_add(b)=N_add(b)+Ra;
    Res1=Res-Ra*(c_0+c(b));
    if  Res1/c(B(2))>=1
    Ra1=fix(Res1/c(B(2)));
    Ra2=min(Ra1,N_seq(b)-N_seq(B(2)));
   N_seq(B(2))=N_seq(B(2))+Ra2;  
   N_add(B(2))=N_add(B(2))+Ra2;  
    Res2=Res1-Ra2*c(B(2));
    if  Res1/c(B(3))>=1
    Ra3=fix(Res2/c(B(3)));
    Ra4=min(Ra3,N_seq(b)-N_seq(B(3)));
    N_seq(B(3))=N_seq(B(3))+Ra4; 
    N_add(B(3))=N_add(B(3))+Ra4; 
    end
    end
    else
    Ra=fix(Res/(c(b)));
    Ra1=min(Ra,N_seq(B(1))-N_seq(b));
    Ra2=fix((Res-c(b)*Ra1)/(c_0+c(b)));
    N_seq(b)=N_seq(b)+Ra1+Ra2;
    N_add(b)=N_add(b)+Ra1+Ra2;
    Res1=Res-Ra1*c(b)-Ra2*(c_0+c(b));
       if  Res1/c(B(2))>=1
    if b==B(2)
    Ra3=fix(Res1/c(B(3)));
    Ra4=min(Ra3,N_seq(b)-N_seq(B(3)));
    N_seq(B(3))=N_seq(B(3))+Ra4; 
    N_add(B(3))=N_add(B(3))+Ra4; 
    else        
    Ra3=fix(Res1/c(B(2)));
    Ra4=min(Ra3,N_seq(b)-N_seq(B(2)));
    N_seq(B(2))=N_seq(B(2))+Ra4; 
    N_add(B(2))=N_add(B(2))+Ra4; 
    end
       end
    end
end
Repl_num=N_add;
run=max(Repl_num);
Y=randvar_gen(Sigma,Mu,run,k);
for r=1:k   
Samples(r,N_old(r)+1:N_seq(r))=Y(r,1:Repl_num(r));
end
  break  
end
l=max(N_add);
if l>0
Repl_num=N_add;
run=max(Repl_num);
Y=randvar_gen(Sigma,Mu,run,k);
for r=1:k   
Samples(r,N_old(r)+1:N_seq(r))=Y(r,1:Repl_num(r));
end
end
end
Mean_siba=sum(Samples,2)./N_seq;
 if find(Mean_siba==max(Mean_siba))==find(Mu==max(Mu))
     Sum_siba=Sum_siba+1;
 end
 end
end
 PCS_rcba=Sum_rcba/num;
 PCS_cba=Sum_cba/num;
 PCS_iba=Sum_iba/num;
 PCS_eba=Sum_eba/num;
  PCS_scba=Sum_scba/num;
    PCS_siba=Sum_siba/num;
 toc
end