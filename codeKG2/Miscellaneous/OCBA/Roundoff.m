function AAl=Roundoff(N,T,N_0,c_0,c,b)
AAl=max(N-N_0,0);
ratio=(T-(sum(c)+c_0)*N_0)/(sum(c.*AAl)+c_0*max(AAl));
AAl=ratio*AAl;
AAl=fix(AAl);
Res=T-(sum(c.*AAl)+c_0*max(AAl)+(sum(c)+c_0)*N_0);
if Res/c(b)>=1
    [~,B]=sort(AAl,'descend');
    if b==B(1)
    Ra=fix(Res/(c_0+c(b)));
    AAl(b)=AAl(b)+Ra;
    Res1=Res-Ra*(c_0+c(b));
     if  Res1/c(B(2))>=1
    Ra1=fix(Res1/c(B(2)));
    Ra2=min(Ra1,AAl(b)-AAl(B(2)));
    AAl(B(2))=AAl(B(2))+Ra2;
    Res2=Res1-Ra2*c(B(2));
    if Res1/c(B(3))>=1
    Ra3=fix(Res2/c(B(3)));
    Ra4=min(Ra3,AAl(b)-AAl(B(3)));
    AAl(B(3))=AAl(B(3))+Ra4;
    end
    end
    else
    Ra=fix(Res/(c(b)));
    Ra1=min(Ra,AAl(B(1))-AAl(b));
    Ra2=fix((Res-c(b)*Ra1)/(c_0+c(b)));
    AAl(b)=AAl(b)+Ra1+Ra2;
     Res1=Res-Ra1*c(b)-Ra2*(c_0+c(b));
     if Res1/c(B(2))>=1
    if b==B(2)
    Ra3=fix(Res1/c(B(3)));
    Ra4=min(Ra3,AAl(b)-AAl(B(3)));
    AAl(B(3))=AAl(B(3))+Ra4;  
    else    
    Ra3=fix(Res1/c(B(2)));
    Ra4=min(Ra3,AAl(b)-AAl(B(2)));
    AAl(B(2))=AAl(B(2))+Ra4;      
    end
     end
    end
end
end