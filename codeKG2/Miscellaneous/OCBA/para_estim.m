function [Mean,covar]=para_estim(Y,Repl_num)
if length(Repl_num)==1
Mean=mean(Y,2);
covar=cov(Y');
else
k=size(Y,1);
Mean=zeros(k,1);
covar=zeros(k,k);
for i=1:k
    Mean(i)=mean(Y(i,1:Repl_num(i)));
for j=1:k
    n=min(Repl_num(i),Repl_num(j));
   y_i=Y(i,1:n);
   y_j=Y(j,1:n);
   C=cov(y_i,y_j);
    covar(i,j)=C(1,2);
end
end
end
end