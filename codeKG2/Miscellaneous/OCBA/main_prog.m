function [N,b]=main_prog(T,covar,Mean,c,c_0,Dep)
k=length(Mean);
[Mx,b]=max(Mean);
I_b=find(Mean==Mx);
Omega=setdiff((1:k)',I_b);
%step1
alpha=zeros(k,1);
alpha(Omega)=((Mean(Omega)-Mean(b)).^2);
D=diag(covar);
if Dep==0
covar=zeros(k,k);
for i=1:k
    covar(i,i)=D(i);
end
end
a=zeros(k,1);
a(Omega)=(D(b)+D(Omega)-2*covar(Omega,b))./alpha(Omega);
ind= 2*covar(Omega,b)<D(Omega);
I=Omega(ind);
J=setdiff(Omega,I);
if isempty(I)||isempty(J)
if isempty(I)
    a_0=max(a(J));
else a_0=max(D(b)./alpha(I)); 
end
else
a_0=max(max(a(J)),max(D(b)./alpha(I)));
end
%step2
ind= a(I)>a_0;
I_0=I(ind);
J_0=setdiff(Omega,I_0);
[a_ord,ind]=unique(a(I_0));
Index=I_0(ind);
%step3
cons=sum(c(Omega)+c_0)/c(b);
upbound=max(a(Omega)+cons/4*max((D(b)-2*covar(Omega,b)),D(b))./alpha(Omega))+1;
l=length(I_0);
l_a=length(Index);
if l<2
 r=0;
 Ind_2=(1:l_a+1)'; 
 E=[];
 %B=[];
 %K=a_ord;
    if l==0
 Index=b;
    end
else
E=zeros(l,2);
B=zeros(l,l);
for i=1:l
 for j=1:l
 if i==j
     B(i,j)=0;
 else
     ind_i=I_0(i);
     ind_j=I_0(j);
e=(D(ind_i)-2*covar(ind_i,b))*alpha(ind_j)-(D(ind_j)-2*covar(ind_j,b))*alpha(ind_i);
e_1=D(b)*((D(ind_i)-2*covar(ind_i,b))-(D(ind_j)-2*covar(ind_j,b)));
    if e==0
     if e_1==0
      B(j,i)=-inf;
     else
     if e_1>0
      B(j,i)=-inf;
     else
      B(i,j)=-inf;  
     end
     end
    else
  if e>0
      B(j,i)=e_1/e; 
  else
    B(i,j)=e_1/e;  
  end
    end
 end
 end
end
for i=1:l
FX=[B(:,i);B(i,:)'];
ind1=find(FX==-inf);
ind2= ind1>l;
ind3=ind1(ind2);
FX(ind3)=0;
indx=find(FX);
FX=FX(indx);
[F,Ind]=sort(FX);    
 Indx=indx(Ind);
 if isempty(Indx)
     E(i,:)=0;
 else
if Indx<=l
    E(i,1)=F(end:end);
    E(i,2)=upbound;
else
if Indx>l
    E(i,1)=-inf;
  E(i,2)=F(1);
else if Indx(1)<=l
     ind=find(Indx>l);
    if Indx(ind(1):end)>l
         E(i,1)=F(ind(1)-1);
          E(i,2)=F(ind(1));
    end       
     end
end
end
 end
end
ind1=find(E(:,2)>a_0);
E=E(ind1,:);
ind2=I_0(ind1);
ind3=find(E(:,1)<upbound);
E=E(ind3,:);
ind4=ind2(ind3);
[~,Ind]=unique(E(:,2));
E=E(Ind,:);
Index=ind4(Ind);
r=length(Index);
if r==1
r=0;
Ind_2=(1:l_a+1)';
%K=a_ord;
else
r=r-1;
K=sort([E(1:r,2);a_ord]);
Ind_1=zeros(r,1);
for i=1:r
ind=find(K==E(i,2));
Ind_1(i)=ind(1);
end
Ind_2=setdiff((1:r+l_a+1)',Ind_1);
end
end
%step4,5,6
m=a_0;
a_ord(l_a+1)=upbound;
num=1;
num_1=1;
num_2=1;
num_3=1;
num_4=1;
%test_2=zeros(l+r+1,1);
%test_3=zeros(l+r+1,1);
%test_4=zeros(l+r+1,1);
%test_5=zeros(l+r+1,1);
%test_6=zeros(l+r+1,1);
%test_7=zeros(l+r+1,1);
for i=0:l_a+r
    if i==0
        x=a_0;
        if isempty(I_0)||isempty(J_0)
if isempty(I_0)
        s_1=x;
    h=x*(sum(c(I_b))+c_0)+sum(c(J_0).*D(J_0)./(alpha(J_0)*x-D(b)+2*covar(J_0,b)))*x;  
else v=Index(num_1);
    s_1=x+10^(-2)*(a_ord(1)-a_0);
    h=s_1*sum(c(I_b))+sum(c(I_0).*(D(I_0)-2*covar(I_0,b))./(alpha(I_0)*s_1-D(b)))*s_1+c_0*(D(v)-2*covar(v,b))/(alpha(v)*s_1-D(b))*s_1; 
end
        else v=Index(num_1);
            s_1=x+10^(-2)*(a_ord(1)-a_0);
h=s_1*sum(c(I_b))+sum(c(J_0).*D(J_0)./(alpha(J_0)*s_1-D(b)+2*covar(J_0,b)))*s_1+sum(c(I_0).*(D(I_0)-2*covar(I_0,b))./(alpha(I_0)*s_1-D(b)))*s_1+c_0*(D(v)-2*covar(v,b))/(alpha(v)*s_1-D(b))*s_1;
        end
    else
   if isempty(intersect(i,Ind_2))
        inter=E(:,2);
        x=inter(num_1);
         num_1=num_1+1;
   else
 x=a_ord(num_2);
         num_2=num_2+1;
   end
       s_1=x;
    end
         if isempty(intersect(i+1,Ind_2))
        inter=E(:,2);
        y=inter(num_3);
          num_3=num_3+1;
         else
% w=length(a_ord);
 %if num_4>w
  % disp([a_0,upbound,num_4,l,num_3,num_2,num_1,i,r])
 %end
 y=a_ord(num_4);
  num_4=num_4+1;
         end   
    s_2=y;
ind= a(I)>x;
I_x=I(ind);
J_x=setdiff(Omega,I_x);
%test_1(i+1)=s_1;
%test_2(i+1)=s_2;
%p=length(I_x);
%test_3(1:p,i+1)=I_x;
if isempty(I_x)||isempty(J_x)
    if isempty(I_x)
        v=1;
         a_left=sum(c(I_b))+c_0-sum(c(J_x).*D(J_x).*(D(b)-2*covar(J_x,b))./(alpha(J_x)*s_1-D(b)+2*covar(J_x,b)).^2);
         a_right=sum(c(I_b))+c_0-sum(c(J_x).*D(J_x).*(D(b)-2*covar(J_x,b))./(alpha(J_x)*s_2-D(b)+2*covar(J_x,b)).^2);
         %test_4(i+1)=x*(c(b)+c_0)+sum(c(J_x).*D(J_x)./(alpha(J_x)*x-D(b)+2*covar(J_x,b)))*x; 
         %test_5(i+1)=y*(c(b)+c_0)+sum(c(J_x).*D(J_x)./(alpha(J_x)*y-D(b)+2*covar(J_x,b)))*y; 
         %test_6(i+1)=a_left;
         %test_7(i+1)=a_right;
         if(a_left<0)&&(a_right>0)
   x=Dichotomy(I_x,J_x,D,covar,b,I_b,c,c_0,v,alpha,s_1,s_2);
         end
 h_x=x*(sum(c(I_b))+c_0)+sum(c(J_x).*D(J_x)./(alpha(J_x)*x-D(b)+2*covar(J_x,b)))*x; 
    else
        v=Index(num_1);
a_left=sum(c(I_b))-sum(D(b)*c(I_x).*(D(I_x)-2*covar(I_x,b))./(alpha(I_x)*s_1-D(b)).^2)-D(b)*c_0*(D(v)-2*covar(v,b))/(alpha(v)*s_1-D(b))^2;
a_right=sum(c(I_b))-sum(D(b)*c(I_x).*(D(I_x)-2*covar(I_x,b))./(alpha(I_x)*s_2-D(b)).^2)-D(b)*c_0*(D(v)-2*covar(v,b))/(alpha(v)*s_2-D(b))^2;
         %test_4(i+1)=x*c(b)+sum(c(I_x).*(D(I_x)-2*covar(I_x,b))./(alpha(I_x)*x-D(b)))*x+c_0*(D(v)-2*covar(v,b))/(alpha(v)*x-D(b))*x;    
         %test_5(i+1)=y*c(b)+sum(c(I_x).*(D(I_x)-2*covar(I_x,b))./(alpha(I_x)*y-D(b)))*y+c_0*(D(v)-2*covar(v,b))/(alpha(v)*y-D(b))*y;
         %test_6(i+1)=a_left;
         %test_7(i+1)=a_right;
if(a_left<0)&&(a_right>0)
   x=Dichotomy(I_x,J_x,D,covar,b,I_b,c,c_0,v,alpha,s_1,s_2);
 end
h_x=x*sum(c(I_b))+sum(c(I_x).*(D(I_x)-2*covar(I_x,b))./(alpha(I_x)*x-D(b)))*x+c_0*(D(v)-2*covar(v,b))/(alpha(v)*x-D(b))*x;    
    end
else
    v=Index(num_1);
a_left=sum(c(I_b))-sum(c(J_x).*D(J_x).*(D(b)-2*covar(J_x,b))./(alpha(J_x)*s_1-D(b)+2*covar(J_x,b)).^2)-sum(D(b)*c(I_x).*(D(I_x)-2*covar(I_x,b))./(alpha(I_x)*s_1-D(b)).^2)-D(b)*c_0*(D(v)-2*covar(v,b))/(alpha(v)*s_1-D(b))^2;
a_right=sum(c(I_b))-sum(c(J_x).*D(J_x).*(D(b)-2*covar(J_x,b))./(alpha(J_x)*s_2-D(b)+2*covar(J_x,b)).^2)-sum(D(b)*c(I_x).*(D(I_x)-2*covar(I_x,b))./(alpha(I_x)*s_2-D(b)).^2)-D(b)*c_0*(D(v)-2*covar(v,b))/(alpha(v)*s_2-D(b))^2;
       %test_4(i+1)=x*c(b)+sum(c(J_x).*D(J_x)./(alpha(J_x)*x-D(b)+2*covar(J_x,b)))*x+sum(c(I_x).*(D(I_x)-2*covar(I_x,b))./(alpha(I_x)*x-D(b)))*x+c_0*(D(v)-2*covar(v,b))/(alpha(v)*x-D(b))*x;      
       %test_5(i+1)=y*c(b)+sum(c(J_x).*D(J_x)./(alpha(J_x)*y-D(b)+2*covar(J_x,b)))*y+sum(c(I_x).*(D(I_x)-2*covar(I_x,b))./(alpha(I_x)*y-D(b)))*y+c_0*(D(v)-2*covar(v,b))/(alpha(v)*y-D(b))*y; 
       %test_6(i+1)=a_left;
       %test_7(i+1)=a_right;
if(a_left<0)&&(a_right>0)
   x=Dichotomy(I_x,J_x,D,covar,b,I_b,c,c_0,v,alpha,s_1,s_2);
end
h_x=x*sum(c(I_b))+sum(c(J_x).*D(J_x)./(alpha(J_x)*x-D(b)+2*covar(J_x,b)))*x+sum(c(I_x).*(D(I_x)-2*covar(I_x,b))./(alpha(I_x)*x-D(b)))*x+c_0*(D(v)-2*covar(v,b))/(alpha(v)*x-D(b))*x;        
end
if (h_x<h)
    m=x;
    h=h_x;
    num=num_1;
end
 end
%step7
 N=zeros(k,1);
ind= a(Omega)>m;
I_1=Omega(ind);
J_1=setdiff(Omega,I_1);
if isempty(I_1)||isempty(J_1)
if isempty(I_1)
   N(I_b)=T/(sum(c(I_b))+c_0+sum(c(J_1).*D(J_1)./(alpha(J_1)*m-D(b)+2*covar(J_1,b))));
   N(J_1)=D(J_1)./(alpha(J_1)*m-D(b)+2*covar(J_1,b))*N(b);
else v=Index(num);
    N(I_b)=T/(sum(c(I_b))+sum(c(I_1).*(D(I_1)-2*covar(I_1,b))./(alpha(I_1)*m-D(b)))+c_0*(D(v)-2*covar(v,b))/(alpha(v)*m-D(b)));
    N(I_1)=(D(I_1)-2*covar(I_1,b))./(alpha(I_1)*m-D(b))*N(b);
end
else v=Index(num);
N(I_b)=T/(sum(c(I_b))+sum(c(J_1).*D(J_1)./(alpha(J_1)*m-D(b)+2*covar(J_1,b)))+sum(c(I_1).*(D(I_1)-2*covar(I_1,b))./(alpha(I_1)*m-D(b)))+c_0*(D(v)-2*covar(v,b))/(alpha(v)*m-D(b)));
N(J_1)=D(J_1)./(alpha(J_1)*m-D(b)+2*covar(J_1,b))*N(b);
N(I_1)=(D(I_1)-2*covar(I_1,b))./(alpha(I_1)*m-D(b))*N(b);
end
%if isnan(N)
%disp([length(I_1),length(J_1),m,v])
%erro
%end
end