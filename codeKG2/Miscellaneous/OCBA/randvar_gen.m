function Y=randvar_gen(Sigma,Mu,run,k)
A=chol(Sigma);
C=A';
X=normrnd(0,1,[k,run]);
Me=repmat(Mu,[1,run]);
Y=Me+C*X;
end