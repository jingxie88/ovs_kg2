function M_C=Mecomp(Y,AAl,Mean,k,n)
AS=zeros(k,1);
I=find(AAl>0);
l=length(I);
 for i=1:l
 AS(I(i))=sum(Y(I(i),1:AAl(I(i))));
 end
M_C=(Mean*n+AS)./(n+AAl);
end