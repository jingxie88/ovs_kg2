function x=Dichotomy(I_x,J_x,D,covar,b,I_b,c,c_0,v,alpha,s_1,s_2)
if s_2==inf
   s_2=10000*s_1;
end
if isempty(I_x)||isempty(J_x)
if isempty(I_x)
    F=sum(c(I_b))-sum(c(J_x).*D(J_x).*(D(b)-2*covar(J_x,b))./(alpha(J_x)*s_1-D(b)+2*covar(J_x,b)).^2);
    x=s_1;
    i=0;
while (abs(F)>10^(-2))||(abs(s_1-s_2))>10^(-2)
       x=(s_1+s_2)/2;
       F=sum(c(I_b))-sum(c(J_x).*D(J_x).*(D(b)-2*covar(J_x,b))./(alpha(J_x)*x-D(b)+2*covar(J_x,b)).^2);  
       if F>0
           s_2=x;
       else s_1=x;
       end
       i=i+1;
if i>=100
  break
end
end
else x=s_1;
    F=sum(c(I_b))-sum(D(b)*c(I_x).*(D(I_x)-2*covar(I_x,b))./(alpha(I_x)*s_1-D(b)).^2)-D(b)*c_0*(D(v)-2*covar(v,b))/(alpha(v)*s_1-D(b))^2;
  i=0;
    while (abs(F)>10^(-2))||(abs(s_1-s_2))>10^(-2)
       x=(s_1+s_2)/2;
       F=sum(c(I_b))-sum(D(b)*c(I_x).*(D(I_x)-2*covar(I_x,b))./(alpha(I_x)*x-D(b)).^2)-D(b)*c_0*(D(v)-2*covar(v,b))/(alpha(v)*x-D(b))^2;  
       if F>0
           s_2=x;
       else s_1=x;
       end
            i=i+1;
       if i>=100
  break
       end
    end
end
else x=s_1;
F=sum(c(I_b))-sum(c(J_x).*D(J_x).*(D(b)-2*covar(J_x,b))./(alpha(J_x)*s_1-D(b)+2*covar(J_x,b)).^2)-sum(D(b)*c(I_x).*(D(I_x)-2*covar(I_x,b))./(alpha(I_x)*s_1-D(b)).^2)-D(b)*c_0*(D(v)-2*covar(v,b))/(alpha(v)*s_1-D(b))^2;
 i=0;
while (abs(F)>10^(-2))||(abs(s_1-s_2))>10^(-2)
       x=(s_1+s_2)/2;
F=sum(c(I_b))-sum(c(J_x).*D(J_x).*(D(b)-2*covar(J_x,b))./(alpha(J_x)*x-D(b)+2*covar(J_x,b)).^2)-sum(D(b)*c(I_x).*(D(I_x)-2*covar(I_x,b))./(alpha(I_x)*x-D(b)).^2)-D(b)*c_0*(D(v)-2*covar(v,b))/(alpha(v)*x-D(b))^2;
       if F>0
           s_2=x;
       else s_1=x;
       end
                   i=i+1;
       if i>=100
  break
       end
end
end
end