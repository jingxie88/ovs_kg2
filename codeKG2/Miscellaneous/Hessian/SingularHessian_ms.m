function [hm,hst] = SingularHessian_ms(I,Dim,istar,invS,Yt,J,Cofx,NumSampled,CofAltsSampled,Sigma0,Sigma,alpha,B,gs,gb)
% I shoud be row vector.
hm = zeros(Dim,Dim,length(I)); 
hs = zeros(Dim,Dim,length(I));
hst = zeros(Dim,Dim,length(I));  
hb = zeros(Dim,Dim); 

for row = 1:Dim
    for col = 1:row
        if col == row
           ths =  2*alpha(row)*(J(row,:).*(CofAltsSampled(:,row)'-Cofx(row))-Sigma0(istar,1:NumSampled));
        else
           ths =  J(row,:).*J(col,:)./Sigma0(istar,1:NumSampled);
        end
        tmp = ths*(invS*Sigma0(1:NumSampled,istar))+J(row,:)*(invS*J(col,:)');
        hb(row,col) = -(tmp+gb(row)*gb(col))/B;
        for j = 1:length(I)
            i = I(j);
            if i~=istar
                hs(row,col,j) = ths(i)-ths*(invS*Sigma0(1:NumSampled,i));
            else
                hm(row,col,j) = ths*(invS*Yt);
                hs(row,col,j) = -2*tmp;
            end
            hst(row,col,j) = (-gb(col)*gs(row,j)+B*hs(row,col,j)-gs(col,j)*gb(row)-Sigma(i,istar)*(hb(row,col)-2/B*gb(row)*gb(col)))/B/B;
        end
     end
end
