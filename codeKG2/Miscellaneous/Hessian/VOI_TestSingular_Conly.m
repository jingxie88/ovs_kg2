function [V,g,h] = VOI_TestSingular_Conly(Cofx,n,Mu,Sigma,mu0,Sigma0,cov0hat,scalehat,noisevarhat,invS,CofAltsSampled,Yt)
Dim=length(Cofx);        
V=-inf;
AlreadySampled=find(sum(abs(CofAltsSampled-ones(n,1)*Cofx),2)==0);
TMP=1:n;
TMP(AlreadySampled)=[];
        
if (~isempty(TMP))
    m= Mu(TMP)==max(Mu(TMP)); 
    mi=ChooseRandomElement(TMP(m));            
    if (~isempty(AlreadySampled))
        A=[AlreadySampled(1),mi];
        istar=AlreadySampled(1);
        [V,st]=VOI(istar,A,1,Mu,Sigma,eye(n)*noisevarhat);
        if nargout>1
            if isnan(st)
                g=randn(Dim,1);
                h = randn(Dim,Dim);h=h+h';
            else
                if nargout == 2
                    g = -SingularGradient_VOI(1,Dim,n,CofAltsSampled,istar,Sigma0,Sigma,invS,Yt,noisevarhat,...
					Cofx,scalehat.^(-2),1,A,Mu,st,1,V);
                else
                    [g,h] = SingularGradient_VOI(1,Dim,n,CofAltsSampled,istar,Sigma0,Sigma,invS,Yt,noisevarhat,...
					Cofx,scalehat.^(-2),1,A,Mu,st,1,V);
                   g = -g; h = -h;
                end
            end
        end
    else
        %Cofx=Coordinates(x,Dim);
        [MuP,SigmaP,Sigmat0]=Update_KalmanPlus_Conly_Abr(Mu,Sigma,mu0,Sigma0,cov0hat,scalehat,n,invS,CofAltsSampled,Yt,Cofx);
        %m=MuP(end);
        A=[n+1,mi];
        [V,st]=VOI(n+1,A,1,MuP,SigmaP,eye(n+1)*noisevarhat);
        if nargout>1
            if isnan(st)
                g=randn(Dim,1);
                h = randn(Dim,Dim);h=h+h';
            else
                if nargout == 2
                    g = -SingularGradient_VOI(1,Dim,n,CofAltsSampled,n+1,Sigmat0,SigmaP,invS,Yt,noisevarhat,...
						 Cofx,scalehat.^(-2),1,A,MuP,st,1,V);
                else
                    [g,h] = SingularGradient_VOI(1,Dim,n,CofAltsSampled,n+1,Sigmat0,SigmaP,invS,Yt,noisevarhat,...
                Cofx,scalehat.^(-2),1,A,MuP,st,1,V);
                g = -g; h = -h;
                end
            end
        end
    end
else
    g=randn(Dim,1);h = randn(Dim,Dim);h = h+h';
end
V=-V; % we return -log(VOI), since we optimize using 'fmincon'.
% disp(V)
% if nargout>1
% disp(g)
% if nargout>2
% disp(h)
% end
% end
