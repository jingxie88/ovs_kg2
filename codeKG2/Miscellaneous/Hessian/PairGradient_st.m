function [g,den,g11,g12,g21,g22] = PairGradient_st(I,Dim,NumSampled,istar,Sigma0,Sigma,invS,J,SampVar,SampCorr,Cofx,alpha,beta)

temp = 2*Sigma0(istar(1),istar(2))*(diff(Cofx))'.*(alpha(:));
temp1 = J*(invS*Sigma0(1:NumSampled,istar(1)));
temp2 = J*(invS*Sigma0(1:NumSampled,istar(2)));

densqure = 2*SampVar*(1-SampCorr)/beta+Sigma(istar(1),istar(1))+Sigma(istar(2),istar(2))-2*Sigma(istar(1),istar(2));
den = sqrt(densqure);

g21 = (-temp+temp2(1:Dim)-temp1(1:Dim))/den;
g22 = (temp+temp1(1+Dim:end)-temp2(1+Dim:end))/den;

l = length(I);
g = zeros(2*Dim,l);
g11 = zeros(Dim,l);
g12 = zeros(Dim,l);
for j = 1:l
i = I(j);
if i~=istar(1)
    if i==istar(2)
        g11(:,j) = temp-J(1:Dim,:)*(invS*Sigma0(1:NumSampled,i));
    else
        g11(:,j) = J(1:Dim,i)-J(1:Dim,:)*(invS*Sigma0(1:NumSampled,i));
    end
else
g11(:,j) = -temp-2*temp1(1:Dim)+temp2(1:Dim);
end
if i~=istar(2)
    if i==istar(1)
        g12(:,j) = temp+J(Dim+1:end,:)*(invS*Sigma0(1:NumSampled,i));
    else
        g12(:,j) = -J(Dim+1:end,i)+J(Dim+1:end,:)*(invS*Sigma0(1:NumSampled,i));
    end
else
g12(:,j) = -temp+2*temp2(1+Dim:end)-temp1(1+Dim:end);
end
g(:,j) = [(den*g11(:,j)-(Sigma(i,istar(1))-Sigma(i,istar(2)))*g21);(den*g12(:,j)-(Sigma(i,istar(1))-Sigma(i,istar(2)))*g22)]/densqure;
end
% disp(g11)
% disp(g12)
% disp(g21)
% disp(g22)