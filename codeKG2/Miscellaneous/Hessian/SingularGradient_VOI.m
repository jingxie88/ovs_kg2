function [g,h] = SingularGradient_VOI(I,Dim,NumSampled,CofAltsSampled,istar,Sigma0,Sigma,invS,Yt,SampVar,Cofx,alpha,beta,A,mu,st,c,V)

PriorCovVec = Sigma0(istar,1:NumSampled);
J = JforGradient(Cofx,Dim,NumSampled,CofAltsSampled,PriorCovVec,alpha);

if length(A)>2
AI = A(I);
ga = SingularGradient_mu(AI,Dim,istar,invS,Yt,J);
gb = SingularGradient_st(AI,Dim,NumSampled,istar,Sigma0,Sigma,invS,J,SampVar,beta);
a = mu(AI); b = st(AI);
a = a(:); b = b(:); c = c(2:end-1); c = c(:); 
% length(c) should be length(a) or length(b)-1
if isempty(c)
    g = zeros(Dim,1);
else
    g = (diff(gb,1,2)*fm(abs(c))-Gradient_c(a,b,ga,gb)*(sign(c).*normcdf(-abs(c)).*diff(b)))/exp(V);
end

else % length(A)==2
s = abs(st(A(1))-st(A(2)));
if s < 1e-8
    g = zeros(Dim,1);
    if nargout>1
       h = zeros(Dim,Dim);
    end
else
    if V < -1e2
        g = randn(Dim,1);
       if nargout>1
          h = randn(Dim,Dim);h = h+h';
       end
    else
        d = abs(mu(A(1))-mu(A(2)));
        [what,B,gsn,gb] = SingularGradient_st(A,Dim,NumSampled,istar,Sigma0,Sigma,invS,J,SampVar,beta);
        gs = -sign(st(A(1))-st(A(2)))*diff(what,1,2);
        gd = -sign(mu(A(1))-mu(A(2)))*diff(SingularGradient_mu(A,Dim,istar,invS,Yt,J),1,2);
        sth = gd*s-d*gs;sth1 = fm(d/s); sth2=normcdf(-d/s);
        kao = sth1*gs-sth2*sth/s;
        g = sign(kao).*exp(log(abs(kao))-V);
        if nargout>1
           [hm,hst] = SingularHessian_ms(A,Dim,istar,invS,Yt,J,Cofx,NumSampled,CofAltsSampled,Sigma0,Sigma,alpha,B,gsn,gb);
           hd = sign(mu(A(1))-mu(A(2)))*(hm(:,:,1)-hm(:,:,2));
           hs = sign(st(A(1))-st(A(2)))*(hst(:,:,1)-hst(:,:,2));
           h = zeros(Dim,Dim);
           for i = 1:Dim
               for j = 1:i
                   h(i,j) = hs(i,j)*sth1+(normpdf(d/s)*sth(i)*sth(j)/s-sth2*s*(hd(i,j)*s-d*hs(i,j)))/s/s;
                   h(i,j) = sign(h(i,j))*exp(log(abs(h(i,j)))-V)-g(i)*g(j);
               end
               if i>1
                  h(1:i-1,i) = h(i,1:i-1)';
               end
           end
        end
    end
end
end
