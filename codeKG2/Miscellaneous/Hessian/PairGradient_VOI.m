function [g,h] = PairGradient_VOI(I,Dim,NumSampled,CofAltsSampled,istar,Sigma0,Sigma,invS,Yt,SampVar,SampCorr,Cofx,alpha,beta,A,mu,st,c,V)
J = JforGradient(Cofx,Dim,NumSampled,CofAltsSampled,Sigma0(istar,1:NumSampled),alpha);
AI = A(I);
ga = PairGradient_mu(AI,Dim,istar,invS,Yt,J);
[gb,B,gs1,gs2,gb1,gb2] = PairGradient_st(AI,Dim,NumSampled,istar,Sigma0,Sigma,invS,J,SampVar,SampCorr,Cofx,alpha,beta);
a = mu(AI); b = st(AI);
a = a(:); b = b(:); c = c(2:end-1); c=c(:);%disp(a);disp(b);disp(c);disp(ga);disp(gb)
% length(c) should be length(a) or length(b)-1
if isempty(c)
    g = zeros(2*Dim,1);h = zeros(2*Dim,2*Dim);
else
%    if V < -1e2
%        disp('a')
%        g = randn(2*Dim,1); h = randn(2*Dim,2*Dim);h = h+h';
%    else
        %g1 = diff(gb,1,2)*fm(abs(c));%disp(g1)
        kao = Gradient_c(a,b,ga,gb);%disp(kao)
        %g2 = kao*(sign(c).*normcdf(-abs(c)).*diff(b));%disp(g2)
        tg = diff(gb,1,2);
        lg = zeros(2*Dim,1);sg = lg;
        for w = 1:2*Dim
            [lg(w),sg(w)] = LogSumExpSignedCoeff([LogEI(-abs(c));LogNormCDF(c)],[tg(w,:)';-kao(w,:)'.*sign(c).*diff(b)]);
        end
        %g = sign(g1-g2).*exp(log(abs(g1-g2))-V);
        g = sg.*exp(lg-V);
        t = max(abs(g));
        if t>1e10
            disp('b')
            g = g/t;
        end
        if nargout>1
           [hm,hst] = PairHessian_ms(AI,Dim,istar,invS,Yt,J,Cofx,NumSampled,CofAltsSampled,Sigma0,Sigma,alpha,B,gs1,gs2,gb1,gb2);
%           disp('hm')          
%           disp(hm)
%           disp('hst')
%           disp(hst)
           hc = zeros(2*Dim,2*Dim,length(c));
           h = zeros(2*Dim,2*Dim);lh=-inf*ones(2*Dim,2*Dim);sh=h;
           for i = 1:2*Dim
               for j = 1:i
                   for k = 1:length(c)
                       hc(i,j,k)=-(((hm(i,j,k+1)-hm(i,j,k))*(b(k+1)-b(k))+(ga(i,k+1)-ga(i,k))*(gb(j,k+1)-gb(j,k))-(ga(j,k+1)-ga(j,k))*(gb(i,k+1)-gb(i,k))-(a(k+1)-a(k))*(hst(i,j,k+1)-hst(i,j,k)))/((b(k+1)-b(k))^2)+2*kao(i,k)*(gb(j,k+1)-gb(j,k))/(b(k+1)-b(k)));
%                       h(i,j) = h(i,j)-normcdf(-abs(c(k)))*sign(c(k))*(hc(i,j,k)*(b(k+1)-b(k))+kao(i,k)*(gb(j,k+1)-gb(j,k))+kao(j,k)*(gb(i,k+1)-gb(i,k)))+fm(abs(c(k)))*(hst(i,j,k+1)-hst(i,j,k))+normpdf(c(k))*kao(i,k)*kao(j,k)*(b(k+1)-b(k));
                       [lh(i,j),sh(i,j)] = LogSumExpSignedCoeff([lh(i,j),LogNormCDF(c(k)),LogEI(-abs(c(k))),LogNormPDF(c(k))],[sh(i,j),-sign(c(k))*(hc(i,j,k)*(b(k+1)-b(k))+kao(i,k)*(gb(j,k+1)-gb(j,k))+kao(j,k)*(gb(i,k+1)-gb(i,k))),hst(i,j,k+1)-hst(i,j,k),kao(i,k)*kao(j,k)*(b(k+1)-b(k))]);
%                       disp(h(i,j))
%                       disp([lh(i,j),sh(i,j)])
                   end
                   h(i,j) = sh(i,j)*exp(lh(i,j)-V)-g(i)*g(j);
 %                  h(i,j) = sign(h(i,j))*exp(log(abs(h(i,j)))-V)-g(i)*g(j);
               end
               if i>1
                  h(1:i-1,i) = h(i,1:i-1)';
               end
           end
%           disp('hc')
%           disp(hc)
        end
%    end
end
