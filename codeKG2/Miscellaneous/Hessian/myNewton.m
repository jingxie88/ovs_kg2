function [xo,Ot,nS]=myNewton(S,x0,Lb,Ub,problem,tol,mxit,mxev)
%   Unconstrained optimization using Newton.
%
%   [xo,Ot,nS]=newton(S,x0,ip,G,H,Lb,Ub,problem,tol,mxit)
%
%   S: objective function
%   x0: initial point
%   ip: (0) no plot (default), (>0) plot figure ip with pause, (<0) plot figure ip
%   G: gradient vector function
%   H: Hessian matrix function
%   Lb, Ub: lower and upper bound vectors to plot (default = x0*(1+/-2))
%   problem: (-1): minimum (default), (1): maximum
%   tol: tolerance (default = 1e-4)
%   mxit: maximum number of iterations (default = 50*(1+4*~(ip>0)))
%   xo: optimal point
%   Ot: optimal value of S
%   nS: number of objective function evaluations

%   Copyright (c) 2001 by LASIM-DEQUI-UFRGS
%   $Revision: 1.0 $  $Date: 2001/07/16 08:10:12 $
%   Argimiro R. Secchi (arge@enq.ufrgs.br)

x00 = x0;opts.SYM = true;
 if nargin < 3 || isempty(Lb),
   Lb=-x0-~x0;
 end
 if nargin < 4 || isempty(Ub),
   Ub=2*x0+~x0;
 end
 if nargin < 5 || isempty(problem),
   problem=-1;
 end
 if nargin < 6 || isempty(tol),
   tol=1e-6;
 end
 if nargin < 7 || isempty(mxit),
   mxit=200;
 end
 if nargin < 8 || isempty(mxev),
   mxev=1000;
 end

 x0=x0(:);
 [y0,gr,he]=feval(S,x0);
 y0=y0*problem;
 gr=gr*problem;
 gr=gr(:);
 he=he*problem;

 %n=size(x0,1);
   
 xo=x0;
 yo=y0;
 it=0;
 nS=1;
 
 while it < mxit && nS < mxev,
  if rcond(he)>1e-4
  d=-linsolve(he,gr,opts);
  else
      d = -pinv(he)*gr;
  end
  stepsize=1;
  x=xo+d;
  yo=feval(S,x)*problem;
  nS=nS+1;

  while yo < y0 || sum((x(:)<Lb(:))+(x(:)>Ub(:)))>0
    stepsize=-0.5*stepsize;
    x=xo+stepsize*d;
    yo=feval(S,x)*problem;
    nS=nS+1;
  end

  xo=x;
  it=it+1;

  if norm(xo-x0) < tol*(0.1+norm(x0)) && abs(yo-y0) < tol*(0.1+abs(y0)),
    break;
  end
  
  [~,gr,he]=feval(S,xo);
         
  gr=gr*problem;
  gr=gr(:);
  he=he*problem;

  x0=xo;
  y0=yo; 
  
 end
 
 Ot=yo*problem;
 
 if it == mxit,
   disp('Warning Newton: reached maximum number of iterations!');
 elseif nS == mxev
        disp('Warning Newton: reached maximum number of function evaluations!');
 end
 
 xo = reshape(xo,size(x00,1),size(x00,2));disp(nS)