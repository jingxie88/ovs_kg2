function [OC,F...%,SF
    ] = CompIA_AKG2M_Hessian(rep,NumStartsP,NumStartsS)
load('CompIA.mat')
beta=1;%N=100;
LD=100;
OC = zeros(1,N+2);
F = zeros(1,N+2);
%SF = zeros(1,N+2);
mu0 = 0;
Dim = 1;
%for rep = 1:REP
theta=(Theta(rep,:))';
%Sigma=Sigma0;
Domain = Domain(:);
options = optimset('GradObj','on','Hessian','user-supplied','Algorithm','trust-region-reflective','TolFun',1e-8,'TolX',1e-8);%,'Display','iter');

IND = randi(LD,[2 Dim]);
SecondStageAlts=Domain(IND);
SecondStageObs=(mvnrnd(theta(IND),[Lambda(IND(1),IND(1)),Lambda(IND(1),IND(2));Lambda(IND(2),IND(1)),Lambda(IND(2),IND(2))]))';
L=[Lambda(IND(1),IND(1)),Lambda(IND(1),IND(2));Lambda(IND(2),IND(1)),Lambda(IND(2),IND(2))];
temp=GPCov(PriorCov,PriorScale,SecondStageAlts(1,:),SecondStageAlts(2,:));
sigma0=[PriorCov,temp;temp,PriorCov];

n = 2;

for iter = 1:1e8    
    Yt=SecondStageObs-mu0;
    invS=inv(sigma0+L);

    [Mu,sigma]=Update_Kalman(mu0,sigma0,n,invS,Yt);
    %disp(Mu)
    x01=ChooseRandomElement(find(Mu==max(Mu)));
    CurrentBest=SecondStageAlts(x01,:);
    
    Bests= sum(abs(SecondStageAlts-repmat(CurrentBest,n,1)),2)==0;
    temp=1:n;temp(Bests)=[];
    
    OC(n)=max(theta)-theta(IND(x01));
    
    %assert(sum(isnan(Mu))==0,'omg')
    if (sum(isnan(Mu))>0)
        Cofx=Domain(randi(LD,[2 Dim]));
        if Dim == 1
            Cofx=Cofx(:);
        end
    else
        if ~isempty(temp)&&(NumStartsP>1)
            x02=temp(ChooseRandomElement(find(Mu(temp)==max(Mu(temp)))));
            SecondBest=SecondStageAlts(x02,:);
            if sum(sum(abs(SecondStageAlts(temp,:)-repmat(SecondBest,length(temp),1)),2)==0)<length(temp)
                if NumStartsP == 2
                    CofX0=[CurrentBest,SecondBest;(Domain(randi(LD,[1 2*Dim])))'];
                else
                    CofX0=[CurrentBest,SecondBest;Domain(randi(LD,[NumStartsP-1 2*Dim]))];
                end
            else
                CofX0=Domain(randi(LD,[NumStartsP 2*Dim]));
            end
        else if NumStartsP == 1
                CofX0=Domain(randi(LD,[NumStartsP 2*Dim]));CofX0 = (CofX0(:))';
            else
                CofX0=Domain(randi(LD,[NumStartsP 2*Dim]));
            end
        end
        % pay attention to matrix size when NumStarts=1or2.
        CofX=zeros(NumStartsP,2*Dim);
        Fval=zeros(NumStartsP,1);
%        grad = @(cc) VOI_TestPair_Gonly_HomoCorr(cc,Dim,n,Mu,sigma,mu0,sigma0,PriorCov,PriorScale,SampVar,SampCorr,invS,SecondStageAlts,Yt);        
%        hess = @(cc) VOI_TestPair_Honly_HomoCorr(cc,Dim,n,Mu,sigma,mu0,sigma0,PriorCov,PriorScale,SampVar,SampCorr,invS,SecondStageAlts,Yt);
        voi = @(cc) VOI_TestPair_Conly_HomoCorr(cc,Dim,n,Mu,sigma,mu0,sigma0,PriorCov,PriorScale,SampVar,SampCorr,invS,SecondStageAlts,Yt);
        %options = optimset('GradObj','on','Hessian','user-supplied','Algorithm','interior-point','HessFcn',hess,'TolFun',1e-8,'TolX',1e-20,'Display','iter');
        for start=1:NumStartsP
            %CofX(start,:) = fmincon(@(cc)VOI_TestPair_Conly_HomoCorr(cc,Dim,n,Mu,sigma,mu0,sigma0,PriorCov,PriorScale,SampVar,SampCorr,invS,SecondStageAlts,Yt),CofX0(start,:),[],[],[],[],Domain(ones(1,2*Dim)),Domain(end)*ones(1,2*Dim),[],options);
            %CofX(start,:) = fmincon(@(cc)VOI_TestPair_Conly_GPSampCorr(cc,Dim,n,Mu,sigma,mu0,sigma0,PriorCov,PriorScale,L,invS,SecondStageAlts,Yt,SampVar,SampCorrScale),CofX0(start,:),[],[],[],[],Domain(ones(1,2*Dim)),Domain(end)*ones(1,2*Dim),[],options);%,'Display','Iter'));
            CofX(start,:) = myNewton(voi,CofX0(start,:),Domain(ones(1,2*Dim)),Domain(end)*ones(1,2*Dim),-1,1e-8);%,mxit)            
            if (sum(isnan(CofX(start,:)))>0)
                CofX(start,:)=CofX0(start,:);
            else
                CofXD=max(min(Domain(end),CofX(start,:)),Domain(1));
                [~,Index]=min(abs(bsxfun(@minus,CofXD,Domain)));
                CofXD=Domain(Index);
                CofX(start,:)=(CofXD(:))';
            end
            Fval(start) = VOI_TestPair_Conly_HomoCorr(CofX(start,:),Dim,n,Mu,sigma,mu0,sigma0,PriorCov,PriorScale,SampVar,SampCorr,invS,SecondStageAlts,Yt);
            %Fval(start) = VOI_TestPair_Conly_GPSampCorr(CofX(start,:),Dim,n,Mu,sigma,mu0,sigma0,PriorCov,PriorScale,L,invS,SecondStageAlts,Yt,SampVar,SampCorrScale);
        end
        [F(n),IS]=min(Fval);F(n)=-F(n);
        Cofx=[CofX(IS,1:Dim);CofX(IS,Dim+1:end)];

        if ~isempty(temp)
            if NumStartsS>2
                x02=temp(ChooseRandomElement(find(Mu(temp)==max(Mu(temp)))));
                SecondBest=SecondStageAlts(x02,:);
                if sum(sum(abs(SecondStageAlts(temp,:)-repmat(SecondBest,length(temp),1)),2)==0)<length(temp)
                    CofX0=[CurrentBest;SecondBest;Domain(randi(LD,[NumStartsS-2 Dim]))];
                else
                    CofX0=[CurrentBest;Domain(randi(LD,[NumStartsS-1 Dim]))];
                end
                
            else if NumStartsS == 2
                    CofX0=[CurrentBest;Domain(randi(LD,[NumStartsS-1 Dim]))];
                else
                    CofX0=Domain(randi(LD,[NumStartsS Dim]));
                end
            end
        else
            CofX0=Domain(randi(LD,[NumStartsS Dim]));
        end
        
        % pay attention to matrix size when NumStarts=1or2.
    
        CofX=zeros(NumStartsS,Dim);
        Fval=zeros(NumStartsS,1);
    
        for start=1:NumStartsS
            CofX(start,:) = fmincon(@(cc)VOI_TestSingular_Conly(cc,n,Mu,sigma,mu0,sigma0,PriorCov,PriorScale,SampVar,invS,SecondStageAlts,Yt),CofX0(start,:),[],[],[],[],Domain(ones(1,Dim)),Domain(end)*ones(1,Dim),[],options);%,'Display','Iter'));
            if (sum(isnan(CofX(start,:)))>0)
                CofX(start,:)=CofX0(start,:);
            else
                CofXD=max(min(Domain(end),CofX(start,:)),Domain(1));
                [~,Index]=min(abs(bsxfun(@minus,CofXD,Domain)));
                CofXD=Domain(Index);
                CofX(start,:)=(CofXD(:))';
            end
            Fval(start) = VOI_TestSingular_Conly(CofX(start,:),n,Mu,sigma,mu0,sigma0,PriorCov,PriorScale,SampVar,invS,SecondStageAlts,Yt);
        end
        [tmp,IS]=min(Fval);
        if tmp < -F(n)
            F(n)=-tmp;
            Cofx=CofX(IS,:);
        end
    end

    if size(Cofx,1)==1 ||(sum(Cofx(1,:)==Cofx(2,:))==Dim)
        disp([rep,n])
        Cofx=Cofx(1,:);
        ind = round((Cofx-Domain(1))/.1)+1;
        IND = [IND;ind];
        AlreadySampled=find(sum(abs(SecondStageAlts-repmat(Cofx,n,1)),2)==0);

        %SF(n) = VOIStar_Singular(Cofx,n,Mu,sigma,mu0,sigma0,PriorCov,PriorScale,SampVar,invS,SecondStageAlts,Yt,1000,SearchLength);
        y=normrnd(theta(ind),sqrt(SampVar));
        n = n+1;
            
        if (n>N)
            break;    
        else

        if (~isempty(AlreadySampled))
            sigma0=[[sigma0,sigma0(:,AlreadySampled(1))];[sigma0(AlreadySampled(1),:),PriorCov]];
        else
            RU=GPCov(PriorCov,PriorScale,SecondStageAlts,Cofx);
            sigma0=[[sigma0,RU];[RU',PriorCov]];
        end
        
        SecondStageAlts=[SecondStageAlts;Cofx];
        SecondStageObs=[SecondStageObs;y];

        L=[[L,zeros(n-1,1)];[zeros(1,n-1),SampVar]];
        L=sparse(L);
        end
    else
        disp([rep,n,n])
        %SF(n) = VOIStar_Pair_HomoCorr(Cofx,n,Mu,sigma,mu0,sigma0,PriorCov,PriorScale,SampVar,SampCorr,invS,SecondStageAlts,Yt,500,SearchLength);           
        OC(n+1) = OC(n);
        F(n+1) = F(n); %SF(n+1) = SF(n);
        n = n+2;

        if n>N
            break;
        else
            
            ind = round((Cofx-Domain(1))/.1)+1;
            IND = [IND;ind];
            RD3=[Lambda(ind(1),ind(1)),Lambda(ind(1),ind(2));Lambda(ind(2),ind(1)),Lambda(ind(2),ind(2))];
            
            Y=(mvnrnd([theta(ind(1)),theta(ind(2))]',RD3))';
            SecondStageObs=[SecondStageObs;Y];

            AlreadySampled=zeros(1,2);

            for j=1:2
                temp=find(sum(abs(SecondStageAlts-repmat(Cofx(j,:),n-2,1)),2)==0);
                if (~isempty(temp))
                    AlreadySampled(j)=temp(1);
                end
            end
            
            temp=GPCov(PriorCov,PriorScale,Cofx(1,:),Cofx(2,:));
            RD=[PriorCov,temp;temp,PriorCov];

            if (AlreadySampled(1)~=0)
                if (AlreadySampled(2)~=0)
                    sigma0=[[sigma0,sigma0(:,[AlreadySampled(1),AlreadySampled(2)])];[sigma0([AlreadySampled(1),AlreadySampled(2)],:),RD]];
                else
                    RU=GPCov(PriorCov,PriorScale,SecondStageAlts,Cofx(2,:));
                    TEMP=[sigma0(:,AlreadySampled(1)),RU];
                    sigma0=[[sigma0,TEMP];[TEMP',RD]];%'
                end
            else if (AlreadySampled(2)~=0)
                    RU=GPCov(PriorCov,PriorScale,SecondStageAlts,Cofx(1,:));
                    TEMP=[RU,sigma0(:,AlreadySampled(2))];
                    sigma0=[[sigma0,TEMP];[TEMP',RD]];
                else
                    RU=GPCov(PriorCov,PriorScale,SecondStageAlts,Cofx);
                    sigma0=[[sigma0,RU];[RU',RD]];
                end
            end
            
            SecondStageAlts=[SecondStageAlts;Cofx];
            L=[[L,zeros(n-2,2)];[zeros(2,n-2),RD3]];
            L=sparse(L);
        end
    end
end