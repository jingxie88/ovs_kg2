function [g,den,g1,g2] = SingularGradient_st(I,Dim,NumSampled,istar,Sigma0,Sigma,invS,J,SampVar,beta)
temp = -J*(invS*Sigma0(1:NumSampled,istar));
den = sqrt(SampVar/beta+Sigma(istar,istar));
g2 = temp/den;

l = length(I);
g = zeros(Dim,l);g1 = zeros(Dim,l);
for j = 1:l
i = I(j);
if i~=istar
g1(:,j) = J(:,i)-J*(invS*Sigma0(1:NumSampled,i));
else
g1(:,j) = 2*temp;
end
g(:,j) = (den*g1(:,j)-Sigma(i,istar)*g2)/(SampVar/beta+Sigma(istar,istar));
end
