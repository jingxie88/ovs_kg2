function [hm,hst,hs,hb] = PairHessian_ms(I,Dim,istar,invS,Yt,J,Cofx,NumSampled,CofAltsSampled,Sigma0,Sigma,alpha,B,gs1,gs2,gb1,gb2)
% I shoud be row vector.
hm = zeros(2*Dim,2*Dim,length(I)); 
hs = zeros(2*Dim,2*Dim,length(I));
hst = zeros(2*Dim,2*Dim,length(I));  
hb = zeros(2*Dim,2*Dim); 

hsi = zeros(2*Dim,2*Dim);

for row = 1:Dim
    for col = 1:row
        if col == row
           ths =  2*alpha(row)*(J(row,:).*(CofAltsSampled(:,row)'-Cofx(1,row))-Sigma0(istar(1),1:NumSampled));
           hsi(row,col) = 2*alpha(row)*Sigma0(istar(1),istar(2))*(2*alpha(row)*(Cofx(1,row)-Cofx(2,row))^2-1)-ths*(invS*Sigma0(1:NumSampled,istar(2)));
        else
           ths =  J(row,:).*J(col,:)./Sigma0(istar(1),1:NumSampled);
           hsi(row,col) = 4*alpha(row)*alpha(col)*Sigma0(istar(1),istar(2))*(Cofx(1,row)-Cofx(2,row))*(Cofx(1,col)-Cofx(2,col))-ths*(invS*Sigma0(1:NumSampled,istar(2)));
        end
        tmp = ths*(invS*Sigma0(1:NumSampled,istar(1)))+J(row,:)*(invS*J(col,:)');
        hb(row,col) = -(tmp+hsi(row,col)+gb1(row)*gb1(col))/B;
        for j = 1:length(I)
            i = I(j);
            if i~=istar(1) 
                if i == istar(2)
                   hs(row,col,j) = hsi(row,col);
                else
                   hs(row,col,j) = ths(i)-ths*(invS*Sigma0(1:NumSampled,i));
                end
            else
                hm(row,col,j) = ths*(invS*Yt);
                hs(row,col,j) = -2*tmp-hsi(row,col);
            end
            hst(row,col,j) = (-gb1(col)*gs1(row,j)+B*hs(row,col,j)-gs1(col,j)*gb1(row)-(Sigma(i,istar(1))-Sigma(i,istar(2)))*(hb(row,col)-2/B*gb1(row)*gb1(col)))/B/B;
        end
     end
end

for row = Dim+1:2*Dim
    for col = Dim+1:row
        if col == row
           ths =  2*alpha(row-Dim)*(J(row,:).*(CofAltsSampled(:,row-Dim)'-Cofx(2,row-Dim))-Sigma0(istar(2),1:NumSampled));
           hsi(row,col) = 2*alpha(row-Dim)*Sigma0(istar(1),istar(2))*(2*alpha(row-Dim)*(Cofx(1,row-Dim)-Cofx(2,row-Dim))^2-1)-ths*(invS*Sigma0(1:NumSampled,istar(1)));           
        else
           ths =  J(row,:).*J(col,:)./Sigma0(istar(2),1:NumSampled);
           hsi(row,col) = 4*alpha(row-Dim)*alpha(col-Dim)*Sigma0(istar(1),istar(2))*(Cofx(1,row-Dim)-Cofx(2,row-Dim))*(Cofx(1,col-Dim)-Cofx(2,col-Dim))-ths*(invS*Sigma0(1:NumSampled,istar(1)));           
        end
        %hsi(row,col) = hsi(row-Dim,col-Dim);
        tmp = ths*(invS*Sigma0(1:NumSampled,istar(2)))+J(row,:)*(invS*J(col,:)');
        hb(row,col) = -(tmp+hsi(row,col)+gb2(row-Dim)*gb2(col-Dim))/B;
        for j = 1:length(I)
            i = I(j);
            if i~=istar(2)
                if i == istar(1)
                   hs(row,col,j) = -hsi(row,col);
                else
                   hs(row,col,j) = -(ths(i)-ths*(invS*Sigma0(1:NumSampled,i)));
                end
            else
                hm(row,col,j) = ths*(invS*Yt);
                hs(row,col,j) = 2*tmp+hsi(row,col);
            end
            hst(row,col,j) = (-gb2(col-Dim)*gs2(row-Dim,j)+B*hs(row,col,j)-gs2(col-Dim,j)*gb2(row-Dim)-(Sigma(i,istar(1))-Sigma(i,istar(2)))*(hb(row,col)-2/B*gb2(row-Dim)*gb2(col-Dim)))/B/B;
        end
     end
end

for row = Dim+1:2*Dim
    for col = 1:Dim
%        if col == row
%           ths =  2*alpha(row-Dim)*(J(row,:).*(CofAltsSampled(:,row-Dim)'-Cofx(2,row-Dim))-Sigma0(istar(2),1:NumSampled));
%        else
%           ths =  J(row,:).*J(col,:)./Sigma0(istar(2),1:NumSampled);
%        end
        if row-Dim == col
           hsi(row,col) = 2*alpha(col)*Sigma0(istar(1),istar(2))*(-2*alpha(col)*(Cofx(1,col)-Cofx(2,col))^2+1)-J(col,:)*(invS*J(row,:)');
        else
           hsi(row,col) = -4*alpha(row-Dim)*alpha(col)*Sigma0(istar(1),istar(2))*(Cofx(1,row-Dim)-Cofx(2,row-Dim))*(Cofx(1,col)-Cofx(2,col))-J(col,:)*(invS*J(row,:)');
        end

%        tmp = ths*(invS*Sigma0(1:NumSampled,istar(2)))+J(row,:)*(invS*J(col,:)');
        hb(row,col) = -(hsi(row,col)+gb2(row-Dim)*gb1(col))/B;
        for j = 1:length(I)
            i = I(j);
            if i == istar(1)
               hs(row,col,j) = -hsi(row,col);
            elseif i == istar(2)
                   hs(row,col,j) = hsi(row,col);
            end
            hst(row,col,j) = (-gb1(col)*gs2(row-Dim,j)+B*hs(row,col,j)-gs1(col,j)*gb2(row-Dim)-(Sigma(i,istar(1))-Sigma(i,istar(2)))*(hb(row,col)-2/B*gb2(row-Dim)*gb1(col)))/B/B;
        end
     end
end
