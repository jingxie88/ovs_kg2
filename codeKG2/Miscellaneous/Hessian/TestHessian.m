%function [g,h] = PairGradient_VOI(I,Dim,NumSampled,CofAltsSampled,istar,Sigma0,Sigma,invS,Yt,SampVar,SampCorr,Cofx,alpha,beta,A,mu,st,c,V)

Cofx = [-1;.5];%CofX(start,:);Cofx = [Cofx(1);Cofx(2)];
NumSampled = n;
CofAltsSampled = SecondStageAlts;
alpha = PriorScale.^(-2);

[MuP,SigmaP,Sigmat0]=Update_KalmanPlus_Conly_Abr(Mu,sigma,mu0,sigma0,PriorCov,PriorScale,NumSampled,invS,CofAltsSampled,Yt,Cofx); 
    
AlreadySampled1=find(sum(abs(CofAltsSampled-ones(NumSampled,1)*Cofx(1,:)),2)==0);
    
AlreadySampled2=find(sum(abs(CofAltsSampled-ones(NumSampled,1)*Cofx(2,:)),2)==0);
    
TMP=1:NumSampled;
   
TMP(union(AlreadySampled1,AlreadySampled2))=[];
    
            
m= Mu(TMP)==max(Mu(TMP)); 
            
mi=ChooseRandomElement(TMP(m));             
A=[NumSampled+1,NumSampled+2,mi];
                %RU=noisevarhat*CorrPredictor(CofAltsSampled,Cofx,dmodel);
                %RL=CorrPredictor(Cofx(1,:),Cofx(2,:),dmodel);
             
EstSampCov1=SampVar*(SampCorr*ones(NumSampled+2)+(1-SampCorr)*eye(NumSampled+2));
                %EstSampCov1=[[EstSampCov,RU];[RU',noisevarhat*[1,RL;RL,1]]];
             
istar=[NumSampled+1,NumSampled+2];
             
[V,st,~,~,c,I]=VOI(istar,A,1,MuP,SigmaP,EstSampCov1);
                       
Sigma0 = Sigmat0;
Sigma = SigmaP;
mu = MuP;

J = JforGradient(Cofx,Dim,NumSampled,CofAltsSampled,Sigma0(istar,1:NumSampled),alpha);
AI = A(I);
ga = PairGradient_mu(AI,Dim,istar,invS,Yt,J);
[gb,B,gs1,gs2,gb1,gb2] = PairGradient_st(AI,Dim,NumSampled,istar,Sigma0,Sigma,invS,J,SampVar,SampCorr,Cofx,alpha,beta);
a = mu(AI); b = st(AI);
a = a(:); b = b(:); c = c(2:end-1); c=c(:);%disp(a);disp(b);disp(c);disp(ga);disp(gb)
% length(c) should be length(a) or length(b)-1
        
kao = Gradient_c(a,b,ga,gb);%disp(kao)
        %g2 = kao*(sign(c).*normcdf(-abs(c)).*diff(b));%disp(g2)
        
tg = diff(gb,1,2);
        
lg = zeros(2*Dim,1);sg = lg;
        
for w = 1:2*Dim
            
[lg(w),sg(w)] = LogSumExpSignedCoeff([LogEI(-abs(c));LogNormCDF(c)],[tg(w,:)';-kao(w,:)'.*sign(c).*diff(b)]);
        
end
        %g = sign(g1-g2).*exp(log(abs(g1-g2))-V);
        
g = sg.*exp(lg-V);

           
[hm,hst,hs,hb] = PairHessian_ms(AI,Dim,istar,invS,Yt,J,Cofx,NumSampled,CofAltsSampled,Sigma0,Sigma,alpha,B,gs1,gs2,gb1,gb2);
           
hc = zeros(2*Dim,2*Dim,length(c));
           
h = zeros(2*Dim,2*Dim);lh=-inf*ones(2*Dim,2*Dim);sh=h;
           
for i = 1:2*Dim
               for j = 1:i
                   for k = 1:length(c)
                       hc(i,j,k)=-(((hm(i,j,k+1)-hm(i,j,k))*(b(k+1)-b(k))+(ga(i,k+1)-ga(i,k))*(gb(j,k+1)-gb(j,k))-(ga(j,k+1)-ga(j,k))*(gb(i,k+1)-gb(i,k))-(a(k+1)-a(k))*(hst(i,j,k+1)-hst(i,j,k)))/((b(k+1)-b(k))^2)+2*kao(i,k)*(gb(j,k+1)-gb(j,k))/(b(k+1)-b(k)));
%                       h(i,j) = h(i,j)-normcdf(-abs(c(k)))*sign(c(k))*(hc(i,j,k)*(b(k+1)-b(k))+kao(i,k)*(gb(j,k+1)-gb(j,k))+kao(j,k)*(gb(i,k+1)-gb(i,k)))+fm(abs(c(k)))*(hst(i,j,k+1)-hst(i,j,k))+normpdf(c(k))*kao(i,k)*kao(j,k)*(b(k+1)-b(k));
                       [lh(i,j),sh(i,j)] = LogSumExpSignedCoeff([lh(i,j),LogNormCDF(c(k)),LogEI(-abs(c(k))),LogNormPDF(c(k))],[sh(i,j),-sign(c(k))*(hc(i,j,k)*(b(k+1)-b(k))+kao(i,k)*(gb(j,k+1)-gb(j,k))+kao(j,k)*(gb(i,k+1)-gb(i,k))),hst(i,j,k+1)-hst(i,j,k),kao(i,k)*kao(j,k)*(b(k+1)-b(k))]);
%                       disp(h(i,j))
%                       disp([lh(i,j),sh(i,j)])
                   end
                   h(i,j) = sh(i,j)*exp(lh(i,j)-V)-g(i)*g(j);
 %                  h(i,j) = sign(h(i,j))*exp(log(abs(h(i,j)))-V)-g(i)*g(j);
               end
               if i>1
                  h(1:i-1,i) = h(i,1:i-1)';
               end
end
%           disp('hc')
%           disp(hc)

Cofx= Cofx+[0.01;0];
[MuP,SigmaP,Sigmat0]=Update_KalmanPlus_Conly_Abr(Mu,sigma,mu0,sigma0,PriorCov,PriorScale,NumSampled,invS,CofAltsSampled,Yt,Cofx); 
[V,st,~,~,c,I]=VOI(istar,A,1,MuP,SigmaP,EstSampCov1);
                       
Sigma0 = Sigmat0;
Sigma = SigmaP;
mu = MuP;

J = JforGradient(Cofx,Dim,NumSampled,CofAltsSampled,Sigma0(istar,1:NumSampled),alpha);
%AI = A(I);
gaa = PairGradient_mu(AI,Dim,istar,invS,Yt,J);
[gbb,B,gs11,gs22,gb11,gb22] = PairGradient_st(AI,Dim,NumSampled,istar,Sigma0,Sigma,invS,J,SampVar,SampCorr,Cofx,alpha,beta);
a = mu(AI); b = st(AI);
a = a(:); b = b(:); c = c(2:end-1); c=c(:);%disp(a);disp(b);disp(c);disp(ga);disp(gb)
% length(c) should be length(a) or length(b)-1
kaoo = Gradient_c(a,b,gaa,gbb);

for i = 1:length(AI)
    disp(i)
    disp([ga(:,i),gaa(:,i)])
    disp(hm(:,:,i))
    disp([gb(:,i),gbb(:,i)])
    disp(hst(:,:,i))
end
for i = 1:length(AI)-1
    disp([kao(:,i),kaoo(:,i)])
    disp(hc(:,:,i))
end

for i = 1:length(AI)
    disp(i)
    disp([gs1(:,i),gs11(:,i);gs2(:,i),gs22(:,i)])
    disp(hs(:,:,i))
end
    disp([gb1,gb11;gb2,gb22])
    disp(hb)
