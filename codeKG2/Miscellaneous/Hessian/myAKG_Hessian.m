function [OC,F,SF,...
    PriorCov,PriorScale,SampVarHat,AllAlts,AllObs,EB] = myAKG_Hessian(T,N,fun,Dim,Domain,SampVar,n1,n2,NumStarts)%,NumUpdates)
tic
N1=n1+n2;

OC = inf*ones(1,N-N1);
F = inf*ones(1,N-N1);
SF = inf*ones(NumStarts,N-N1);
EB = inf*ones(Dim,N-N1);

LD=length(Domain);
%Domain = Domain(:);
scale0=abs(Domain(end)-Domain(1))*ones(1,Dim);
options = optimset('GradObj','on','Hessian','user-supplied','Algorithm','trust-region-reflective','TolFun',1e-8);

FirstStageAlts1=Domain(randi(LD,[n1 Dim]));
if n1 == 1
    FirstStageAlts1=(FirstStageAlts1(:))';
else if Dim == 1
        FirstStageAlts1=FirstStageAlts1(:);
    end
end
% FirstStageCov1=GPCov(SampVar,SampCorrScale,FirstStageAlts1,FirstStageAlts1);
FirstStageObs1=(mvnrnd(-feval(fun,FirstStageAlts1,Dim),SampVar*eye(n1)))';%'
[~,Best]=sort(FirstStageObs1,'descend');
FirstStageAlts2=FirstStageAlts1(Best(1:n2),:);
FirstStageObs2=(mvnrnd(-feval(fun,FirstStageAlts2,Dim),eye(n2)*SampVar))';%'

FirstStageAlts=[FirstStageAlts1;FirstStageAlts2];
FirstStageObs=[FirstStageObs1;FirstStageObs2(:)];
[PriorCov,SampVarHat,PriorScale,mu0]= MLEofHyperparameters_IndepSamp(FirstStageAlts,FirstStageObs,.5,scale0,scale0);

SecondStageAlts=Domain(randi(LD,[1 Dim]));
SecondStageAlts=(SecondStageAlts(:))';
SecondStageObs=normrnd(-feval(fun,SecondStageAlts,Dim),sqrt(SampVar));
sigma0=PriorCov;

%Update = [30,60,round(exp(log(90):(log(N-N1)-log(90))/(NumUpdates-3):log(N-N1)))];
Update=[30,60,92,140,214,327,500];

for n = 1:N-N1
    if toc > T 
        break
    else
    if sum(n==Update)>0
        AllAlts=[FirstStageAlts;SecondStageAlts];
        AllObs=[FirstStageObs;SecondStageObs];
        [PriorCov,SampVarHat,PriorScale,mu0]= MLEofHyperparameters_IndepSamp(AllAlts,AllObs,.5,scale0,scale0);
        sigma0=eye(n);
        for i=2:n
            sigma0(i,1:i-1)=GPCov(1,PriorScale,SecondStageAlts(i,:),SecondStageAlts(1:i-1,:));
            sigma0(1:i-1,i)=sigma0(i,1:i-1)';%'
        end
        sigma0=sigma0*PriorCov;
    end
    
    L=eye(n)*SampVarHat;
    L=sparse(L);

    Yt=SecondStageObs-mu0;
    invS=inv(sigma0+L);

    [Mu,sigma]=Update_Kalman(mu0,sigma0,n,invS,Yt);

    x01=ChooseRandomElement(find(Mu==max(Mu)));
    CurrentBest=SecondStageAlts(x01,:);
    EB(:,n) = CurrentBest(:);
    
    Bests= sum(abs(SecondStageAlts-repmat(CurrentBest,n,1)),2)==0;
    temp=1:n;temp(Bests)=[];
    
    OC(n)=feval(fun,CurrentBest,Dim);
    
    if (sum(isnan(Mu))>0)
        Cofx=Domain(randi(LD,[1 Dim]));
        Cofx=(Cofx(:))';
    else
        if ~isempty(temp)
            if NumStarts>2
                x02=temp(ChooseRandomElement(find(Mu(temp)==max(Mu(temp)))));
                SecondBest=SecondStageAlts(x02,:);
                if sum(sum(abs(SecondStageAlts(temp,:)-repmat(SecondBest,length(temp),1)),2)==0)<length(temp)
                    tmp=Domain(randi(LD,[NumStarts-2 Dim]));
                    if NumStarts == 3
                        tmp=(tmp(:))';
                    else if Dim == 1
                            tmp=tmp(:);
                        end
                    end
                    CofX0=[CurrentBest;SecondBest;tmp];
                else
                    tmp=Domain(randi(LD,[NumStarts-1 Dim]));
                    if NumStarts == 2
                        tmp=(tmp(:))';
                    else if Dim == 1
                            tmp=tmp(:);
                        end
                    end
                    CofX0=[CurrentBest;tmp];
                end
                
            else if NumStarts == 2
                    tmp=Domain(randi(LD,[NumStarts-1 Dim]));
                    tmp=(tmp(:))';
                    CofX0=[CurrentBest;tmp];
                else
                    CofX0=Domain(randi(LD,[1 Dim]));CofX0 = (CofX0(:))';
                end
            end
        else
            CofX0=Domain(randi(LD,[NumStarts Dim]));
            if NumStarts == 1
                CofX0=(CofX0(:))';
            else if Dim == 1
                    CofX0=CofX0(:);
                end
            end
        end
        
        for start=1:NumStarts
            SF(start,n)=-VOI_TestSingular_Conly(CofX0(start,:),n,Mu,sigma,mu0,sigma0,PriorCov,PriorScale,SampVarHat,invS,SecondStageAlts,Yt);
        end
        
        CofX=zeros(NumStarts,Dim);
        Fval=zeros(NumStarts,1);
    
        for start=1:NumStarts
            CofX(start,:) = fmincon(@(cc)VOI_TestSingular_Conly(cc,n,Mu,sigma,mu0,sigma0,PriorCov,PriorScale,SampVarHat,invS,SecondStageAlts,Yt),CofX0(start,:),[],[],[],[],Domain(ones(1,Dim)),Domain(end)*ones(1,Dim),[],options);%,'Display','Iter'));
            if (sum(isnan(CofX(start,:)))>0)
                CofX(start,:)=CofX0(start,:);
                Fval(start)=-SF(start,n);
            else
                CofXD=max(min(Domain(end),CofX(start,:)),Domain(1));
                [~,Index]=min(abs(bsxfun(@minus,CofXD,Domain(:))));
                CofXD=Domain(Index);
                CofX(start,:)=(CofXD(:))';
                Fval(start) = VOI_TestSingular_Conly(CofX(start,:),n,Mu,sigma,mu0,sigma0,PriorCov,PriorScale,SampVarHat,invS,SecondStageAlts,Yt);
            end
        end
        [F(n),IS]=min(Fval);F(n)=-F(n);
        Cofx=CofX(IS,:);
    end
        disp(n)
        AlreadySampled=find(sum(abs(SecondStageAlts-repmat(Cofx,n,1)),2)==0);

        %SF(n) = VOIStar_Singular(Cofx,n,Mu,sigma,mu0,sigma0,PriorCov,PriorScale,SampVarHat,invS,SecondStageAlts,Yt,1000,SearchLength);
        y=normrnd(-feval(fun,Cofx,Dim),sqrt(SampVar));
        
        if (~isempty(AlreadySampled))
            sigma0=[[sigma0,sigma0(:,AlreadySampled(1))];[sigma0(AlreadySampled(1),:),PriorCov]];
        else
            RU=GPCov(PriorCov,PriorScale,SecondStageAlts,Cofx);
            sigma0=[[sigma0,RU];[RU',PriorCov]];
        end
        
        SecondStageAlts=[SecondStageAlts;Cofx];
        SecondStageObs=[SecondStageObs;y];
    end
end