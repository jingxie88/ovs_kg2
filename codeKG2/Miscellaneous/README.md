# This is my README

Files for the Assemble to Order problem at SimOpt.org.

"AssembleOrder.m": matlab code from SimOpt.org.

"oldscript_ATO": KG/KG2 algorithms for ATO (earlier version of "AKG.m" and "AKG2.m" in the main directory).

"codegen_ATO": converted C code of the problem (required by COMPASS).

"compass_ATO": implementation result of COMPASS on ATO.

"Hong_codeATO": Hong's matlab code for a slightly different ATO problem.


