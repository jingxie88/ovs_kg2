clf
Dim = 1;
Domain = -5:.1:4.9;
LD=length(Domain);
k=LD;
SampVar=50;
SampCorr = 0.5;
PriorCov=100;PriorScale=.5;
Lambda=SampVar*(SampCorr*ones(k,k)+(1-SampCorr)*eye(k));

Alts=Domain';
Sigma0=GPCov(PriorCov,PriorScale,Alts,Alts);
theta=(mvnrnd(zeros(k,1),Sigma0))';
mu0=zeros(k,1);
beta=1;
N=100;

[~,TB] = max(theta);

clf
Sigma=Sigma0;
mu=mu0;
Alts=[];Obs=[];
for n=1:500%N
    Best=max(mu);
    Best=ChooseRandomElement(find(mu==Best));

    gamma=KGFactor(k,mu,Sigma,Lambda,beta);
    g=find(gamma==max(max(gamma)));
        
    i=ChooseRandomElement(g);
    x=mod(i,k);
    if (x==0)
        x=k;
    end
    xx=ceil(i/k);
         
    gamma=tril(gamma)+tril(gamma,-1)';

    subplot(1,2,2)
    %lb = min(min(gamma));
    %ub = max(max(gamma));
%     disp(ub)
%     if ub<0
%         faint=log(-ub):(log(-lb)-log(-ub))/9:log(-lb);
%     contourf(1:k,1:k,gamma,-exp(faint(end:-1:1)))
%     else if lb<0
%          faint=0:log(-lb)/8:log(-lb);           
%         contourf(1:k,1:k,gamma,[-exp(faint(end:-1:1)),ub])
%         else
%           faint=log(lb):(log(ub)-log(lb))/9:log(ub);          
%         contourf(1:k,1:k,gamma,exp(faint))
%         end
%     end
    %surfc(1:k,1:k,gamma)
   % contourf(1:k,1:k,max(gamma,gl))

    %contour(1:k,1:k,gamma)
    %surf(1:k,1:k,gamma)
    %surf(0:9,0:9,gamma(1:10,1:10))
   %axis([1 10 1 10])
    %jl=ub-lb;
    %gl=lb+(ub-lb)*.99;
    gl=sort(gamma(:));
    pcolor(1:k,1:k,max(gamma,gl(500)))
  
%    %meshc(1:k,1:k,gamma)
%    pc=pcolor(1:k,1:k,gamma);
%    set(pc,'Cdata',gamma)%[lb,gl:(ub-gl)/9,ub])
colorbar('location','southoutside')
    axis([1 k 1 k])
    hold on     
plot([x,xx],min(99.9,[xx,x]),'o','MarkerEdgeColor','k',...
                 'MarkerFaceColor','k','MarkerSize',10)
     %grid on

%surface(1:k,1:k,gamma,'EdgeColor',[.8 .8 .8],'FaceColor','none')
    %axis([1 k 1 k -10 5])
    %view(-15,25)
%colormap cool

%    colormap ([1  1  0; 0  1  1])

%     if x == Best
%         plot(x,theta(x),'oy','MarkerEdgeColor','k',...
%                 'MarkerFaceColor','y','MarkerSize',10)
%             if xx ~= x
%                 plot(xx,theta(xx),'og','MarkerEdgeColor','k',...
%         'MarkerFaceColor','g','MarkerSize',10)
%             end
%     else
%         plot(x,theta(x),'og','MarkerEdgeColor','k',...
%         'MarkerFaceColor','g','MarkerSize',10)
%         if xx == Best
%             plot(xx,theta(xx),'oy','MarkerEdgeColor','k',...
%         'MarkerFaceColor','y','MarkerSize',10)
%         else
%             plot(xx,theta(xx),'og','MarkerEdgeColor','k',...
%         'MarkerFaceColor','g','MarkerSize',10)
%         end
%     end
    
%     subplot(1,2,2)
%     plot3(repmat(1:k,1,k),reshape(repmat(1:k,k,1),1,k^2),gamma(:),'o',...
%         'MarkerEdgeColor','m','MarkerFaceColor','m','MarkerSize',2)
%     hold on
%     plot3(x,xx,gamma(x,xx),'og','MarkerEdgeColor','k',...
%                 'MarkerFaceColor','g','MarkerSize',10)
%     plot3(xx,x,gamma(xx,x),'og','MarkerEdgeColor','k',...
%                 'MarkerFaceColor','g','MarkerSize',10)   
%             
%     axis([0 k 0 k median(gamma(:))-2*(max(gamma(:))-median(gamma(:))) median(gamma(:))+1.5*(max(gamma(:))-median(gamma(:)))])
%     grid on
%     grid(gca,'minor')
%     %set(gca,'XGrid','on','YGrid','on','ZGrid','on')
    xlabel('x^{(1)}','fontsize',20)
    ylabel('x^{(2)}','fontsize',20)    
    title('log [ v^{KG}_{n-1} ( x^{(1)}, x^{(2)} ) ]','fontsize',20,'fontweight','b')

    if(xx==x)
    y=normrnd(theta(x),sqrt(Lambda(x,x)));
    
    subplot(1,2,1)    
    hold on
    if n>1
    f0=plot(Alts,Obs,'o','MarkerEdgeColor',[0.95 0.95 0.95],...
                 'MarkerFaceColor',[0.5 0.5 0.5],'MarkerSize',5);
    end
    f1=plot(x,max(min(y,25),-25),'o','MarkerEdgeColor','k',...
                 'MarkerFaceColor','k','MarkerSize',10);
    
    f2=plot(1:k,theta,'color','r','LineWidth',2);
hold on
%line([TB TB],[-20 theta(TB)],'color',[.5 .0 0],'LineStyle','--')
f3=plot(1:k,mu,'color',[0 .5 1],'LineWidth',2);
f4=plot(1:k,mu-1.96*sqrt(diag(Sigma)),'--','color',[0 .5 1],'LineWidth',1);
plot(1:k,mu+1.96*sqrt(diag(Sigma)),'--','color',[0 .5 1],'LineWidth',1)

    plot(TB,theta(TB),'o','MarkerEdgeColor','r',...
                'MarkerFaceColor','r','MarkerSize',10)
    plot(Best,mu(Best),'o','MarkerEdgeColor',[0 .5 1],...
                'MarkerFaceColor',[0 .5 1],'MarkerSize',10)

%line([Best Best],[-20 mu(Best)],'color',[0 0 0.25],'LineStyle','--')%'or','MarkerEdgeColor','k','MarkerFaceColor','r','MarkerSize',10)
xlabel('x','fontsize',20)
title(sprintf('n = %d',n),'fontsize',20,'fontweight','b')
    axis([0 100 -25 25])    
if n>1
love=legend([f2,f3,f4,f1,f0],' \theta',' \mu_{n-1}',' \mu_{n-1} \pm 1.96\sigma_{n-1}',' (x_{n} , y_{n})',' \{ (X_i , Y_i) \}_{i<n}','Location','SouthEast');
else
    love=legend([f2,f3,f4,f1],' \theta',' \mu_{n-1}',' \mu_{n-1} \pm 1.96\sigma_{n-1}',' (x_{n} , y_{n})','Location','SouthEast');
end
set(love,'Fontsize',15)
    Alts = [Alts,x];
    Obs = [Obs,y];

    [mu,Sigma]=Update(k,mu,Sigma,Lambda,x,beta,y);
    else
        Y=(mvnrnd([theta(xx),theta(x)]',[Lambda(xx,xx),Lambda(xx,x);Lambda(x,xx),Lambda(x,x)]))';
        X=zeros(2,k);
        X(1,xx)=1;
        X(2,x)=1;
    
    subplot(1,2,1)    
    hold on
    if n>1
    f0=plot(Alts,Obs,'o','MarkerEdgeColor',[0.95 0.95 0.95],...
                 'MarkerFaceColor',[0.5 0.5 0.5],'MarkerSize',5);
    end
    f1=plot([xx;x],max(min(Y,25),-25),'o','MarkerEdgeColor','k',...
                 'MarkerFaceColor','k','MarkerSize',10);
    f2=plot(1:k,theta,'color','r','LineWidth',2);
hold on
%line([TB TB],[-20 theta(TB)],'color',[.5 .0 0],'LineStyle','--')
f3=plot(1:k,mu,'color',[0 .5 1],'LineWidth',2);
f4=plot(1:k,mu-1.96*sqrt(diag(Sigma)),'--','color',[0 .5 1],'LineWidth',1);
plot(1:k,mu+1.96*sqrt(diag(Sigma)),'--','color',[0 .5 1],'LineWidth',1)

    plot(TB,theta(TB),'o','MarkerEdgeColor','r',...
                'MarkerFaceColor','r','MarkerSize',10)
    plot(Best,mu(Best),'o','MarkerEdgeColor',[0 .5 1],...
                'MarkerFaceColor',[0 .5 1],'MarkerSize',10)

%line([Best Best],[-20 mu(Best)],'color',[0 0 0.25],'LineStyle','--')%'or','MarkerEdgeColor','k','MarkerFaceColor','r','MarkerSize',10)
xlabel('x','fontsize',20)
title(sprintf('n = %d',n),'fontsize',20,'fontweight','b')
axis([0 100 -25 25])    
if n>1
love=legend([f2,f3,f4,f1,f0],' \theta',' \mu_{n-1}',' \mu_{n-1} \pm 1.96\sigma_{n-1}',' (X_{n} , Y_{n})',' \{ (X_i , Y_i) \}_{i<n}','Location','SouthEast');
else
    love=legend([f2,f3,f4,f1],' \theta',' \mu_{n-1}',' \mu_{n-1} \pm 1.96\sigma_{n-1}',' (X_{n} , Y_{n})','Location','SouthEast');
end
set(love,'Fontsize',15)
        [mu,Sigma]=Update(k,mu,Sigma,Lambda,X,beta,Y);
    Alts = [Alts,xx,x];
    Obs = [Obs,Y'];
    

    end
fp = fillPage(gcf, 'margins', [0 0 0 0], 'papersize', [20 10]);
    print(gcf, '-dpdf', '-r300', sprintf('test%d.pdf',n-1))
    
    clf
    disp(n)
end


clf
Sigma=Sigma0;
mu=mu0;
Alts=[];
Obs=[];

% subplot(1,2,1)
% plot(1:k,theta,'color','r','LineWidth',2)
% hold on
% plot(1:k,mu,'color',[0 .5 1],'LineWidth',2)
% plot(1:k,mu-1.96*sqrt(diag(Sigma)),'--','color',[0 .5 1],'LineWidth',1)
% plot(1:k,mu+1.96*sqrt(diag(Sigma)),'--','color',[0 .5 1],'LineWidth',1)
% xlabel('x','fontsize',20)
% title('\theta;  \mu_0, \Sigma_0','fontsize',20,'fontweight','b')
    [mu,Sigma]=Update(k,mu,Sigma,Lambda,x,beta,y);
     Alts = [Alts,x];
    Obs = [Obs,y];

for n=3:500%00%N
    gamma=IKGFactor(k,mu,Sigma,Lambda,beta);
    g=find(gamma==max(max(gamma)));
    x=ChooseRandomElement(g);
%     if x == Best
%             plot(x,theta(x),'oy','MarkerEdgeColor','k',...
%                 'MarkerFaceColor','y','MarkerSize',10)
%     else
%     plot(x,theta(x),'og','MarkerEdgeColor','k',...
%                 'MarkerFaceColor','g','MarkerSize',10)
%     end
    
    
    subplot(1,2,2)
    %semilogy(1:k,gamma,'g','LineWidth',2,'color','k')
    plot(1:k,gamma,'g','LineWidth',2,'color','k')
    hold on
    plot(x,gamma(x),'o','MarkerEdgeColor','k',...
                'MarkerFaceColor','k','MarkerSize',10)
    %grid on
    xlabel('x','fontsize',20)    
    title('log [ v^{KG}_{n-1} ( x ) ]','fontsize',20,'fontweight','b')
    %title('log [ v^{KG}_2 ( x ) / c ( x ) ]','fontsize',20,'fontweight','b')
    axis([0 100 -60 5])
    
    y=normrnd(theta(x),sqrt(Lambda(x,x)));
    Best=max(mu);
    Best=ChooseRandomElement(find(mu==Best));
    
    subplot(1,2,1)    
    hold on
    f0=plot(Alts,Obs,'o','MarkerEdgeColor',[0.95 0.95 0.95],...
                 'MarkerFaceColor',[0.5 0.5 0.5],'MarkerSize',5);
    f1=plot(x,min(max(y,-25),25),'o','MarkerEdgeColor','k',...
                 'MarkerFaceColor','k','MarkerSize',10);
f2=plot(1:k,theta,'color','r','LineWidth',2);
%line([TB TB],[-20 theta(TB)],'color',[.5 .0 0],'LineStyle','--')
f3=plot(1:k,mu,'color',[0 .5 1],'LineWidth',2);
f4=plot(1:k,mu-1.96*sqrt(diag(Sigma)),'--','color',[0 .5 1],'LineWidth',1);
                 
plot(1:k,mu+1.96*sqrt(diag(Sigma)),'--','color',[0 .5 1],'LineWidth',1)

    plot(TB,theta(TB),'o','MarkerEdgeColor','r',...
                'MarkerFaceColor','r','MarkerSize',10)
    plot(Best,mu(Best),'o','MarkerEdgeColor',[0 .5 1],...
                'MarkerFaceColor',[0 .5 1],'MarkerSize',10)

%line([Best Best],[-20 mu(Best)],'color',[0 0 0.25],'LineStyle','--')%'or','MarkerEdgeColor','k','MarkerFaceColor','r','MarkerSize',10)
xlabel('x','fontsize',20)
title(sprintf('n = %d',n),'fontsize',20,'fontweight','b')
    axis([0 100 -25 25])
love=legend([f2,f3,f4,f1,f0],' \theta',' \mu_{n-1}',' \mu_{n-1} \pm 1.96\sigma_{n-1}',' (x_{n} , y_{n})',' \{ (x_i , y_i) \}_{i<n}','Location','SouthEast');
%love=legend([f2,f3,f4,f0],' \theta',' \mu_{1}',' \mu_{1} \pm 1.96\sigma_{1}',' (x_{1} , y_{1})','Location','SouthEast');
%love=legend([f2,f3,f4,f1,f0],' \theta',' \mu_{1}',' \mu_{1} \pm 1.96\sigma_{1}',' (x_{2} , y_{2})',' (x_{1} , y_{1})','Location','SouthEast');

set(love,'Fontsize',15)
fp = fillPage(gcf, 'margins', [0 0 0 0], 'papersize', [15 10]);
    print(gcf, '-dpdf', '-r300', sprintf('testS%d.pdf',n-1))
%print(gcf, '-dpdf', '-r300', sprintf('testS0%d.pdf',n-1))
        
    clf   
    Alts = [Alts,x];
Obs = [Obs,y];

    [mu,Sigma]=Update(k,mu,Sigma,Lambda,x,beta,y);
    disp(n)
end

clf
Dim = 1;
Domain = -5:.1:4.9;
LD=length(Domain);
k=LD;
SampVar=100;
SampCorr = 0.5;
PriorCov=50;PriorScale=.5;
Lambda=SampVar*(SampCorr*ones(k,k)+(1-SampCorr)*eye(k));

Alts=Domain';
Sigma0=GPCov(PriorCov,PriorScale,Alts,Alts);
theta=(mvnrnd(zeros(k,1),Sigma0))';
mu0=zeros(k,1);
beta=1;
N=100;

[~,TB] = max(theta);

clf
Sigma=Sigma0;
mu=mu0;
Alts=[];
Obs=[];

subplot(1,2,1)
plot(1:k,theta,'color','r','LineWidth',2)
hold on
%line([TB TB],[-20 theta(TB)],'color',[.5 .0 0],'LineStyle','--')

plot(1:k,mu,'color',[0 .5 1],'LineWidth',2)
plot(1:k,mu-1.96*sqrt(diag(Sigma)),'--','color',[0 .5 1],'LineWidth',1)
plot(1:k,mu+1.96*sqrt(diag(Sigma)),'--','color',[0 .5 1],'LineWidth',1)
    axis([0 100 -25 25])    
    xlabel('x','fontsize',20)
    %title('t = 0','fontsize',20,'fontweight','b')
love=legend(' \theta',' \mu_0',' \mu_0 \pm 1.96\sigma_0','Location','SouthEast');
set(love,'Fontsize',15)
fp = fillPage(gcf, 'margins', [0 0 0 0], 'papersize', [15 10]);
    print(gcf, '-dpdf', '-r300', 'testSI.pdf')

clf
x=randi(k);
    y=normrnd(theta(x),sqrt(SampVar));

subplot(1,2,1)
f1=plot(x,y,'o','MarkerEdgeColor','k',...
                 'MarkerFaceColor','k','MarkerSize',10);
hold on

f2=plot(1:k,theta,'color','r','LineWidth',2);
%line([TB TB],[-20 theta(TB)],'color',[.5 .0 0],'LineStyle','--')
f3=plot(1:k,mu,'color',[0 .5 1],'LineWidth',2);
f4=plot(1:k,mu-1.96*sqrt(diag(Sigma)),'--','color',[0 .5 1],'LineWidth',1);
plot(1:k,mu+1.96*sqrt(diag(Sigma)),'--','color',[0 .5 1],'LineWidth',1)
    axis([0 100 -25 25])    
    xlabel('x','fontsize',20)
    title('n = 1','fontsize',20,'fontweight','b')
love=legend([f2,f3,f4,f1],' \theta',' \mu_0',' \mu_0 \pm 1.96\sigma_0',' (x_1 , y_1)','Location','SouthEast');
set(love,'Fontsize',15)
fp = fillPage(gcf, 'margins', [0 0 0 0], 'papersize', [15 10]);
    print(gcf, '-dpdf', '-r300', 'testS0.pdf')
    
    [mu,Sigma]=Update(k,mu,Sigma,Lambda,x,beta,y);
     Alts = [Alts,x];
    Obs = [Obs,y];
    clf
n=2;
    gamma=IKGFactor(k,mu,Sigma,Lambda,beta);
    g=find(gamma==max(max(gamma)));
    x=ChooseRandomElement(g);
%     if x == Best
%             plot(x,theta(x),'oy','MarkerEdgeColor','k',...
%                 'MarkerFaceColor','y','MarkerSize',10)
%     else
%     plot(x,theta(x),'og','MarkerEdgeColor','k',...
%                 'MarkerFaceColor','g','MarkerSize',10)
%     end
    
    
    subplot(1,2,2)
    %semilogy(1:k,gamma,'g','LineWidth',2,'color','k')
    plot(1:k,gamma,'g','LineWidth',2,'color','k')
    hold on
    plot(x,gamma(x),'o','MarkerEdgeColor','k',...
                'MarkerFaceColor','k','MarkerSize',10)
    %grid on
    xlabel('x','fontsize',20)    
    title('log [ v^{KG}_1 ( x ) ]','fontsize',20,'fontweight','b')
    axis([0 100 -60 5])
    
    y=normrnd(theta(x),sqrt(Lambda(x,x)));
    Best=max(mu);
    Best=ChooseRandomElement(find(mu==Best));
    
    subplot(1,2,1)    
    hold on
    f0=plot(Alts,Obs,'o','MarkerEdgeColor',[0.95 0.95 0.95],...
                 'MarkerFaceColor',[0.5 0.5 0.5],'MarkerSize',5);
f2=plot(1:k,theta,'color','r','LineWidth',2);
%line([TB TB],[-20 theta(TB)],'color',[.5 .0 0],'LineStyle','--')
f3=plot(1:k,mu,'color',[0 .5 1],'LineWidth',2);
f4=plot(1:k,mu-1.96*sqrt(diag(Sigma)),'--','color',[0 .5 1],'LineWidth',1);
                 
plot(1:k,mu+1.96*sqrt(diag(Sigma)),'--','color',[0 .5 1],'LineWidth',1)

    plot(TB,theta(TB),'o','MarkerEdgeColor','r',...
                'MarkerFaceColor','r','MarkerSize',10)
    plot(Best,mu(Best),'o','MarkerEdgeColor',[0 .5 1],...
                'MarkerFaceColor',[0 .5 1],'MarkerSize',10)

%line([Best Best],[-20 mu(Best)],'color',[0 0 0.25],'LineStyle','--')%'or','MarkerEdgeColor','k','MarkerFaceColor','r','MarkerSize',10)
xlabel('x','fontsize',20)
title(sprintf('n = %d',n-1),'fontsize',20,'fontweight','b')
    axis([0 100 -25 25])
love=legend([f2,f3,f4,f0],' \theta',' \mu_{1}',' \mu_{1} \pm 1.96\sigma_{1}',' (x_{1} , y_{1})','Location','SouthEast');
set(love,'Fontsize',15)
fp = fillPage(gcf, 'margins', [0 0 0 0], 'papersize', [15 10]);
print(gcf, '-dpdf', '-r300', sprintf('testS0%d.pdf',n-1))
    
subplot(1,2,1)    

hold on
    f1=plot(x,min(max(y,-25),25),'o','MarkerEdgeColor','k',...
                 'MarkerFaceColor','k','MarkerSize',10);

love=legend([f2,f3,f4,f1,f0],' \theta',' \mu_{1}',' \mu_{1} \pm 1.96\sigma_{1}',' (x_{2} , y_{2})',' (x_{1} , y_{1})','Location','SouthEast');
title(sprintf('n = %d',n),'fontsize',20,'fontweight','b')

set(love,'Fontsize',15)
fp = fillPage(gcf, 'margins', [0 0 0 0], 'papersize', [15 10]);
    print(gcf, '-dpdf', '-r300', sprintf('testS%d.pdf',n-1))
        
    clf   
    Alts = [Alts,x];
Obs = [Obs,y];

    [mu,Sigma]=Update(k,mu,Sigma,Lambda,x,beta,y);
    disp(n)
   
