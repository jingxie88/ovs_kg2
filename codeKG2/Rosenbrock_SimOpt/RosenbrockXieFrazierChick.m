% This is an implementation of an alternate version of Rosenbrock's function,
% used in our paper.  The function itself is a little bit different, and it
% also supports correlation in the noise using CRN.
%
% Inputs
% x is a vector at which we wish to evaluate the function.
% runlength is ignored.
% seed is the index of the substreams to use (integer >= 1)
% other is a structure containing the following fields:
%   other.stream is an additional random number substream, which is used to implement correlated noise between two points.  This argument is not optional.
%   other.q is a parameter of the Rosenbrock Function.  The dimension of the problem is q.  (Compare this to the other variant, for which the dimension is 2q.)  This argument is optional.
%   other.noise_variance is the variance of the noise.  This argument is optional.
%   other.noise_correlation is the correlation in the noise between two different points, with the same seed.  This argument is optional.
%
% Outputs:
% fn is the observed function value.
% All other outputs are set to NaN.


function [fn, FnVar, FnGrad, FnGradCov, constraint, ConstraintCov, ConstraintGrad, ConstraintGradCov] = RosenbrockXieFrazierChick(x, runlength, seed, other);

constraint = NaN;
ConstraintCov = NaN;
ConstraintGrad = NaN;
ConstraintGradCov = NaN;
FnVar = NaN;
FnGrad = NaN;
FnGradCov = NaN;


% Check arguments and fill in default values.

if ~isfield(other,'stream')
    error('The argument "other" should be a structure containing, at least, the field "stream".');
end

if ~isfield(other,'q')
    other.q = 6; % Value used in the paper
end

if ~isfield(other,'noise_variance')
    other.noise_variance = 125; % Value used in the paper
end

if ~isfield(other,'noise_correlation')
    other.noise_correlation = 0.4; % Value used in the paper
end

if (runlength <= 0) || (runlength ~= round(runlength)) || (seed <= 0) || (round(seed) ~= seed)
    error('runlength should be positive integer, seed must be a positive integer\n');
end

if (other.q <= 0) || (other.q ~= round(other.q))
    error('other.q must be a positive integer');
end

if (other.noise_variance)<0
    error('other.noise_variance must be non-negative.')
end

if other.noise_correlation>1 || other.noise_correlation<0
    error('other.noise_correlation must be between 0 and 1.')
end

% Generate a new random number stream, to use for generating the part of the
% noise common across alternatives, and set this stream to start with the
% passed seed.
OurStream = RandStream.create('mrg32k3a', 'NumStreams', 1);
OurStream.Substream = seed;

% Set the global stream to our stream, and save the old stream.
OldStream = RandStream.setGlobalStream(OurStream);

% Generate a normal random variable from this common stream.
CommonNoise=normrnd(0,1,1);

% Now generate another normal random variable from the "other" stream.
% This will generate independent random numbers.
RandStream.setGlobalStream(other.stream);
IndependentNoise=normrnd(0,1,1);

%Restore the old stream
RandStream.setGlobalStream(OldStream); 

% Create noise that has variance noisevar, and correlation rho.
% We require that a^2 + b^2 = noise_variance,
% and that noise_correlation = a^2/noise_variance.
a = sqrt(other.noise_correlation * other.noise_variance);
b = sqrt(other.noise_variance - a^2);
noise = a*CommonNoise + b*IndependentNoise;

fn = RosenbrockXieFrazierChickExact(x,other.q) + noise;
