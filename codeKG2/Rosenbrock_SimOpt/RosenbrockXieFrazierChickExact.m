% Calculates the exact value of the variant of the Rosenbrock function used in
% Xie, Chick, Frazier. The input x should be a q-dimensional vector. In Xie,
% Chick, Frazier, q was 6.
function y = RosenbrockXieFrazierChickExact(x,q)

if length(x)~=q
    error(sprintf('Input x=%s should have length q=%d',mat2str(x),q));
end

y=0;
for j=1:(q-1)
  y=y+100*(x(j)^2-x(j+1))^2+(x(j)-1)^2;
end
