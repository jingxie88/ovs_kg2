% A unit test for RosenbrockXieFrazierChick.m and RosenbrockXieFrazierChickExact.m
% which is an optional argument, and is a length-4 binary vector that tells
% which tests to run.  If no argument is passed, all tests are run.

function TestRosenbrockXieFrazierChick(which)
  if (nargin==0)
    which = [1 1 1 1];
  end 
  if (which(1))
    test1();
  end
  if (which(2))
    test2();
  end
  if (which(3))
    test3();
  end
  if (which(4))
    test4();
  end
end


% Test the mean value
function status = test1()
  stream = RandStream.create('mrg32k3a', 'NumStreams', 42);
  other = struct('stream',stream,'q',6);
  x = rand(1,6);
  n = 1000;
  y = zeros(1,n);
  for i=1:n
      y(i) = RosenbrockXieFrazierChick(x,1,i,other);
  end
  m = mean(y);
  s = std(y)/sqrt(n);
  exact = RosenbrockXieFrazierChickExact(x,other.q);
  z = (m-exact)/s;
  if (abs(z)>3)
      disp(sprintf('Mean value %.2f, Stderr=%.2f, Exact value=%.2f, Z statistics=%.2f',m,s,exact,z));
      disp('abs(Z statistic)>3.');
      disp('Test1 failed.  FAIL.')
      status = 0;
  else
      disp('Test1 passed.  OK.')
      status = 1;
  end
end


% Test the variance
function status = test2()
  stream = RandStream.create('mrg32k3a', 'NumStreams', 42);
  other = struct('stream',stream,'q',6, 'noise_variance', 100);
  x = rand(1,6);
  n = 10000;
  y = zeros(1,n);
  for i=1:n
      y(i) = RosenbrockXieFrazierChick(x,1,i,other);
  end
  v = var(y);
  if (abs(v-other.noise_variance)>10)
       disp(sprintf('Sample variance %g, Exact variance %g',v,other.noise_variance))
      disp('Test2 failed.  FAIL.')
      status = 0;
  else
      disp('Test2 passed.  OK.')
      status = 1;
  end
end


% Test the correlation
function status = test3()
  stream = RandStream.create('mrg32k3a', 'NumStreams', 42);
  other = struct('stream',stream,'q',6, 'noise_correlation', .4);
  n = 10000;
  y1 = zeros(1,n);
  y2 = zeros(1,n);
  x1 = rand(1,6);
  x2 = rand(1,6);
  for i=1:n
      % Sample many pairs of points, each with the same seed.
      y1(i) = RosenbrockXieFrazierChick(x1,1,i,other);
      y2(i) = RosenbrockXieFrazierChick(x2,1,i,other);
  end

  % Calculate the correlation between these pairs.  It should equal other.noise_correlation.
  corr_matrix = corrcoef(y1,y2)
  % Take the off-diagonal term of the correlation matrix, which is the
  % correlation between y1 and y2.
  corr = corr_matrix(1,2);

  if (abs(corr-other.noise_correlation)>.1)
      disp(sprintf('Sample correlation %g, Exact correlation %g',corr,other.noise_correlation))
      disp('Test3 failed.  FAIL.')
      status = 0;
  else
      disp('Test3 passed.  OK.')
      status = 1;
  end
end

function status = test4()
  valuerange = -.8:.3:1.9;
  % Get the optimum value of RosenbrockXieFrazierChickExact, with and without
  % using brute force, and compare them.
  minimize = 1;
  [best_y_nobruteforce,best_x_nobruteforce] = RosenbrockXieFrazierChickOpt(valuerange,6,minimize,0);
  [best_y_bruteforce,best_x_bruteforce] = RosenbrockXieFrazierChickOpt(valuerange,6,minimize,1);
  if (all(best_x_nobruteforce == best_x_nobruteforce) && best_y_bruteforce==best_y_nobruteforce)
      disp('Test4 passed.  OK.')
      status = 1;
  else
      disp('Brute-force minimum of RosenbrockXieFrazierChick')
      disp(sprintf('solution = %s, value = %f', mat2str(best_x_bruteforce), best_y_bruteforce));
      disp('Exact minimum of RosenbrockXieFrazierChick')
      disp(sprintf('solution = %s, value = %f', mat2str(best_x_nobruteforce), best_y_nobruteforce));
      disp('Test4 failed.  FAIL.')
      status = 0;
  end
end
