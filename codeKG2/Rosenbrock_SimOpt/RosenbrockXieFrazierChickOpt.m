% Calculates the optimum value of RosenbrockXieChickFrazier.
% valuerange is the set of values to test in each dimension, so the search
% space is valuerange^q.  In much of the code, it is set to the vector
% [-0.8,-0.5,...,1.9] written in matlab as -.8:.3:1.9; This is hard-coded to
% q=6 --- if you pass a value of q other than 6, there is an error.  In some
% cases, this function computes the optimum analytically.  If you want to force
% a brute-force calculation, set the optional argument use_bruteforce to 1.
% This code can minimize or maximize.
function [best_y,best_x] = RosenbrockXieFrazierChickOpt(valuerange,q,minimize,use_bruteforce)
if (nargin<4)
  use_bruteforce=0;
end
if (q~=6)
  error('RosenbrockXieFrazierChickOpt only allows computing for q=6.');
end

% For q>= 3, the global minimum of RosenbrockXieFrazierChick is at the point [1,..,1].
% If valuerange includes 1, and we are minimizing, then this is the answer.
% If valuerange doesn't include 1, or we are maximizing, then we have to use brute force.
if (~use_bruteforce && minimize && ismember(1,valuerange))
  best_x = ones(1,q);
  best_y = 0;
  return;
end

% Set up our objective function f so that maximizing f corresponds to doing the
% desired optimization on RosenbrockXieFrazierChickExact (minimize or
% maximize). 
if (minimize)
  f = @(x) -1*RosenbrockXieFrazierChickExact(x,q);
else
  f = @(x) RosenbrockXieFrazierChickExact(x,q);
end

best_x=valuerange(ones(1,q));
best_y=f(best_x);
for i1=1:length(valuerange)
  for i2=1:length(valuerange)
    for i3=1:length(valuerange)
      for i4=1:length(valuerange)
        for i5=1:length(valuerange)
          for i6=1:length(valuerange)
	    x=valuerange([i1 i2 i3 i4 i5 i6]);
	    y=f(x);
            if (y>best_y)
              best_x = x;
	      best_y = y;
	      disp(sprintf('RosenbrockXieFrazierChickOpt: New best of %f at %s', best_y, mat2str(best_x)));
	    end
          end
        end
       end
     end
  end
end
