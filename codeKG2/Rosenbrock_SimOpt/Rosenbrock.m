
%   ***************************************
%   *** Code written by German Gutierrez***
%   ***         gg92@cornell.edu        ***
%   ***                                 ***
%   *** Updated by Shane Henderson to   ***
%   *** use standard calling and random ***
%   *** number streams.                 ***
%   *** Updated by Peter Frazier with a ***
%   *** few bugfixes.                   ***
%   ***************************************

% Last updated July 30, 2014

%RETURNS: Average value of G_m encountered throughout the simulation.


function [fn, FnVar, FnGrad, FnGradCov, constraint, ConstraintCov, ConstraintGrad, ConstraintGradCov] = Rosenbrock(x, runlength, seed, other);
%function [fn, FnVar, FnGrad, FnGradCov, constraint, ConstraintCov,ConstraintGrad, ConstraintGradCov] = Rosenbrock(x, runlength, seed, other);
% runlength is the number of trials to simulate
% seed is the index of the substreams to use (integer >= 1)
% other represents q, where the dimension of the problem is 2q

constraint = NaN;
ConstraintCov = NaN;
ConstraintGrad = NaN;
ConstraintGradCov = NaN;

if (runlength <= 0) || (runlength ~= round(runlength)) || (seed <= 0) || (round(seed) ~= seed),
    fprintf('runlength should be positive integer, seed must be a positive integer\n');
    fn = NaN;
    FnVar = NaN;
    FnGrad = NaN;
    FnGradCov = NaN;
    
else
    
    q=other;
    m=500;
    nTrials=runlength;
    values=zeros(nTrials,1);

    % Generate a new stream for random numbers
    [OurStream1, OurStream2] = RandStream.create('mrg32k3a', 'NumStreams', 2);

    % Set the substream to the "seed"
    OurStream1.Substream = seed;
    OurStream2.Substream = seed;

    % Generate x's
    OldStream = RandStream.setGlobalStream(OurStream1);
    
    %Generate error terms for G_m(x)
    RandStream.setGlobalStream(OurStream2)
    norm=normrnd(0,1,1,nTrials);
    
    RandStream.setGlobalStream(OldStream); %Restore old stream

for i=1:nTrials
    gx=0;
    for j=1:2:2*q-1
        gx=gx+((1-x(j))^2)+(100*((x(j+1)-(x(j)^2))^2));
    end
    G_m = gx+sqrt((1+gx)/m)*norm(i);
    values(i)=G_m;
end

fn=mean(values);
FnVar = NaN;
FnGrad = NaN;
FnGradCov = NaN;
end
end
