  function fn = RosenbrockExact(x,q)
    fn=0;
    for j=1:2:2*q-1
        fn=fn+((1-x(j))^2)+(100*((x(j+1)-(x(j)^2))^2));
    end
  end
