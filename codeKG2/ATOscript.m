% This script runs the KG and KG2 algorithms on the ATO (assemble to order)
% problem.  

% PF: this script has not been updated to reflect changes to the interface of AKG2.

% When you run this script, make sure that the directories containing the AKG,
% AKG2, AssembleOrder, and associated functions called by these functions, are
% included in the path.  For example, if you are in the directory containing
% this script, you can first run the following command
% addpath(genpath('.'))

% Set up some information about the problem that we will solve (ATO).
dim = 8;
x0 = 20*ones(1,dim); % initial starting point (full inventory for each dimension)
runlength = 70; % parameter used by the simopt AssembleOrder function
problem = @AssembleOrder;
valuerange = 0:20; % range of each component of the search space
probparam = [dim,runlength];

budget = 1:200; % report values for each of the 200 samples

% Accelerated KG2.

% NumStartS is number of starting singletons
% NumStartP is number of starting pairs
% rescale is a rescaling of the hyperparameters, can be anything >= 1.  
% UpdateTimes are the times at which we update the MLE
others2 = struct('n1',10*dim,'n2',2*dim,'NumStartS',4,'NumStartP',2,...
    'rescale',2,'UpdateTimes',[30,60,100,150,200:100:budget(end)]);
solverparam2 = struct('numinitsols',0, 'numfinalsols',length(budget), 'other',others2);

A2 = AKG2(x0,budget,problem,valuerange,probparam,solverparam2,randi(999999999),randi(999999999));

% Accelerated KG.
others = struct('n1',10*dim,'n2',2*dim,'NumStarts',5,...
    'rescale',2,'UpdateTimes',[30,60,100,150,200:100:budget(end)]);
solverparam = struct('numinitsols',0, 'numfinalsols',length(budget), 'other',others);

A = AKG(x0,budget,problem,valuerange,probparam,solverparam,randi(999999999),randi(999999999));
